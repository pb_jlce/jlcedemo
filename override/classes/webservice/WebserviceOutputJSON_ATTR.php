<?php

/**
 * JLCE -> clase para un output en formato Json pero conservando los atributos del XML inicial
 * @author José Lorenzo Cameselle Escribano <lorenzo.cameselle@gmail.com>
 * @noinspection DuplicatedCode
 */
class WebserviceOutputJSON_ATTR implements WebserviceOutputInterface
{
    public $docUrl = '';
    public $languages = [];
    protected $wsUrl;
    protected $schemaToDisplay;

    /**
     * @param $schema
     * @return $this
     */
    public function setSchemaToDisplay($schema)
    {
        if (is_string($schema)) {
            $this->schemaToDisplay = $schema;
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchemaToDisplay()
    {
        return $this->schemaToDisplay;
    }

    /**
     * @param $url
     * @return $this
     */
    public function setWsUrl($url)
    {
        $this->wsUrl = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWsUrl()
    {
        return $this->wsUrl;
    }

    /**
     * @return string
     */
    public function getContentType(): string
    {
        return 'application/json';
    }

    /**
     * @param $languages
     */
    public function __construct($languages = [])
    {
        $this->languages = $languages;
    }

    /**
     * @param $languages
     * @return $this
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
        return $this;
    }

    /**
     * @return string
     */
    public function renderErrorsHeader(): string
    {
        return '<errors>' . "\n";
    }

    /**
     * @return string
     */
    public function renderErrorsFooter(): string
    {
        return '</errors>' . "\n";
    }

    /**
     * @param $message
     * @param $code
     * @return string
     */
    public function renderErrors($message, $code = null): string
    {
        $str_output = '<error>' . "\n";
        if ($code !== null) {
            $str_output .= '<code><![CDATA[' . $code . ']]></code>' . "\n";
        }
        $str_output .= '<message><![CDATA[' . $message . ']]></message>' . "\n";
        $str_output .= '</error>' . "\n";
        return $str_output;
    }

    /**
     * @param $field
     * @return string
     */
    public function renderField($field): string
    {
        $ret = '';
        $node_content = '';
        $ret .= '<' . $field['sqlId'];
        // display i18n fields
        if (isset($field['i18n']) && $field['i18n']) {
            foreach ($this->languages as $language) {
                $more_attr = '';
                if (isset($field['synopsis_details']) || (isset($field['value']) && is_array($field['value']))) {
                    $more_attr .= ' xlink:href="' . $this->getWsUrl() . 'languages/' . $language . '"';
                    if (isset($field['synopsis_details']) && $this->schemaToDisplay != 'blank') {
                        $more_attr .= ' format="isUnsignedId" ';
                    }
                }
                $node_content .= '<language id="' . $language . '"' . $more_attr . '>';
                $node_content .= '<![CDATA[';
                if (isset($field['value'][$language])) {
                    $node_content .= $field['value'][$language];
                }
                $node_content .= ']]>';
                $node_content .= '</language>';
            }
        } else {
            // display not i18n fields value
            if (array_key_exists('xlink_resource', $field) && $this->schemaToDisplay != 'blank') {
                if (!is_array($field['xlink_resource'])) {
                    $ret .= ' xlink:href="' . $this->getWsUrl() . $field['xlink_resource'] . '/' . $field['value'] . '"';
                } else {
                    $ret .= ' xlink:href="' . $this->getWsUrl() . $field['xlink_resource']['resourceName'] . '/' .
                        (isset($field['xlink_resource']['subResourceName']) ? $field['xlink_resource']['subResourceName'] . '/' . $field['object_id'] . '/' : '') . $field['value'] . '"';
                }
            }

            if (isset($field['getter']) && $this->schemaToDisplay != 'blank') {
                $ret .= ' notFilterable="true"';
            }

            if (isset($field['setter']) && !$field['setter'] && $this->schemaToDisplay == 'synopsis') {
                $ret .= ' read_only="true"';
            }

            if (array_key_exists('value', $field)) {
                $node_content .= '<![CDATA[' . $field['value'] . ']]>';
            }
        }

        if (isset($field['encode'])) {
            $ret .= ' encode="' . $field['encode'] . '"';
        }

        if (isset($field['synopsis_details']) && !empty($field['synopsis_details']) && $this->schemaToDisplay !== 'blank') {
            foreach ($field['synopsis_details'] as $name => $detail) {
                $ret .= ' ' . $name . '="' . (is_array($detail) ? implode(' ', $detail) : $detail) . '"';
            }
        }
        $ret .= '>';
        $ret .= $node_content;
        $ret .= '</' . $field['sqlId'] . '>' . "\n";
        return $ret;
    }

    /**
     * @param $node_name
     * @param $params
     * @param $more_attr
     * @param $has_child
     * @return string
     */
    public function renderNodeHeader($node_name, $params, $more_attr = null, $has_child = true): string
    {
        $string_attr = '';
        if (is_array($more_attr)) {
            foreach ($more_attr as $key => $attr) {
                if ($key === 'xlink_resource') {
                    $string_attr .= ' xlink:href="' . $attr . '"';
                } else {
                    $string_attr .= ' ' . $key . '="' . $attr . '"';
                }
            }
        }
        $end_tag = (!$has_child) ? '/>' : '>';
        return '<' . $node_name . $string_attr . $end_tag . "\n";
    }

    /**
     * @param $params
     * @return string
     */
    public function getNodeName($params): string
    {
        $node_name = '';
        if (isset($params['objectNodeName'])) {
            $node_name = $params['objectNodeName'];
        }
        return $node_name;
    }

    /**
     * @param $node_name
     * @param $params
     * @return string
     */
    public function renderNodeFooter($node_name, $params): string
    {
        return '</' . $node_name . '>' . "\n";
    }

    /**
     * @param $content
     * @return array|string|string[]|null
     */
    public function overrideContent($content)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
        $xml .= '<prestashop>' . "\n";
        $xml .= $content;
        $xml .= '</prestashop>' . "\n";

        $xml_sin_xlinks = preg_replace('/xlink:.*\"/', '', $xml); //Se eliminan los atributos xlink
        $xml_sin_nodeType = preg_replace('/nodeType.*\"/', '', $xml_sin_xlinks); //Se eliminan los atributos nodeType
        $xml_sin_attr_api = preg_replace('/api.*\"/', '', $xml_sin_nodeType); //Se eliminan los atributos api
        $clean_xml = preg_replace('/notFilterable.*\"/', '', $xml_sin_attr_api); //Se eliminan los atributos notFilterable

        include 'XML/Unserializer.php';

        $opciones = array(
            XML_UNSERIALIZER_OPTION_ATTRIBUTES_PARSE => true,
            XML_UNSERIALIZER_OPTION_ATTRIBUTES_ARRAYKEY => '_attributes',
            XML_UNSERIALIZER_OPTION_WHITESPACE => XML_UNSERIALIZER_WHITESPACE_NORMALIZE
        );

        ## Instancia del deserializador:
        $unserializer = new XML_Unserializer($opciones);

        ## Deserializar el XML pasado como parámetro:
        $status = $unserializer->unserialize($clean_xml);

        if (PEAR::isError($status)) {
            return $status->getMessage();
        } else {
            $data = $unserializer->getUnserializedData();

            $dataJsonPretty = json_encode($data, JSON_PRETTY_PRINT);
            $dataJsonClean = preg_replace_callback("/\\\\u([a-f0-9]{4})/", function ($matches) {
                return iconv('UCS-4LE', 'UTF-8', pack('V', hexdec('U' . $matches[1])));
            }, $dataJsonPretty);

            return $dataJsonClean;
        }
    }

    /**
     * @return string
     */
    public function renderAssociationWrapperHeader(): string
    {
        return '<associations>' . "\n";
    }

    /**
     * @return string
     */
    public function renderAssociationWrapperFooter(): string
    {
        return '</associations>' . "\n";
    }

    /**
     * @param $obj
     * @param $params
     * @param $assoc_name
     * @param $closed_tags
     * @return string
     */
    public function renderAssociationHeader($obj, $params, $assoc_name, $closed_tags = false): string
    {
        $end_tag = ($closed_tags) ? '/>' : '>';
        $more = '';
        if ($this->schemaToDisplay != 'blank') {
            if (array_key_exists('setter', $params['associations'][$assoc_name]) && !$params['associations'][$assoc_name]['setter']) {
                $more .= ' readOnly="true"';
            }
            $more .= ' nodeType="' . $params['associations'][$assoc_name]['resource'] . '"';
            if (isset($params['associations'][$assoc_name]['virtual_entity']) && $params['associations'][$assoc_name]['virtual_entity']) {
                $more .= ' virtualEntity="true"';
            } else {
                if (isset($params['associations'][$assoc_name]['api'])) {
                    $more .= ' api="' . $params['associations'][$assoc_name]['api'] . '"';
                } else {
                    $more .= ' api="' . $assoc_name . '"';
                }
            }
        }
        return '<' . $assoc_name . $more . $end_tag . "\n";
    }

    /**
     * @param $obj
     * @param $params
     * @param $assoc_name
     * @return string
     */
    public function renderAssociationFooter($obj, $params, $assoc_name): string
    {
        return '</' . $assoc_name . '>' . "\n";
    }


}

