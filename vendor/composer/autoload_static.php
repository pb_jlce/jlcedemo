<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit6539e130b03bf4f3c184f079b4fde68c
{
    public static $prefixLengthsPsr4 = array (
        'e' => 
        array (
            'jlcedemo\\Controllers\\' => 21,
            'jlcedemo\\Classes\\' => 17,
        ),
        'P' => 
        array (
            'PrestaShop\\Module\\jlcedemo\\' => 27,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'jlcedemo\\Controllers\\' => 
        array (
            0 => __DIR__ . '/../..' . '/controllers',
        ),
        'jlcedemo\\Classes\\' => 
        array (
            0 => __DIR__ . '/../..' . '/classes',
        ),
        'PrestaShop\\Module\\jlcedemo\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
        'jlcedemo\\Classes\\JlcedemoWSMisc' => __DIR__ . '/../..' . '/classes/JlcedemoWSMisc.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit6539e130b03bf4f3c184f079b4fde68c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit6539e130b03bf4f3c184f079b4fde68c::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit6539e130b03bf4f3c184f079b4fde68c::$classMap;

        }, null, ClassLoader::class);
    }
}
