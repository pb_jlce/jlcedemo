<?php
if (!defined('_PS_VERSION_')) exit;

/**
 * @param $object
 * @return bool
 * @author Jose Lorenzo Cameselle Escribano <lorenzo.cameselle@gmail.com>
 */
function upgrade_module_1_5_2($object)
{
    $object->uninstallOverrides();
    $object->installOverrides();
    return true;
}

