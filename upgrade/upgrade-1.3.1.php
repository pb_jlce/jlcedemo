<?php
if (!defined('_PS_VERSION_')) exit;

/**
 * @param $object
 * @return mixed
 * @author Jose Lorenzo Cameselle Escribano <lorenzo.cameselle@gmail.com>
 */
function upgrade_module_1_3_1($object)
{
    return ($object->registerHook('actionValidateOrder'));
}

