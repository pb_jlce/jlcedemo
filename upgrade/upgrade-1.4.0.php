<?php
if (!defined('_PS_VERSION_')) exit;

/**
 * @param $object
 * @return bool
 * @author Jose Lorenzo Cameselle Escribano <lorenzo.cameselle@gmail.com>
 */
function upgrade_module_1_4_0($object)
{
    $object->registerHook('addWebserviceResources');
    return true;
}

