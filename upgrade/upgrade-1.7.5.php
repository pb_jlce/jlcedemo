<?php
if (!defined('_PS_VERSION_')) exit;

/**
 * @param $object
 * @return bool
 * @author Jose Lorenzo Cameselle Escribano <lorenzo.cameselle@gmail.com>
 */
function upgrade_module_1_7_5($object)
{
    $object->unregisterHook('actionAttributeCombinationDelete');
    return true;
}

