(function () {

    'use strict';

    if (document.getElementById('scratch') !== null) {
        let canvas = document.getElementById('scratch');
        let context = canvas.getContext('2d');
        let jlce_hide_elements = document.querySelectorAll('.jlce_hide');

        [].forEach.call(jlce_hide_elements, function (thisEl) {
            thisEl.classList.remove('jlce_hide');
        });

        // default value
        context.globalCompositeOperation = 'source-over';

        // fill circle
        context.beginPath();
        context.fillStyle = '#555555';
        context.rect(0, 0, 300, 120);
        context.fill();

        //----------------------------------------------------------------------------

        let isDrag = false;

        function clearArc(x, y) {
            context.globalCompositeOperation = 'destination-out';
            context.beginPath();
            context.arc(x, y, 10, 0, Math.PI * 2, false);
            context.fill();
        }

        canvas.addEventListener('mousedown', function (event) {
            isDrag = true;

            clearArc(event.offsetX, event.offsetY);
        }, false);

        canvas.addEventListener('mousemove', function (event) {
            if (!isDrag) {
                return;
            }

            clearArc(event.offsetX, event.offsetY);
        }, false);

        canvas.addEventListener('mouseup', function () {
            isDrag = false;
        }, false);

        canvas.addEventListener('mouseleave', function () {
            isDrag = false;
        }, false);

        //----------------------------------------------------------------------------

        canvas.addEventListener('touchstart', function (event) {
            if (event.targetTouches.length !== 1) {
                return;
            }

            event.preventDefault();

            isDrag = true;

            clearArc(event.touches[0].offsetX, event.touches[0].offsetY);
        }, false);

        canvas.addEventListener('touchmove', function (event) {
            if (!isDrag || event.targetTouches.length !== 1) {
                return;
            }

            event.preventDefault();

            clearArc(event.touches[0].offsetX, event.touches[0].offsetY);
        }, false);

        canvas.addEventListener('touchend', function () {
            isDrag = false;
        }, false);
    }
}());
