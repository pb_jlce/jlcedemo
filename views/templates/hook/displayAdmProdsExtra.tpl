{*debug*}
{if isset($product_items_features)}
    <script>
        productItemsFeatures = JSON.parse('{$product_items_features|@json_encode nofilter}');
    </script>
    {if isset($product_items_features_html_data) && isset($jlce_id_product)}
        <div data-locale="{$lang_code}" class="jlcedemo row">
            <fieldset class="col-md-6 form-group">
                <label for="form_product_items_features" class="form-control-label translation-label-{$lang_code}">Características</label>
                <input type="text" id="form_product_items_features" name="form_product_items_features[{$lang_id}]"
                       class="form-control" value="{$product_items_features_html_data}" disabled/>
                <input type="hidden" id="jlce_id_product" name="jlce_id_product" value="{$jlce_id_product}">
            </fieldset>
        </div>
    {/if}
{/if}
