$(function () {
    if ($("body.adminorders").length) {
        // Para la página de editar el pedido en el BO
        formatAdminOrdersJlceMeasures();
    } else if ($("body.adminproducts").length) {
        // Para la página del producto en el BO
        hideJlceMeasures();
    }
});

// En la página de editar el pedido en el BO, da formato a la personalización jlce_Measures para que sea entendible para el usuarios
// Se modifican los precios y cantidades para las productos ML y M2
function formatAdminOrdersJlceMeasures() {
    var tr_cellProducts = $('.cellProduct');
    var tr_order_customizations = $('.order-product-customization');

    for (let i = 0; i < tr_cellProducts.length; i++) {
        let tr_cellProduct = tr_cellProducts[i];
        let tr_order_customization = tr_order_customizations[i];

        let order_jlce_Measures_element = $(tr_order_customization).find('p')[0];
        let order_jlce_Measures_object = JSON.parse(order_jlce_Measures_element.innerHTML.substring(32));

        let next_p = $(tr_order_customization).find('p')[1];

        if (order_jlce_Measures_object.prodType != 'NA') {
            let unit_price = $(tr_cellProduct).find('.cellProductUnitPrice')[0];
            let qty = $(tr_cellProduct).find('.cellProductQuantity .badge')[0];
            let available_qty = $(tr_cellProduct).find('.cellProductAvailableQuantity')[0];
            let total_price = $(tr_cellProduct).find('.cellProductTotalPrice')[0].innerHTML;

            total_price = total_price.replace('&nbsp;', '');
            total_price = parseFloat(total_price.replace(',','.'));

            qty.innerHTML = order_jlce_Measures_object.baseUnits;
            available_qty.innerHTML = parseInt(available_qty.innerHTML / order_jlce_Measures_object.frmtConvRatio);
            unit_price.innerHTML = ((total_price / order_jlce_Measures_object.baseUnits).toString().replace('.', ',')) + ' €';
        }

        switch (order_jlce_Measures_object.prodType) {
            case 'ML':
                order_jlce_Measures_element.innerHTML =
                    '<div class="jlce_Measures_div">' +
                    '<h4>Características</h4>' +
                    '<ul>' +
                    '<li><strong>Tipo de producto:</strong> ' + order_jlce_Measures_object.prodType + '</li>' +
                    '<li><strong>Cantidad solicitada (medidas):</strong> ' + order_jlce_Measures_object.msrQtyWntd.toString().replace('.', ',') + ' m</li>' +
                    '<li><strong>Largo:</strong> ' + order_jlce_Measures_object.long.toString().replace('.', ',') + ' m</li>' +
                    '</ul><ul>' +
                    '<li><strong>Peso:</strong> ' + order_jlce_Measures_object.weight.toString().replace('.', ',') + ' Kg</li>' +
                    '<li><strong>Precio Unitario:</strong> ' + order_jlce_Measures_object.untPrc.toString().replace('.', ',') + ' €</li>' +
                    '<li><strong>Precio de Corte:</strong> ' + order_jlce_Measures_object.cuttingPrice.toString().replace('.', ',') + ' €</li>' +
                    '</ul><ul>' +
                    '<li><strong>Descuento por formato (ratio):</strong> ' + order_jlce_Measures_object.frmtDiscRt.toString().replace('.', ',') + ' %</li>' +
                    '<li><strong>Descuento por formato:</strong> ' + order_jlce_Measures_object.frmtDisc.toString().replace('.', ',') + ' €</li>' +
                    '</ul>' +
                    '</div>'
                    ;
                break;
            case 'M2':
                order_jlce_Measures_element.innerHTML =
                    '<div class="jlce_Measures_div">' +
                    '<h4>Características</h4>' +
                    '<ul>' +
                    '<li><strong>Tipo de producto:</strong> ' + order_jlce_Measures_object.prodType + '</li>' +
                    '<li><strong>Cantidad solicitada (medidas):</strong> ' + order_jlce_Measures_object.msrQtyWntd.toString().replace('.', ',') + ' m</li>' +
                    '<li><strong>Largo:</strong> ' + order_jlce_Measures_object.long.toString().replace('.', ',') + ' m</li>' +
                    '</ul><ul>' +
                    '<li><strong>Ancho:</strong> ' + order_jlce_Measures_object.width.toString().replace('.', ',') + ' m</li>' +
                    '<li><strong>Peso:</strong> ' + order_jlce_Measures_object.weight.toString().replace('.', ',') + ' Kg</li>' +
                    '<li><strong>Precio Unitario:</strong> ' + order_jlce_Measures_object.untPrc.toString().replace('.', ',') + ' €</li>' +
                    '</ul><ul>' +
                    '<li><strong>Precio de Corte:</strong> ' + order_jlce_Measures_object.cuttingPrice.toString().replace('.', ',') + ' €</li>' +
                    '<li><strong>Descuento por formato (ratio):</strong> ' + order_jlce_Measures_object.frmtDiscRt.toString().replace('.', ',') + ' %</li>' +
                    '<li><strong>Descuento por formato:</strong> ' + order_jlce_Measures_object.frmtDisc.toString().replace('.', ',') + ' €</li>' +
                    '</ul><ul>' +
                    '</ul>' +
                    '</div>'
                    ;
                break;
            case 'NA':
                order_jlce_Measures_element.innerHTML =
                    '<div class="jlce_Measures_div">' +
                    '<h4>Características</h4>' +
                    '<ul>' +
                    '<li><strong>Tipo de producto:</strong> ' + order_jlce_Measures_object.prodType + '</li>' +
                    '<li><strong>Peso:</strong> ' + order_jlce_Measures_object.weight.toString().replace('.', ',') + ' Kg</li>' +
                    '<li><strong>Precio Unitario:</strong> ' + order_jlce_Measures_object.untPrc.toString().replace('.', ',') + ' €</li>' +
                    '</ul><ul>' +
                    '<li><strong>Descuento por formato (ratio):</strong> ' + order_jlce_Measures_object.frmtDiscRt.toString().replace('.', ',') + ' %</li>' +
                    '<li><strong>Descuento por formato:</strong> ' + order_jlce_Measures_object.frmtDisc.toString().replace('.', ',') + ' €</li>' +
                    '</ul>' +
                    '</div>'
                    ;
                break;
        }

        if (next_p) {
            next_p.style = 'clear: left';
        }
    }
}

// En la página del producto en el BO, en la sección Opciones -> Personalización se oculta la personalización jlce_Measures para el usuarios
function hideJlceMeasures() {
    var lis_customFieldCollection = $('.customFieldCollection li');

    for (let li of lis_customFieldCollection) {
        let input_jlce_Measures = $(li).find('input[value="jlce_Measures"]');

        if (input_jlce_Measures.length) {
            li.style = 'display: none;';
        }
    }
}