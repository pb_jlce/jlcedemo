<?php

use jlcedemo\Classes\JlcedemoTools;
use jlcedemo\Classes\JlcedemoWSMisc;

/**
 * Class WebserviceSpecificManagementJlcedemo
 * @author Jose Lorenzo Cameselle Escribano <lorenzo.cameselle@gmail.com>
 */
class WebserviceSpecificManagementJlcedemo implements WebserviceSpecificManagementInterface
{
    /** @var WebserviceOutputBuilder */
    protected $objOutput;
    protected $output;

    /** @var WebserviceRequest */
    protected $wsObject;

    /**
     * @var array
     */
    protected array $validateFunctions = [
        'test', 'test2', 'test3', 'test4',
        'getWsOrderByID', 'getWsOrderState',
        'getWsOrdersByStates', 'setStock',
        'setItemLotsStock', 'getActiveProdsList',
        'getActiveItemsList', 'getActiveDTypeItemsLst',
        'updateOrderStateByWs', 'checkProdImg',
        'setFullproduct', 'setFullproducts',
        'getOrdrsIdsByStates',
    ];

    /**
     * @param WebserviceOutputBuilderCore $obj
     * @return WebserviceSpecificManagementInterface
     */
    public function setObjectOutput(WebserviceOutputBuilderCore $obj)
    {
        $this->objOutput = $obj;

        return $this;
    }

    public function setWsObject(WebserviceRequestCore $obj)
    {
        $this->wsObject = $obj;

        return $this;
    }

    public function getWsObject()
    {
        return $this->wsObject;
    }

    public function getObjectOutput()
    {
        return $this->objOutput;
    }

    public function setUrlSegment($segments)
    {
        $this->urlSegment = $segments;

        return $this;
    }

    public function getUrlSegment()
    {
        return $this->urlSegment;
    }

    /**
     * Método para manejar el request.
     * @throws WebserviceException
     */
    public function manage()
    {
        if (count($this->wsObject->urlSegment) < 2) {
            throw new WebserviceException('Error url', array(100, 400));
        }

        if (!in_array($this->wsObject->urlSegment[1], $this->validateFunctions)) {
            throw new WebserviceException('Error validación url', array(100, 400));
        }

        $urlSegment = $this->wsObject->urlSegment;

        $funcion = $urlSegment[1];
        $params_1 = $urlSegment[2] ?? null;
        $params_2 = $urlSegment[3] ?? null;
        $params_3 = $urlSegment[4] ?? null;
        $this->objects = $this->{$funcion} ($params_1, $params_2, $params_3);
    }

    /**
     * En la salida lo que se hace es mostrar el objeto del body del request,
     * si es que ha pasado correctamente.
     * @return string
     */
    public function getContent(): string
    {
        if (empty($this->output)) $this->output .= $this->getWsObject()->getXmlInput();
        return $this->output;
    }

    /**
     * GET
     * @return string
     */
    private function test()
    {
        //$myTest = Order::getOrderIdByReference("AQOHTD071");
        //$myTest = Product::getProductAttrFormatCnvrsnRatio(2, 6);
        $myTest = json_encode(["Test" => "Hola Mundo !!"]);

        $this->output .= $myTest;
        return $this->output;
    }

    /**
     * POST
     * Ejemplo: https://cnc-robotica.com/api/jlcedemo/test2/hey
     * Resultado: {"errors":[{"code":200,"message":"¡¡ Acaba de subir el test: hey !!"}]}
     *
     * @param $testCode
     * @return mixed
     * @throws WebserviceException
     */
    private function test2($testCode)
    {
        throw new WebserviceException("¡¡ Acabas de postear el texto: $testCode", 200);
    }

    /**
     * POST param
     * @param $param
     * @return string
     */
    private function test3($param): string
    {
        $param = JlcedemoWSMisc::getOrderIdByReference($param);

        $this->output .= "$param";
        return $this->output;
    }

    /**
     * POST JSON body
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws WebserviceException
     * @noinspection DuplicatedCode
     */
    private function test4(): string
    {
        $errores = '';

        ## Primeramente se recibe el objeto de la request, pasado a array asociativo:
        $combinationJSON = $this->getWsObject()->getXmlInput();
        $combinationArr = json_decode($combinationJSON, true);
        $combinationArr['product_reference'] = "2005101000";

        $result = JlcedemoWSMisc::createOrUpdateCombination(
            $combinationArr, false, true, []
        );

        ## Siempre que en el resultado anterior se generen errores:
        if (is_string($result)) $errores .= $result;

        ## Se reemplanzan ciertos caracteres en la cadena de errores:
        $errores = str_replace('···P', '··· P', $errores);
        $errores = str_replace('···A', '··· A', $errores);
        $errores = str_replace('···¡', '··· ¡', $errores);
        $erroresArr = explode(' ··· ', $errores);
        $errores_json = json_encode($erroresArr, 128 | 256);

        ## Si se llega a este punto con errores:
        if (!empty(trim($errores))) throw new WebserviceException($errores_json, 400);

        ## Vaciado de caché:
        JlcedemoTools::clearPSCache();

        ## Si se llega aquí, se visualiza el objeto generado:
        $this->output .= $combinationJSON;
        return $this->output;
    }

    /**
     * @param int $id_order (El identificador del pedido de cliente de PS)
     * @return string (Se devuelven los datos del pedido parametrizado en formato JSON)
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    private function getWsOrderByID(int $id_order): string
    {
        ## Se obtiene el array del pedido cuyo ID se ha pasado como parámetro:
        $order_array = $this->getOrderByID($id_order);

        ## Salida en formato string JSON:
        $this->output .= json_encode($order_array, 128);
        return $this->output;
    }

    /**
     * @param int $id_order (El identificador del pedido de cliente de PS)
     * @return array (Se devuelven los datos del pedido parametrizado en formato arreglo)
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    private function getOrderByID(int $id_order): array
    {
        ## Se crea la estructura estándar del resource 'orders':
        $order_array = (new Order($id_order))->getFields();
        $order_rows = JlcedemoWSMisc::getWsJLCEOrderRows($id_order);
        if (!empty($order_rows)) {
            $order_rows = JlcedemoTools::arrayValuesToNumeric($order_rows, [
                "product_reference", "product_ean13", "product_upc"
            ]);
            foreach ($order_rows as $index => $eachOrdrRow) {
                if (array_key_exists('product_id', $eachOrdrRow)
                    && array_key_exists('product_attribute_id', $eachOrdrRow)
                ) {
                    $order_rows[$index]['product_name'] = Product::getProductName($eachOrdrRow['product_id']);
                    $attrParamsArr = Product::getAttributesParams(
                        $eachOrdrRow['product_id'],
                        $eachOrdrRow['product_attribute_id']
                    );
                    $order_rows[$index]['attributes_params'] = JlcedemoTools::arrayValuesToNumeric($attrParamsArr);
                }
            }
        }
        $order_array['associations']['order_rows'] = is_array($order_rows) ? $order_rows : [];

        ## Se obtienen datos extra relacionados con el pedido:
        $order_array_extra = JlcedemoWSMisc::getOrderExtraData($id_order, (int)$order_array['id_cart']);
        $cumulativeAvailability = 1;
        if (!empty($order_array_extra)) {
            $cumulativeAvailability = (int)$order_array_extra[0]['cumulative_availability'];
            $order_array['associations']['order_extra'] = $order_array_extra;
        }

        ## Operador intracomunitario. Se considera que por defecto no lo es:
        $intraEUOperator = false;
        $order_array['intra_eu_operator'] = false;
        /* @TODO En Randrade hay un módulo que comprueba que un NIF/CIF de un cliente sea
         * el de un operador intracomunitario. Si aquí, en CNCR, lo quieren, habrá que instalar
         * bien el mismo módulo, bien uno que haga algo similar. También se puede plantear
         * un desarrollo personalizado desde el módulo 'jlcedemo'...; al fin y al cabo,
         * es conectarse a una API y revisar si el cliente es operador intracomunitario o no.
         */

        ## Cada pedido tiene una dirección de envío y otra de facturación (normalmente coinciden).
        ## Por defecto las entidades 'order' de PS no anidan esa información, pero aquí se va a
        ## implementar así, de manera que se muestren los datos de esas direcciones:
        $id_address_delivery = (int)$order_array['id_address_delivery'];
        $id_address_invoice = (int)$order_array['id_address_invoice'];
        $order_array['id_address_delivery'] = JlcedemoWSMisc::getFullAddress($id_address_delivery, (int)$order_array['id_lang']);

        if ($id_address_delivery === $id_address_invoice) {
            $order_array['id_address_invoice'] = 'address_delivery';
            if (!empty($order_array['id_address_delivery']['intra_eu_operator'])
                && $order_array['id_address_delivery']['intra_eu_operator'] == 1
            ) {
                $intraEUOperator = true;
            }
        } else {
            $order_array['id_address_invoice'] = JlcedemoWSMisc::getFullAddress($id_address_invoice, (int)$order_array['id_lang']);
            if (!empty($order_array['id_address_invoice']["intra_eu_operator"])
                && $order_array['id_address_invoice']["intra_eu_operator"] == 1
            ) {
                $intraEUOperator = true;
            }
        }

        ## Determinar si el cliente del pedido actúa como operador intracomunitario:
        if ($intraEUOperator) $order_array['intra_eu_operator'] = true;

        ## Los pedidos están relacionados con un cliente. Las entidades 'order' no anidan la información del
        ## 'customer'. Dado que se quieren obtener todos los datos posibles del pedido, se tiene que:
        $id_customer = (int)$order_array['id_customer'];
        $order_array['id_customer'] = JlcedemoWSMisc::getFullCustomerData($id_customer, (int)$order_array['id_lang']);
        $first_customer_address_id = JlcedemoTools::getInitialCustomerAddressId($id_customer); // Dirección por defecto
        $siret = $order_array['id_customer']['siret'];

        ## Si no se guarda el NIF/CIF en el campo 'siret' entonces hay que buscar ese dato en la dirección principal del cliente:
        if (empty($siret) || strtoupper($siret) === 'NULL') {
            ## Siempre que haya una primera dirección activa del customer, se procede a buscar el NIF/CIF y generar un FederalTaxID:
            if ($first_customer_address_id && $first_customer_address_id !== 0) {
                $order_array['id_customer']['nif_cif'] = JlcedemoWSMisc::getDNIAddress($first_customer_address_id);
                $order_array['id_customer']['federalTaxID'] = JlcedemoWSMisc::getFederalTaxID_CustomerAddress($first_customer_address_id);
            }

        } else {
            ## Cuando sí hay establecido un 'siret' (NIF/CIF) para el customer,
            ## se busca formar el FederalTaxID a partir de ese dato:
            $siret = JlcedemoTools::trimCadena($siret);

            ## El Siret, por defecto, no llevaría el código ISO de ningún país:
            $siret_con_iso_code = false;

            ## Valores por defecto de los campos 'nif_cif' y 'federalTaxID':
            $order_array['id_customer']['nif_cif'] = $siret;
            $order_array['id_customer']['federalTaxID'] = '';

            ## Array que contiene los códigos ISO de todos los países:
            $coutries_iso_codes = JlcedemoTools::getCountries_iso_codes();

            ## A continuación se comprueba que el 'siret' no contenga ya el código ISO de algún país:
            foreach ($coutries_iso_codes as $codigo_iso => $pais) {
                if (JlcedemoTools::startsWith($siret, $codigo_iso)) $siret_con_iso_code = true;
            }

            if ($siret_con_iso_code) {
                ## Para el caso del campo 'nif_cif' devolverá lo mismo salvo el código ISO del país:
                $order_array['id_customer']['nif_cif'] = substr($siret, 2);
                $order_array['id_customer']['federalTaxID'] .= $siret;
            }

        }

        ## Se obtienen detalles adicionales del transportista (su nombre, etc):
        $order_array['id_carrier'] = JlcedemoWSMisc::getSomeCarrierData((int)$order_array['id_carrier']);

        ## Se modifican ciertos nombres de claves, para hacerlos más representativos:
        $order_array = JlcedemoTools::array_replace_key($order_array, 'id_address_delivery', 'address_delivery');
        $order_array = JlcedemoTools::array_replace_key($order_array, 'id_address_invoice', 'address_invoice');
        $order_array = JlcedemoTools::array_replace_key($order_array, 'id_customer', 'customer');
        $order_array = JlcedemoTools::array_replace_key($order_array, 'id_carrier', 'carrier');

        ## Se obtiene la disponibilidad total acumulada del pedido:
        $order_array['disponibilidad'] = $cumulativeAvailability;

        ## Se obtienen los posibles cupones de descuento que puedan estar imputados al pedido:
        $order_array['cupones'] = JlcedemoWSMisc::getOrderVouchers($id_order, (int)$order_array['id_cart']);

        ## Se devuelve el array con los datos del pedido y sus líneas, en modo estándar:
        return $order_array;
    }

    /**
     * @param int|null $id_order (El identificador de pedido de cliente de PrestaShop)
     * @param string|null $reference_order (La referencia de pedido de cliente de PrestaShop)
     * @return string (Se devuelve el estado actual del pedido de cliente recibido por parámetro)
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws WebserviceException
     */
    private function getWsOrderState(?int $id_order, ?string $reference_order): string
    {
        if (empty($id_order) && empty($reference_order)) {
            throw new WebserviceException('Se necesita recibir un Id de pedido válido o su referencia !!', 400);
        }

        if (empty($id_order) && !empty($reference_order)) {
            $id_order = JlcedemoWSMisc::getOrderIdByReference($reference_order);
        }

        $this->output .= (new Order($id_order))->getCurrentState();
        return $this->output;
    }

    /**
     * @param string $estados (Cadena de filtro SQL 'IN' por estados)
     * @param string $limit_offset (Acotación de resultados devueltos)
     * @return string (Se devuelve cadena JSON con los datos de los pedidos solicitados)
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    private function getWsOrdersByStates(string $estados, string $limit_offset): string
    {
        ## Array contenedor de los pedidos resultantes:
        $orders = ['orders' => []];

        ## Obtención del 'limit' y el 'offset' de la consulta, para así controlar el límite de resultados devueltos:
        $limit_offset_array = explode(',', $limit_offset);
        $limit = (int)$limit_offset_array[0];
        $offset = (int)$limit_offset_array[1];

        ## Se obtienen los IDs relacionados con los pedidos cuyos estados son los pasados como parámetro:
        $getOrdersSQLQuery = /** @lang MySQL DB query */
            "SELECT id_order
            FROM " . _DB_PREFIX_ . "orders
            WHERE current_state IN ($estados)
            LIMIT $limit OFFSET $offset";
        $orders_ids = Db::getInstance()->executeS($getOrdersSQLQuery);

        ## Se recorren los rows obtenidos de la consulta sql anterior:
        foreach ($orders_ids as $orders_id_row) {
            ## En el array contenedor 'orders' se van registrando todos los pedidos con los estados solicitados:
            $orders['orders'][] = $this->getOrderByID((int)$orders_id_row['id_order']);
        }

        ## Salida en formato string JSON:
        $this->output .= json_encode($orders, 128);
        return $this->output;
    }

    /**
     * @param string $ordersStates (IDs de estados de pedidos de PS separados por comas)
     * @return string (Se devuelve cadena JSON con los IDs de los pedidos requeridos)
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    private function getOrdrsIdsByStates(string $ordersStates): string
    {
        ## Array contenedor de los pedidos resultantes:
        $ordrsIds = [];

        ## Se obtienen los IDs relacionados con los pedidos cuyos estados son los pasados como parámetro:
        $mySQLQuery = /** @lang MySQL DB query */
            "SELECT id_order
            FROM " . _DB_PREFIX_ . "orders
            WHERE current_state IN ($ordersStates)";
        $ordersIdsArr = Db::getInstance()->executeS($mySQLQuery);

        ## Se recorren los rows obtenidos de la consulta sql anterior:
        foreach ($ordersIdsArr as $eachOrderId) {
            ## Se llena el array de Ids con cada Id obtenido de la consulta:
            $ordrsIds[] = (int)$eachOrderId["id_order"];
        }

        ## Salida en formato string JSON:
        $this->output .= json_encode($ordrsIds, 128);
        return $this->output;
    }

    /**
     * Función por la cual, a partir de un objeto request que representa a un conjunto
     * de artículos, se actualiza el stock de cada artículo incluido en el request.
     *
     * @return mixed (Se devuelve un mensaje indicando el éxito o una excepción indicando
     * los errores del procedimiento ejecutado)
     * @throws WebserviceException|PrestaShopException
     */
    private function setStock()
    {
        $errores_proceso = '';
        $contador_errores = 0;

        ## Se recibe el objeto de la request, pasado a array asociativo:
        $objetoRecibido = $this->getWsObject()->getXmlInput();
        $objetoRecibidoArray = json_decode($objetoRecibido, true);

        ## Carritos temporales para minorar consecuentemente el stock recibido:
        //try {
        //    $tempCarts = array_merge(Cart::getNAItemsTempCarts(), Cart::getDecimalItemsTempCarts());
        //} catch (PrestaShopDatabaseException $exc) {
        //    throw new WebserviceException("NO se han podido obtener los carritos temporales !!", 500);
        //}
        $tempCarts = [];

        ## A continuación se recorre cada artículo de la request:
        foreach ($objetoRecibidoArray as $articulo) {
            ## Se actualiza el stock disponible de cada artículo solicitado en la request:
            if (isset($articulo['IdProductAttr']) && is_numeric($articulo['IdProductAttr'])) {
                $updResult = JlcedemoWSMisc::updateItemStock($articulo, true, $tempCarts);
            } else {
                $updResult = JlcedemoWSMisc::updateItemStock($articulo, false, $tempCarts);
            }

            ## Si se ha producido un error:
            if ($updResult === false) {
                $contador_errores++;
                $thisItemCode = trim($articulo['ItemCode']);
                $errores_proceso .= "¡¡ ERROR al intentar sincronizar el stock y/o precio para el artículo $thisItemCode !! ··· ";
            }
        }

        ## Se reemplanzan ciertos caracteres en la cadena de errores y se le da forma final:
        $errores_proceso = str_replace('···¡', '··· ¡', $errores_proceso);
        $errores = explode(' ··· ', $errores_proceso);
        $errores_json = json_encode($errores, JSON_UNESCAPED_SLASHES);

        ## Si en este punto hay tantos errores como la cantidad de objetos a postear:
        if ($contador_errores === count($objetoRecibidoArray)) {
            ## Entonces ha fallado el proceso entero y se devuelve un 'bad request':
            throw new WebserviceException($errores_json, 400);
        }

        ## Si se llega a este punto con errores varios:
        if (!empty(trim($errores_proceso))) {
            ## Se devuelve un código 226 porque sí ha habido un POST parcial:
            throw new WebserviceException($errores_json, 226);
        }

        ## Vaciado de caché de la web:
        JlcedemoTools::clearPSCache(false, false);

        ## Si se llega aquí (sin errores):
        return $this->output;
    }

    /**
     * Función por la cual, a partir de un objeto request que representa a un conjunto
     * de artículos, se actualiza el campo 'measures_stocks' de cada artículo recibido.
     *
     * @return mixed (Se devuelve un mensaje indicando el éxito o una excepción indicando
     * los errores del procedimiento ejecutado)
     * @throws WebserviceException
     */
    private function setItemLotsStock()
    {
        $productAttrsArr = [];

        ## Se recibe el objeto de la request, pasado a array asociativo:
        $reqObject = $this->getWsObject()->getXmlInput();
        $reqObjectArr = json_decode($reqObject, true);
        $requestMsrsArr = $reqObjectArr;

        ## A continuación se recorre cada artículo de la request. Se busca conformar un array con todos
        ## los artículos (hijos), o combinaciones, y sus listados de medidas y stocks relacionados:
        foreach ($requestMsrsArr as $eachItemMsr) {
            $thisIdProductAttr = (int)$eachItemMsr['IdProductAttr'];
            $thisReqLong = (int)$eachItemMsr['Long'];
            $thisReqWidth = (int)$eachItemMsr['Width'];
            $m2Item = ($thisReqLong > 0 && $thisReqWidth > 0);
            $msrsStocksArr = [];
            $itemFound = false;
            ## A continuación se busca conformar el 'measures_stocks' de cada 'product_attribute' recibido:
            foreach ($requestMsrsArr as $thisIndex => $thisItem) {
                ## Siempre que se esté ante el mismo 'id_product_attribute':
                if ((int)$thisItem['IdProductAttr'] === $thisIdProductAttr) {
                    ## Se definen:
                    $thisLong = (int)$thisItem['Long'];
                    $thisWidth = (int)$thisItem['Width'];
                    $thisUnits = $thisItem['QtyUnits'] * 1;
                    $thisStockQty = $thisItem['QuantityOnStock'] * 1;
                    $isM2Item = ($thisLong > 0 && $thisWidth > 0);
                    $roundPrecision = 3;
                    $itemFound = true;
                    if ($isM2Item) {
                        ## Cálculo de las unidades (según medidas y cantidad disponible) para gestión por M2:
                        $qtyUnitary = ($thisLong * $thisWidth) / 1000000;
                        $thisUnits = round(($thisStockQty / $qtyUnitary), 2);
                        $roundPrecision = 6;
                    }
                    ## Se conforma el array de medidas y sus stocks en la presente combinación:
                    $msrsStocksArr[] = [
                        "long" => $thisLong,
                        "width" => $thisWidth,
                        "qty_units" => $thisUnits,
                        "quantity_on_stock" => round($thisStockQty, $roundPrecision)
                    ];
                    unset($requestMsrsArr[$thisIndex]);
                } else {
                    ## Dado que los datos están correlativamente ordenados por su 'IdProductAttr':
                    if ($itemFound) break;
                }
            }

            ## Se va conformando el array de stocks por medidas de combinaciones de productos:
            if (!empty($msrsStocksArr)) {
                $productAttrsArr[] = [
                    "id_product" => (int)$eachItemMsr['IdProduct'],
                    "id_product_attribute" => (int)$eachItemMsr['IdProductAttr'],
                    "reference" => $eachItemMsr['ItemCode'],
                    "old_reference" => $eachItemMsr['OldItemCode'] ?? null,
                    "m2_item" => $m2Item,
                    "measures_stocks" => $msrsStocksArr
                ];
            }
        }

        ## Ahora se busca actualizar el stock de lotes 'measures_stocks' de cada artículo (hijo) recibido:
        $errorsArr = [];
        foreach ($productAttrsArr as $productAttrArr) {
            $id_product = (int)$productAttrArr["id_product"];
            $itemReference = $productAttrArr["reference"];
            $setItemLotsInv = JlcedemoTools::setItemLotsInventory(
                $itemReference, $productAttrArr["measures_stocks"]
            );
            ## Para un resultado NO esperado:
            if (!$setItemLotsInv) {
                $errorsArr[] = [
                    "ERROR => ItemCode = $itemReference ( id_product = $id_product )"
                ];
                PrestaShopLogger::addLog("El artículo de código $itemReference (producto con ID $id_product), "
                    . "NO ha podido actualizar correctamente sus stocks por medidas de lotes disponibles", 3);
            }
        }

        ## Control de errores:
        $sizeOfErr = sizeof($errorsArr);
        $sizeOfReq = sizeof($reqObjectArr);
        ## Dependiendo del tamaño del array de errores, de haberlos:
        if ($sizeOfErr > 0 && $sizeOfErr < $sizeOfReq) {
            ## Se devuelve un código 200 porque sí ha habido un POST parcial:
            throw new WebserviceException(json_encode($errorsArr), 226);
        } elseif ($sizeOfReq > 0 && $sizeOfErr === $sizeOfReq) {
            ## Entonces ha fallado el proceso entero y se devuelve un 'bad request':
            throw new WebserviceException(json_encode($errorsArr), 400);
        }

        ## Vaciado de caché:
        JlcedemoTools::clearPSCache(false, false);

        ## Si se llega aquí (sin errores):
        return $this->output;
    }

    /**
     * Función que actualiza, vía WebService, el estado de un pedido al que se busca.
     *
     * @param int|string $order_code (El ID de pedido o la referencia de pedido de PrestaShop)
     * @param int $id_order_state (El ID del estado al que se desea cambiar)
     * @param string $target_states (Sólo se va a actuar con los pedidos que estén en alguno de estos estados)
     * @return string (Se devuelve un mensaje indicando el éxito o una excepción indicando el fracaso del procedimiento)
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws WebserviceException
     */
    private function updateOrderStateByWs($order_code, int $id_order_state, string $target_states)
    {
        if (is_numeric($order_code)) $order_code = (int)$order_code;

        ## Se llama al método capacitado para el cambio de estado del pedido:
        $resultado = JlcedemoWSMisc::updateOrderState($order_code, $id_order_state, explode(',', $target_states));

        if (!$resultado) {
            throw new WebserviceException(
                "NO es posible modificar el estado del pedido $order_code !! Revisa su estado actual.", 400
            );
        }

        $this->output .= "Estado actualizado !!";
        return $this->output;
    }

    /**
     * @return string (Se devuelve el listado de productos (padres) activos en web)
     * @throws PrestaShopDatabaseException
     */
    private function getActiveProdsList()
    {
        $activeProdsArr = JlcedemoWSMisc::getActiveProductsList();
        $this->output .= json_encode($activeProdsArr);
        return $this->output;
    }

    /**
     * @return string (Se devuelve el listado de artículos hijos activos en la web)
     * @throws PrestaShopDatabaseException
     */
    private function getActiveItemsList()
    {
        $activeItemsArr = JlcedemoWSMisc::getActiveItemsList();
        $this->output .= json_encode($activeItemsArr);
        return $this->output;
    }

    /**
     * @param string|null $prodReference (Por defecto 'null'. Si se recibe
     * una referencia de producto (padre), se filtrará la consulta por ella)
     * @return string (Se devuelve el listado de artículos hijos activos en web.
     * Sólo tipos decimales, o sea, tipos distintos de 'NA'; esto es, los 'ML',
     * 'M2','M3')
     * @throws PrestaShopDatabaseException
     */
    private function getActiveDTypeItemsLst(?string $prodReference = null)
    {
        $activeItemsArr = JlcedemoWSMisc::getActiveDTypeItemsLst($prodReference);
        $this->output .= json_encode($activeItemsArr);
        return $this->output;
    }

    /**
     * @return int (Se devuelve el resultado del chequeo por el que se comprueba
     * si un producto determinado existe y tiene una imagen subida o no. Si el
     * producto no existe el resultado sería negativo)
     */
    private function checkProdImg(): int
    {
        ## Primeramente se recibe el objeto de la request, pasado a array asociativo:
        $refObject = $this->getWsObject()->getXmlInput();
        $refArray = json_decode($refObject, true);

        ## Se devuelve el resultado de la comprobación:
        $this->setWsObject(new WebserviceRequest);
        $checkProdImage = JlcedemoWSMisc::checkProdImage($refArray['reference']);
        $this->output = (int)$checkProdImage;
        return $this->output;
    }

    /**
     * A partir de un objeto request que representa a un producto padre con sus
     * combinaciones (artículos hijos), se hace un POST que crea o actualiza esa
     * información en PrestaShop.
     *
     * @return string (Se devuelve un mensaje indicando el éxito o una excepción
     * indicando los errores del procedimiento ejecutado)
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws WebserviceException
     */
    private function setFullproduct(): string
    {
        $errores = '';

        ## Primeramente se recibe el objeto de la request, pasado a array asociativo:
        $fullProduct_object = $this->getWsObject()->getXmlInput();
        $fullProduct_array = json_decode($fullProduct_object, true);

        ## Se determinan el producto padre y sus combinaciones (los artículos hijos):
        $combinations_array = $fullProduct_array['combinations']; // Sólo las combinaciones en este caso
        unset($fullProduct_array['combinations']); // Ahora se elimina la clave (con sus valores) 'combinations'
        $product_array = $fullProduct_array; // Sólo el producto padre (sin combinaciones en este caso)

        ## Datos de base del producto padre:
        $product_reference = trim($product_array['reference']);
        $product_reference_old = trim($product_array['reference_old']);

        ## En este resource se recibe un objeto que contiene un producto padre con n combinaciones
        ## ( los artículos ) que dependen de él. Por tanto, se empieza posteando el producto base:
        $prodEditionRslt = JlcedemoWSMisc::createOrUpdateProduct($product_array);

        if (is_string($prodEditionRslt)) {
            $errores .= $prodEditionRslt;
            $errores = str_replace('···P', '··· P', $errores);
            $errores = str_replace('···¡', '··· ¡', $errores);

            ## Cuando en la cadena de resultado se encuentra un 'ERROR FATAL', hay errores
            ## de consideración, por tanto se devuelve una respuesta 'bad request' (400):
            if (strpos($errores, 'ERROR FATAL') !== false) {
                $errores = explode(' ··· ', $errores);
                $errores_json = json_encode($errores, 64 | 128 | 256);
                throw new WebserviceException($errores_json, 400);
            }
        } else {
            $counter = 1;
            $childsAmount = count($combinations_array);
            ## Creación de los artículos hijo (las combinaciones):
            foreach ($combinations_array as $combination_array) {
                ## Se indica el producto padre en cada combinación:
                $combination_array['product_reference'] = $product_reference;
                $combination_array['product_reference_old'] = $product_reference_old;

                ## También se hace necesario pasar el precio por gramo del producto padre, así como
                ## su precio por unidad de medida si corresponde (éste sólo para los 'ML', 'M2', 'M3');
                ## sin olvidar el peso unitario del producto padre para los artículos de venta por unidad:
                $combination_array['product_price'] = $prodEditionRslt["prd_price"];
                if (!empty($prodEditionRslt["prd_unit_price"])) {
                    $combination_array['product_unit_price'] = $prodEditionRslt["prd_unit_price"];
                }
                if (!empty($prodEditionRslt["prd_unit_weight"])) {
                    $combination_array['product_unit_weight'] = $prodEditionRslt["prd_unit_weight"];
                }

                ## A continuación se intenta postear cada combinación individualmente. Si
                ## es la última del producto, debe lanzarse un guardado de dicho producto:
                if ($counter === $childsAmount) {
                    $resultado_combinacion = JlcedemoWSMisc::createOrUpdateCombination(
                        $combination_array, false, true
                    );
                } else {
                    $resultado_combinacion = JlcedemoWSMisc::createOrUpdateCombination($combination_array);
                }

                ## Siempre que en el resultado anterior se generen errores:
                if (is_string($resultado_combinacion)) {
                    ## Se van añadiendo a la cadena general de 'errores':
                    $errores .= $resultado_combinacion;
                    $errores = str_replace('···A', '··· A', $errores);
                    $errores = str_replace('···¡', '··· ¡', $errores);
                }

                $counter++;
            }
        }

        ## Si se llega a este punto con errores:
        if (!empty(trim($errores))) {
            $errores = explode(' ··· ', $errores);
            $errores_json = json_encode($errores, 128 | 256);
            ## Se devuelve un código 200 porque los errores no tienen por qué impedir
            ## la creación del producto y, al menos, una parte de sus combinaciones:
            throw new WebserviceException($errores_json, 226);
        }

        ## Vaciado de caché:
        JlcedemoTools::clearPSCache();

        ## Si se llega aquí, se visualiza el objeto generado:
        $this->output .= $fullProduct_object;
        return $this->output;
    }

    /**
     * A partir de un objeto request que representa a un grupo de productos ( padres ) con sus
     * combinaciones ( artículos hijos ), se hace un POST que crea o actualiza esas entidades en PS.
     *
     * @return string (Se devuelve un mensaje indicando el éxito o una excepción indicando los errores
     * del procedimiento ejecutado)
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws WebserviceException
     */
    private function setFullproducts(): string
    {
        $errores_proceso = '';
        $contador_errores = 0;

        ## Primeramente se recibe el objeto de la request, pasado a array asociativo:
        $fullProducts_object = $this->getWsObject()->getXmlInput();
        $fullProducts_array = json_decode($fullProducts_object, true);
        $fullProducts_array_count = count($fullProducts_array);

        ## Adicionalmente se obtienen los carritos temporales. El objetivo es registrar
        ## el stock de los artículos teniendo en cuenta esto:
        $tempCarts = [];

        ## Se recorre el array anterior, trabajando con cada producto del mismo:
        foreach ($fullProducts_array as $fullProduct_array) {
            ## Se determinan el producto padre y sus combinaciones ( los artículos ):
            $combinations_array = $fullProduct_array['combinations']; // Sólo las combinaciones en este caso
            unset($fullProduct_array['combinations']); // Ahora se elimina la clave (con sus valores) 'combinations'

            ## Datos de base del producto padre:
            $product_reference = trim($fullProduct_array['reference']);
            $product_reference_old = trim($fullProduct_array['reference_old']);

            ## Se empieza posteando este producto base ( producto padre ):
            $prodEditionRslt = JlcedemoWSMisc::createOrUpdateProduct($fullProduct_array);

            if (is_string($prodEditionRslt)) {
                ## Cuando se encuentra un 'ERROR FATAL' que haya impedido el POST de esta entidad,
                ## entonces se descarta este producto y se intenta postear el siguiente:
                if (strpos($prodEditionRslt, 'ERROR FATAL') !== false) {
                    $contador_errores++;
                    $errores_proceso .= str_replace('ERROR FATAL', 'ERROR', $prodEditionRslt);
                    continue;
                }

                ## Si no hay error fatal pero sí algún error, se añade a la cadena de errores del proceso:
                $errores_proceso .= $prodEditionRslt;
            } else {
                $counter = 1;
                $childsAmount = count($combinations_array);
                ## Creación de los artículos hijo ( las combinaciones ):
                foreach ($combinations_array as $combination_array) {
                    ## Se indica el producto padre en cada combinación, si es que no lo tiene indicado ya:
                    if (empty(trim($combination_array['product_reference']))) {
                        $combination_array['product_reference'] = $product_reference;
                    }

                    if (empty(trim($combination_array['product_reference_old']))) {
                        $combination_array['product_reference_old'] = $product_reference_old;
                    }

                    ## También se hace necesario pasar el precio por gramo del producto padre, así como
                    ## su precio por unidad de medida si corresponde (éste sólo para los 'ML', 'M2', 'M3');
                    ## sin olvidar el peso unitario del producto padre para los artículos de venta por unidad:
                    $combination_array['product_price'] = $prodEditionRslt["prd_price"];
                    if (!empty($prodEditionRslt["prd_unit_price"])) {
                        $combination_array['product_unit_price'] = $prodEditionRslt["prd_unit_price"];
                    }
                    if (!empty($prodEditionRslt["prd_unit_weight"])) {
                        $combination_array['product_unit_weight'] = $prodEditionRslt["prd_unit_weight"];
                    }

                    ## A continuación se intenta postear cada combinación individualmente. Si
                    ## es la última del producto, debe lanzarse un guardado de dicho producto:
                    if ($counter === $childsAmount) {
                        $resultado_combinacion = JlcedemoWSMisc::createOrUpdateCombination(
                            $combination_array, false, true, $tempCarts
                        );
                    } else {
                        $resultado_combinacion = JlcedemoWSMisc::createOrUpdateCombination(
                            $combination_array, false, false, $tempCarts
                        );
                    }

                    ## Siempre que en el resultado anterior se generen errores:
                    if (is_string($resultado_combinacion)) {
                        ## Se van añadiendo a la cadena general de 'errores':
                        $errores_proceso .= $resultado_combinacion;
                    }

                    $counter++;
                }
            }
        }

        ## Se reemplanzan ciertos caracteres en la cadena de errores:
        $errores_proceso = str_replace('···P', '··· P', $errores_proceso);
        $errores_proceso = str_replace('···A', '··· A', $errores_proceso);
        $errores_proceso = str_replace('···¡', '··· ¡', $errores_proceso);
        $errores = explode(' ··· ', $errores_proceso);
        $errores_json = json_encode($errores, 128 | 256);

        ## Si en este punto hay tantos errores fatales como la cantidad de productos a postear:
        if ($contador_errores === $fullProducts_array_count) {
            ## Entonces ha fallado el proceso entero y se devuelve un 'bad request':
            throw new WebserviceException($errores_json, 400);
        }

        ## Si se llega a este punto con errores varios:
        if (!empty(trim($errores_proceso))) {
            ## Se devuelve un código 200 porque sí ha habido una creación parcial:
            throw new WebserviceException($errores_json, 226);
        }

        ## Vaciado de caché:
        JlcedemoTools::clearPSCache();

        ## Si se llega aquí (sin errores), se visualizan los objetos generados:
        $this->output .= $fullProducts_object;
        return $this->output;
    }


}

