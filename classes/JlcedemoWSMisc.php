<?php

namespace jlcedemo\Classes;

use Db;
use Tag;
use Shop;
use Hook;
use Group;
use Tools;
use Order;
use Image;
use Search;
use Module;
use Product;
use Context;
use Feature;
use Category;
use Supplier;
use Validate;
use Currency;
use Language;
use Attribute;
use ImageType;
use Warehouse;
use ObjectModel;
use Combination;
use ImageManager;
use FeatureValue;
use Manufacturer;
use Configuration;
use TaxRulesGroup;
use SpecificPrice;
use AttributeGroup;
use StockAvailable;
use ProductSupplier;
use PrestaShopLogger;
use SpecificPriceRule;
use TaxManagerFactory;
use PrestaShopException;
use StockManagerFactory;
use WarehouseProductLocation;
use PrestaShopDatabaseException;
use jlcedemo\Classes\JlcedemoTools;
use PrestaShop\PrestaShop\Core\Domain\Product\ValueObject\ProductType;

/**
 * Class JlcedemoWSMisc
 * @author Jose Lorenzo Cameselle Escribano <lorenzo.cameselle@gmail.com>
 */
class JlcedemoWSMisc extends ObjectModel
{
    /**
     * @var array (array validators)
     */
    public static $validators = array(
        'active' => array('JlcedemoWSMisc', 'getBoolean'),
        'tax_rate' => array('JlcedemoWSMisc', 'getPrice'),
        /** Tax excluded */
        'price_tex' => array('JlcedemoWSMisc', 'getPrice'),
        /** Tax included */
        'price_tin' => array('JlcedemoWSMisc', 'getPrice'),
        'reduction_price' => array('JlcedemoWSMisc', 'getPrice'),
        'reduction_percent' => array('JlcedemoWSMisc', 'getPrice'),
        'wholesale_price' => array('JlcedemoWSMisc', 'getPrice'),
        'ecotax' => array('JlcedemoWSMisc', 'getPrice'),
        'name' => array('JlcedemoWSMisc', 'createMultiLangField'),
        'description' => array('JlcedemoWSMisc', 'createMultiLangField'),
        'description_short' => array('JlcedemoWSMisc', 'createMultiLangField'),
        'meta_title' => array('JlcedemoWSMisc', 'createMultiLangField'),
        'meta_keywords' => array('JlcedemoWSMisc', 'createMultiLangField'),
        'meta_description' => array('JlcedemoWSMisc', 'createMultiLangField'),
        'link_rewrite' => array('JlcedemoWSMisc', 'createMultiLangField'),
        'available_now' => array('JlcedemoWSMisc', 'createMultiLangField'),
        'available_later' => array('JlcedemoWSMisc', 'createMultiLangField'),
        'category' => array('JlcedemoWSMisc', 'split'),
        'online_only' => array('JlcedemoWSMisc', 'getBoolean'),
    );

    /**
     * @param $field
     * @return bool
     */
    protected static function getBoolean($field): bool
    {
        return (bool)$field;
    }

    /**
     * @param $field
     * @return float
     */
    protected static function getPrice($field): float
    {
        $field = ((float)str_replace(',', '.', $field));
        return ((float)str_replace('%', '', $field));
    }

    /**
     * @param $field
     * @return array
     */
    public static function split($field): array
    {
        if (empty($field)) return [];

        $separator = Tools::getValue('multiple_value_separator');
        if (is_null($separator) || trim($separator) == '') {
            $separator = ',,';
        }

        do {
            $uniqid_path = _PS_UPLOAD_DIR_ . uniqid();
        } while (file_exists($uniqid_path));

        file_put_contents($uniqid_path, $field);

        $tab = '';
        if (!empty($uniqid_path)) {
            $fd = fopen($uniqid_path, 'r');
            $tab = fgetcsv($fd, MAX_LINE_SIZE, $separator);
            fclose($fd);
            if (file_exists($uniqid_path)) {
                @unlink($uniqid_path);
            }
        }

        if (empty($tab) || (!is_array($tab))) return [];

        return $tab;
    }

    /**
     * @param $field
     * @return array
     */
    public static function createMultiLangField($field): array
    {
        $result = [];
        foreach (Language::getIDs(false) as $id_lang) {
            $result[$id_lang] = $field;
        }

        return $result;
    }

    /**
     * @param $array
     * @param $funcname
     * @param mixed $user_data
     * @return bool
     */
    public static function arrayWalk(&$array, $funcname, &$user_data = false): bool
    {
        if (!is_callable($funcname)) return false;

        foreach ($array as $k => $row) {
            if (!call_user_func_array($funcname, array($row, $k, &$user_data))) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $infos
     * @param $key
     * @param $entity
     * @return bool
     * @throws PrestaShopException
     */
    public static function fillInfo($infos, $key, &$entity)
    {
        $infos = trim($infos);
        if (isset(self::$validators[$key][1]) && self::$validators[$key][1] == 'createMultiLangField' && Tools::getValue('iso_lang')) {
            $id_lang = Language::getIdByIso(Tools::getValue('iso_lang'));
            $tmp = call_user_func(self::$validators[$key], $infos);
            foreach ($tmp as $id_lang_tmp => $value) {
                if (empty($entity->{$key}[$id_lang_tmp]) || $id_lang_tmp == $id_lang) {
                    $entity->{$key}[$id_lang_tmp] = $value;
                }
            }
        } elseif (!empty($infos) || $infos == '0') { // ($infos == '0') => Para deshabilitar un producto usar '0' en 'active', porque empty('0') devuelve 'true'
            $entity->{$key} = isset(self::$validators[$key]) ? call_user_func(self::$validators[$key], $infos) : $infos;
        }

        return true;
    }

    /**
     * @param int $id_order (El identificador del pedido de cliente de PS)
     * @return array (Se devuelve un array que representa las líneas de detalle
     * del pedido solicitado por parámetro)
     * @throws PrestaShopDatabaseException
     */
    public static function getWsJLCEOrderRows(int $id_order): array
    {
        $query = /** @lang MySQL DB query */
            "SELECT
                id_order_detail as id,
                product_id,
                product_price,
                $id_order id_order,
                product_attribute_id,
                product_quantity,
                product_name,
                product_reference,
                product_ean13,
                product_upc,
                unit_price_tax_incl,
                unit_price_tax_excl,
                CASE
                    WHEN (
                        SELECT COUNT(*)
                        FROM " . _DB_PREFIX_ . "feature_product
                        WHERE " . _DB_PREFIX_ . "feature_product.id_product = " . _DB_PREFIX_ . "order_detail.product_id
                        AND " . _DB_PREFIX_ . "feature_product.id_feature = 2
                        AND " . _DB_PREFIX_ . "feature_product.id_feature_value = 9
                    ) > 0 THEN 'M2'
                    WHEN (
                        SELECT COUNT(*)
                        FROM " . _DB_PREFIX_ . "feature_product
                        WHERE " . _DB_PREFIX_ . "feature_product.id_product = " . _DB_PREFIX_ . "order_detail.product_id
                        AND " . _DB_PREFIX_ . "feature_product.id_feature = 2
                        AND " . _DB_PREFIX_ . "feature_product.id_feature_value = 8
                    ) > 0 THEN 'ML'
                    ELSE 'NA'
                END \"udm\"
            FROM " . _DB_PREFIX_ . "order_detail
            WHERE id_order = $id_order";
        $result = Db::getInstance()->executeS($query);

        if (is_array($result) && !empty($result)) return $result;

        return [];
    }

    /**
     * @param string $order_reference (La referencia del pedido de cliente de PS)
     * @return int (Se devuelve el identificador 'id_order' del pedido de cliente)
     */
    public static function getOrderIdByReference(string $order_reference): int
    {
        $sqlStrQuery = /** @lang MySQL DB query */
            "SELECT id_order
            FROM " . _DB_PREFIX_ . "orders
            WHERE reference = '$order_reference'";
        return (int)Db::getInstance()->getValue($sqlStrQuery);
    }

    /**
     * @param int $id_order (El identificador del pedido de cliente de PS)
     * @param int $id_cart (El identificador del carrito relacionado con el pedido de cliente de PS)
     * @return array (Se devuelve un arreglo con los datos extra del pedido de cliente parametrizado)
     * @throws PrestaShopDatabaseException
     */
    public static function getOrderExtraData(int $id_order, int $id_cart): array
    {
        $order_array_extra = [];

        ## Se obtienen las líneas del megacarrito relacionado con el pedido:
        $mySQLQuery = /** @lang MySQL DB query */
            "SELECT
                PCP.id_cart,
                PCP.id_customization,
                PCP.id_product,
                PCP.id_product_attribute as product_attribute_id,
                POD.product_reference as reference,
                PCD.value jlce_measures,
                0 as this_long,
                0 as this_width,
                0 as this_height,
                NULL as quantity,
                CASE
                    WHEN (
                        SELECT COUNT(*)
                        FROM " . _DB_PREFIX_ . "feature_product PFP
                        WHERE PFP.id_product = PCP.id_product
                        AND PFP.id_feature = 2
                        AND PFP.id_feature_value = 9
                    ) > 0 THEN 'M2'
                    WHEN (
                        SELECT COUNT(*)
                        FROM " . _DB_PREFIX_ . "feature_product PFP
                        WHERE PFP.id_product = PCP.id_product
                        AND PFP.id_feature = 2
                        AND PFP.id_feature_value = 8
                    ) > 0 THEN 'ML'
                    ELSE 'NA'
                END \"udm\",
                PEIF.item_features,
                POD.total_price_tax_excl,
                POD.reduction_percent,
                POD.tax_rate,
                POD.original_product_price,
                PP.price base_product_price,
                PPA.price base_product_price_impact
            FROM " . _DB_PREFIX_ . "cart_product PCP
            INNER JOIN " . _DB_PREFIX_ . "product PP
                ON PP.id_product = PCP.id_product
            INNER JOIN " . _DB_PREFIX_ . "product_attribute PPA
                ON PPA.id_product_attribute = PCP.id_product_attribute
            LEFT JOIN " . _DB_PREFIX_ . "order_detail POD
                ON POD.id_customization = PCP.id_customization
                AND POD.product_attribute_id = PCP.id_product_attribute
                AND POD.id_order = $id_order
            LEFT JOIN " . _DB_PREFIX_ . "customized_data PCD
                ON PCD.id_customization = PCP.id_customization
            LEFT JOIN " . _DB_PREFIX_ . "jlcedemo_items_features PEIF
                ON PEIF.item_reference = POD.product_reference
                AND PEIF.id_product = PCP.id_product
            WHERE PCP.id_cart = $id_cart";
        $cartProduct_rows = Db::getInstance()->executeS($mySQLQuery);

        ## Se recorre el array anterior:
        $cumulativeAvailability = 0; // Disponibilidad de artículos acumulada
        foreach ($cartProduct_rows as $carProduct_row) {
            ## Si se diera el caso de que:
            if (empty($carProduct_row['reference'])
                || empty($carProduct_row['jlce_measures'])
                || empty($carProduct_row['item_features'])
                || strpos($carProduct_row['jlce_measures'], "\"prodType\"") === false
            ) {
                ## Se descarta esta línea
                continue;
            }

            ## Todos los valores convertibles a numéricos, se convierten a ello:
            $carProduct_row = JlcedemoTools::arrayValuesToNumeric(
                $carProduct_row, ['reference', 'jlce_measures', 'udm', 'item_features']
            );

            ## Referencia del artículo hijo (combinación):
            $thisItemReference = $carProduct_row['reference'];

            ## Se obtienen ahora los atributos y medidas guardados en 'jlce_measures':
            $jlceMeasuresArr = json_decode($carProduct_row['jlce_measures'], true);
            ## Así como las características del presente artículo hijo (combinación):
            $itemFeaturesArr = json_decode($carProduct_row['item_features'], true);

            ## Del arreglo de características se declaran:
            $itemType = $itemFeaturesArr['UTM'];
            $longFormat = $itemFeaturesArr['LongFormat'] * 1;
            $widthFormat = $itemFeaturesArr['WidthFormat'] * 1;
            $heightFormat = $itemFeaturesArr['HeightFormat'] * 1;
            $unitsFormat = (int)$itemFeaturesArr['UnitsFormat'];
            $availability = (int)$itemFeaturesArr['Availability'];
            if ($availability > $cumulativeAvailability) $cumulativeAvailability = $availability;

            ## Cálculo del formato almacenado según el tipo de artículo:
            switch ($itemType) {
                case 'ML':
                    ## Metro lineal:
                    $formatStored = $longFormat;
                    break;
                case 'M2':
                    ## Metro cuadrado:
                    $formatStored = $longFormat * $widthFormat;
                    break;
                case 'M3':
                    ## Metro cúbico:
                    $formatStored = $longFormat * $widthFormat * $heightFormat;
                    break;
                default:
                    ## Tipo 'NA':
                    $formatStored = $unitsFormat;
            }

            ## Cálculo del precio de venta del presente artículo hijo (combinación):
            $ordrItemBasePrice = $carProduct_row['original_product_price'] * 1;
            $prodBasePrice = $carProduct_row['base_product_price'] * 1;
            $prodBasePriceImpact = $carProduct_row['base_product_price_impact'] * 1;
            $itemBasePrice = $prodBasePrice + $prodBasePriceImpact;

            ## Cálculo del descuento en el producto (no específico) vía 'grupo' del cliente:
            $customerdiscountpc = round((1 - ($ordrItemBasePrice / $itemBasePrice)), 6);

            ## A partir de las 'jlce_measures' se tiene que:
            $formtDiscRatio = $jlceMeasuresArr['frmtDiscRt'] / 100;
            $formtDisc = $jlceMeasuresArr['frmtDisc'] * 1;
            if (empty($formtDisc) && !empty($formtDiscRatio)) $formtDiscRatio = 0;
            $carProduct_row['baseUnits'] = (int)$jlceMeasuresArr['baseUnits']; // Unidades solicitadas
            $carProduct_row['length'] = $jlceMeasuresArr['long'] * 1; // Largo solicitado
            $carProduct_row['width'] = $jlceMeasuresArr['width'] * 1; // Ancho solicitado
            $carProduct_row['height'] = $jlceMeasuresArr['height'] * 1; // Alto solicitado
            $carProduct_row['weight'] = $jlceMeasuresArr['weight'] * 1; // Peso solicitado
            $carProduct_row['msrQtyWntd'] = $jlceMeasuresArr['msrQtyWntd'] * 1; // Cantidad solicitada (resultado medida)
            $carProduct_row['frmtConvRatio'] = $jlceMeasuresArr['frmtConvRatio'] * 1; // Ratio de convesión de formato
            $carProduct_row['format'] = $formatStored; // Formato almacenado
            $carProduct_row['formatextra'] = $jlceMeasuresArr['cuttingPrice'] * 1; // Precio de los cortes
            $carProduct_row['formatdiscountpc'] = $formtDiscRatio; // Descuento por formato (ratio)
            $carProduct_row['frmtDisc'] = $formtDisc; // Descuento por formato
            $carProduct_row['customerdiscountpc'] = $customerdiscountpc; // Descuento de cliente (ratio)
            $carProduct_row['productdiscountpc'] = $carProduct_row['reduction_percent'] / 100; // Descuento específico producto (ratio)
            $carProduct_row['priceunitary'] = $jlceMeasuresArr['untPrc'] * 1; // Precio por unidad de medida
            $carProduct_row['price'] = round(
                ($carProduct_row['total_price_tax_excl'] / $carProduct_row['baseUnits']), 6
            ); // Precio (sin IVA) por unidad solicitada
            $carProduct_row['tax_rate'] *= 1;

            ## Se eliminan las siguientes claves:
            unset($carProduct_row['jlce_measures']);
            unset($carProduct_row['item_features']);
            unset($carProduct_row['original_product_price']);
            unset($carProduct_row['base_product_price']);
            unset($carProduct_row['base_product_price_impact']);
            unset($carProduct_row['reduction_percent']);
            unset($carProduct_row['total_price_tax_excl']);

            ## Cálculo de la cantidad teniendo en cuenta el tipo de producto:
            $carProduct_row['quantity'] = $carProduct_row['baseUnits'];
            if ($jlceMeasuresArr['prodType'] !== "NA") {
                $thisLong = $jlceMeasuresArr["long"] * 1;
                $thisWidth = $jlceMeasuresArr["width"] * 1;
                $thisHeight = $jlceMeasuresArr["height"] * 1;
                $thisUnits = $jlceMeasuresArr['baseUnits'] * 1;

                ## Cálculo de cantidad:
                if ($thisLong > 0 && $thisWidth > 0) {
                    $thisQty = $thisLong * $thisWidth * $thisUnits;
                } else {
                    $thisQty = $thisLong * $thisUnits;
                }

                $carProduct_row['quantity'] = $thisQty;
                $carProduct_row['this_long'] = $thisLong * 1000;
                $carProduct_row['this_width'] = $thisWidth * 1000;
                $carProduct_row['this_height'] = $thisHeight * 1000;
            } else {
                ## Para productos recortes y retales, sus atributos 'Largo/Alto' y 'Ancho' se
                ## van a considerar como propiedades de largo y ancho. Así se tiene que:
                if (strrpos($thisItemReference, 'RET', -3) !== false
                    || strrpos($thisItemReference, 'REC', -3) !== false
                ) {
                    $attrParamsArr = Product::getAttributesParams(
                        $carProduct_row['id_product'],
                        $carProduct_row['product_attribute_id']
                    );

                    ## Se determina la referencia (o código) del artículo original (no el recorte):
                    $originItemCode = substr($thisItemReference, 0, -3);
                    ## Y ahora si el artículo original se gestiona o no por metro cuadrado:
                    $isM2CuttingProdType = JlcedemoTools::isM2ItemType($originItemCode);

                    ## Hay ciertos casos en los que el atributo ancho es un atributo y no una propiedad.
                    ## Para los casos de recortes (o retales) bajo esa casuística, no debe considerarse
                    ## el ancho. Así, siempre que el array anterior no esté vacío, se tiene que:
                    if (!empty($attrParamsArr[0]['group'])) {
                        foreach ($attrParamsArr as $attrArray) {
                            if ($attrArray['group'] === 'Largo/Alto') {
                                $carProduct_row['this_long'] = (int)$attrArray['name'];
                            }
                            if ($attrArray['group'] === 'Ancho') {
                                if ($isM2CuttingProdType) {
                                    ## Chapas:
                                    $carProduct_row['this_width'] = (int)$attrArray['name'];
                                } else {
                                    ## Barras:
                                    $carProduct_row['this_width'] = 0;
                                }
                            }
                        }

                        ## Cálculo de cantidad:
                        $attLong = $carProduct_row['this_long'];
                        $attWidth = $carProduct_row['this_width'];
                        $thisUnits = $carProduct_row['baseUnits'];

                        if ($attLong > 0 && $attWidth > 0) {
                            $thisQty = ($attLong / 1000) * ($attWidth / 1000) * $thisUnits;
                        } else {
                            $thisQty = ($attLong / 1000) * $thisUnits;
                        }

                        $carProduct_row['quantity'] = $thisQty > 0 ? round($thisQty, 6) : $thisUnits;
                    }
                }
            }

            ## Se llena el array contenedor 'order_array_extra' con los rows del carrito indicado:
            $order_array_extra[] = $carProduct_row;
        }

        ## Ahora se busca registrar la disponibilidad acumulada por cada tupla:
        $orderExtraArr = [];
        foreach ($order_array_extra as $eachExtraDada) {
            $eachExtraDada['cumulative_availability'] = $cumulativeAvailability;
            $orderExtraArr[] = $eachExtraDada;
        }

        return $orderExtraArr;
    }

    /**
     * Función que busca conformar un FederalTaxID a partir de una serie de datos dados:
     * un NIF/CIF, un código de operador intracomunitario (VAT number) y un código ISO de país.
     *
     * @param string $iso_code (El código ISO del país. Por ejemplo 'ES' para España o 'PT' para Portugal.)
     * @param string $dni_field (Un valor para el NIF/CIF. Si no se aporta ese dato, será una cadena vacía por defecto.)
     * @param string $vat_number (Un valor para el VAT number. Si no se aporta ese dato, será una cadena vacía por defecto.)
     * @return string (Se devuelve un FederalTaxID conformado por los valores anteriores. De no haber un NIF/CIF o
     * un código de operador intracomunitario (VAT number) se devuelve una cadena vacía sin más.)
     */
    public static function formFederalTaxID(string $iso_code, string $dni_field = '', string $vat_number = ''): string
    {
        $federalTaxID = '';

        ## Se comprueba primeramente que haya un VAT number; de ser así, el FederalTaxID sería
        ## directamente igual a ese VAT number, siempre que sea, al menos de inicio, correcto:
        if (!(empty($vat_number) || $vat_number === 'null' || $vat_number === 'NULL')) {
            /*
             * Antes de nada se comprueba que el VAT number comience por un código ISO de
             * país; esto es, se comprueba que al menos en el comienzo de dicha cadena se
             * cumpla el formato. Si en este punto no lleva ningún código ISO de país, se
             * le concatena el código ISO del país asociado a la dirección obtenida
             */
            $vat_number_con_iso_code = false; // Por defecto se va a suponer que no lleva ese código ISO
            $coutries_iso_codes = JlcedemoTools::getCountries_iso_codes(); // Se obtiene un array con todos los códigos ISO de países

            ## Se recorre el array de todos los códigos ISO de países para determinar si el VAT Number
            ## comienza por alguno de esos códigos, lo que determinaría que al menos en inicio es correcto:
            foreach ($coutries_iso_codes as $codigo_iso => $pais) {
                if (JlcedemoTools::startsWith(JlcedemoTools::trimCadena($vat_number), $codigo_iso)) {
                    $vat_number_con_iso_code = true;
                }
            }

            if ($vat_number_con_iso_code) {
                $federalTaxID .= JlcedemoTools::trimCadena($vat_number);
            } else {
                $federalTaxID .= $iso_code . JlcedemoTools::trimCadena($vat_number);
            }
        } elseif (!(empty($dni_field) || $dni_field === 'null' || $dni_field === 'NULL')) {
            ## Cuando no hay un VAT number, se busca formar un FederalTaxID con los datos
            ## 'iso_code' y 'dni':
            $federalTaxID .= $iso_code . JlcedemoTools::trimCadena($dni_field);

        }

        return $federalTaxID;
    }

    /**
     * Función personalizada para obtener todos los datos de una dirección
     * determinada por su ID, teniendo en cuenta el 'id_lang' pasado por
     * parámetro.
     *
     * @param int $id_address (Identificador de dirección)
     * @param int $id_lang (Identificador de idioma)
     * @return array (Datos de la dirección)
     */
    public static function getFullAddress(int $id_address, int $id_lang): array
    {
        ## Formato tipo de una dirección:
        $full_address_format = [
            'id_address' => '',
            'alias' => '',
            'city' => '',
            'postcode' => '',
            'provincia' => '',
            'pais' => 'España', // Por defecto el país es España
            'iso_code' => 'ES', // Por defecto el de España
            'other' => '',
            'phone' => '',
            'phone_mobile' => '',
            'company' => '',
            'firstname' => '',
            'lastname' => '',
            'address1' => '',
            'address2' => '',
            'vat_number' => '',
            'dni' => '',
        ];

        /* @TODO La columna 'intra_eu_operator' viene de Randrade, que tiene un módulo que
         * comprueba que un NIF/CIF de un cliente sea el de un operador intracomunitario.
         * Si aquí, en CNCR, lo quieren, habrá que instalar bien el mismo módulo, bien uno
         * que haga algo similar. También se puede plantear un desarrollo personalizado desde
         * el módulo 'jlcedemo'...; al fin y al cabo, es conectarse a una API y revisar si el
         * cliente es operador intracomunitario o no. Por ahora se pasa un false directamente.
         */
        $sql_address = /** @lang MySQLQuery */
            "SELECT
				PA.id_address, PA.alias, PA.city, PA.postcode,
				IFNULL(PS.name, '---') provincia, PCL.name pais,
				PC.iso_code, PA.other, PA.phone, PA.phone_mobile,
				PA.company, PA.firstname, PA.lastname, PA.address1,
				PA.address2, PA.vat_number, PA.dni, 0 intra_eu_operator
			FROM " . _DB_PREFIX_ . "address PA
			INNER JOIN " . _DB_PREFIX_ . "country_lang PCL
				ON PCL.id_country = PA.id_country
				AND PCL.id_lang = $id_lang
			INNER JOIN " . _DB_PREFIX_ . "country PC
				ON PC.id_country = PA.id_country
			LEFT JOIN " . _DB_PREFIX_ . "state PS
				ON PS.id_state = PA.id_state
			WHERE PA.id_address = $id_address";
        $array_full_address = Db::getInstance()->getRow($sql_address);

        ## Si la consulta anterior arroja un resultado:
        if (!empty($array_full_address["id_address"])) {
            ## Se recorre el array del resultado de la consulta y se rellena el array de formato
            ## de la dirección con los valores que arroje el resultado indicado:
            foreach ($array_full_address as $key => $value) {
                $full_address_format[$key] = $value;
                if ($key === 'id_address') $full_address_format[$key] = (int)$value;
            }
        }

        ## A continuación se conforma el campo 'address_federalTaxID' de la dirección:
        $full_address_format['address_federalTaxID'] = self::formFederalTaxID(
            $full_address_format['iso_code'], $full_address_format['dni'], $full_address_format['vat_number']
        );

        return $full_address_format;
    }

    /**
     * Función personalizada para obtener todos los datos de un cliente
     * determinado por su ID, teniendo en cuenta el 'id_lang' recibido
     * por parámetro.
     *
     * @param int $id_customer (Identificador del cliente)
     * @param int $id_lang (Identificador del idioma)
     * @return array (Datos del cliente parametrizado en formato arreglo)
     */
    public static function getFullCustomerData(int $id_customer, int $id_lang): array
    {
        /* @TODO la columna 'tipo_cliente' viene de un desarrollo hecho para Randrade.
         * Los tipos que se manejan en Randrade son: "particular", "autonomo", "empresa".
         * En el futuro lo suyo sería realizar un desarrollo similar a lo hecho para
         * Randrade y consultar dicho valor de la columna 'tipo_cliente' de la tabla
         * que corresponda de la base de datos.
         *
         * En Randrade se utiliza el campo 'siret' para registrar ahí el NIF/CIF del cliente;
         * en cambio, el campo 'ape' lo utilizan para registrar un código de cliente interno
         * que manejaban del antiguo ERP 'EXIT'.
         */
        $sql_customer = /** @lang MySQLQuery */
            "SELECT
				PC.id_customer,
				PC.firstname,
				PC.lastname,
				PC.email,
				PC.birthday,
				PGL.name AS grupo,
				'general' tipo_cliente,
				PC.company,
				PC.siret,
				PC.ape,
				PC.newsletter,
				PC.website,
				PC.note
			FROM " . _DB_PREFIX_ . "customer PC
			INNER JOIN " . _DB_PREFIX_ . "group_lang PGL
				ON PGL.id_group = PC.id_default_group
			WHERE PC.id_customer = $id_customer
			AND PGL.id_lang = $id_lang";
        $array_customer_data = Db::getInstance()->getRow($sql_customer);
        if (empty($array_customer_data["id_customer"])) return [];

        if ($array_customer_data['birthday'] === '0000-00-00') {
            $array_customer_data['birthday'] = null;
        }

        if ($array_customer_data['newsletter'] === '0') {
            $array_customer_data['newsletter'] = false;
        } else {
            $array_customer_data['newsletter'] = true;
        }

        ## Se añaden adicionalmente los campos personalizados 'federalTaxID'
        ## y 'nif_cif'; siendo por defecto cadenas vacías:
        $array_customer_data['nif_cif'] = '';
        $array_customer_data['federalTaxID'] = '';
        $array_customer_data['id_customer'] = (int)$array_customer_data['id_customer'];

        return $array_customer_data;
    }

    /**
     * Función que devuelve el ID de la dirección por defecto de un cliente (customer);
     * esto es, en este caso la primera dirección que coincida con el primer alias de
     * dirección que haya creado (aunque se hubiere modificado), pero que esté actualmente
     * y en cualquier caso activa y no eliminada.
     *
     * @param int $id_customer (Identificador del cliente (customer))
     * @return int (Se devuelve el ID de la dirección; 0 si no existe.)
     */
    public static function getInitialCustomerAddressId(int $id_customer): int
    {
        return JlcedemoTools::getInitialCustomerAddressId($id_customer);
    }

    /**
     * Esta función devuelve el valor del campo 'dni' de la tabla 'ps_address',
     * para un 'id_address' dado recibido por parámetro.
     *
     * @param int $id_address (El ID de la dirección)
     * @return string (El NIF/CIF (campo 'dni') relacionado con la dirección)
     */
    public static function getDNIAddress(int $id_address): string
    {
        $sqlQuery = /** @lang MySQLQuery */
            "SELECT dni
            FROM " . _DB_PREFIX_ . "address
            WHERE id_address = $id_address";
        $nif_cif = Db::getInstance()->getValue($sqlQuery);
        return !is_null($nif_cif) ? JlcedemoTools::trimCadena($nif_cif) : '';
    }

    /**
     * Esta función devuelve el FederalTaxID (equivale a un VAT Number)
     * de una determinada dirección de un cliente (customer).
     *
     * @param int $id_address (El ID de la dirección en la que buscar la información)
     * @return string (El FederalTaxID del customer para la dirección seleccionada)
     */
    public static function getFederalTaxID_CustomerAddress(int $id_address): string
    {
        $sql_address = /** @lang MySQLQuery */
            "SELECT
                dir.vat_number,
                dir.dni,
                ctr.iso_code
            FROM " . _DB_PREFIX_ . "address dir
            INNER JOIN " . _DB_PREFIX_ . "country ctr
                ON ctr.id_country = dir.id_country
            WHERE dir.id_address = $id_address";
        $queryRes = Db::getInstance()->getRow($sql_address);

        ## Resultado esperado:
        if (!empty($queryRes["iso_code"])) {
            ## Se conforma un FederalTaxID:
            return self::formFederalTaxID(
                $queryRes['iso_code'], $queryRes['dni'], $queryRes['vat_number']
            );
        }

        ## Resultado NO esperado:
        return '';
    }

    /**
     * Con esta función se obtienen ciertos datos del carrier (como su nombre, p.ej.)
     *
     * @param int $id_carrier (Identificador del carrier de PrestaShop)
     * @return array (Se devuelven los datos en formato arreglo)
     */
    public static function getSomeCarrierData(int $id_carrier): array
    {
        $sql_carrier = /** @lang MySQLQuery */
            "SELECT
                crr.id_carrier,
                crr.id_reference,
                crr.name,
                crr.shipping_handling gastos_manipulacion,
                crr.active,
                crr.is_free gratis,
                crrlng.delay
            FROM " . _DB_PREFIX_ . "carrier crr
            INNER JOIN " . _DB_PREFIX_ . "carrier_lang crrlng
                ON crrlng.id_carrier = crr.id_carrier
                AND crrlng.id_lang = 1
            WHERE crr.id_carrier = $id_carrier";
        $array_carrier_data = Db::getInstance()->getRow($sql_carrier);

        $array_datos_boleanos = ['active', 'gastos_manipulacion', 'gratis'];
        foreach ($array_datos_boleanos as $dato_boleano) {
            if ($array_carrier_data[$dato_boleano] === '0') {
                $array_carrier_data[$dato_boleano] = false;
            } else {
                $array_carrier_data[$dato_boleano] = true;
            }
        }

        return JlcedemoTools::arrayValuesToNumeric($array_carrier_data, $array_datos_boleanos);
    }

    /**
     * @param int $id_order (Identificador del pedido)
     * @param int $id_cart (Identificador del carrito relacionado con el pedido anterior)
     * @return array (Se devuelven los cupones relacionados con el pedido recibido por parámetro)
     * @throws PrestaShopDatabaseException
     */
    public static function getOrderVouchers(int $id_order, int $id_cart): array
    {
        return JlcedemoTools::getOrderVouchers($id_order, $id_cart);
    }

    /**
     * Función personalizada para actualizar el stock de un producto (todos sus hijos).
     * Se tiene en cuenta el tipo de producto y se busca el SKU (ItemCode) en campos
     * venidos de SAP B1 como el 'ForeignName', por ejemplo...
     *
     * @param array $productArray (Array de datos de un artículo)
     * @param bool $withIdProductAttrs (Por defecto 'false'. Si se pasa un 'true' el update
     * se hará en base a identificadores de artículos dados en el array de artículo del
     * primer parámetro)
     * @param array $tempCarts (Por defecto un array vacío. Si se recibe un array de carritos
     * temporales, se tendrá en cuenta a la hora registrar el stock del artículo)
     * @return bool|null (Se devuelve 'true' cuando el stock del artículo es actualizado o
     * 'false' si no ha sido posible hacerlo. Se devuelve 'null' si el producto no existe)
     * @throws PrestaShopException
     */
    public static function updateItemStock(
        array $productArray,
        bool  $withIdProductAttrs = false,
        array $tempCarts = []
    ): ?bool
    {
        ## Siempre que el método de actualización de stocks no se base en identificadores de atributos
        ## ya pasados por el array del primer parámetro:
        if (!$withIdProductAttrs) {
            ## El SKU del artículo no puede valer ciertos valores:
            $sku_no_validas = ['', 'null', '0', '-1', null, 0, -1];

            ## Determinación de la referencia sku del artículo (tanto la nueva, como la antigua (de haberla)):
            $sku_reference = '';
            if (array_key_exists('ItemCode', $productArray) && !in_array(trim($productArray['ItemCode']), $sku_no_validas, true)) {
                $sku_reference = trim($productArray['ItemCode']);
            }

            $reference_old = $sku_reference;
            if (array_key_exists('U_SEICodAntiguo', $productArray) && !in_array(trim($productArray['U_SEICodAntiguo']), $sku_no_validas, true)) {
                $reference_old = trim($productArray['U_SEICodAntiguo']);
            } elseif (array_key_exists('ForeignName', $productArray) && !in_array(trim($productArray['ForeignName']), $sku_no_validas, true)) {
                $reference_old = trim($productArray['ForeignName']);
            }

            ## Cantidad en stock:
            $available_stock = $productArray['QuantityOnStock'] * 1;
            if ($reference_old === $sku_reference) $reference_old = false;

            ## Se procede a determinar el ID del producto (padre) y el ID del artículo en cuestión (la combinación):
            $sql_ids_producto_y_prodattr = /** @lang MySQL DB query */
                "SELECT id_product, id_product_attribute
                FROM " . _DB_PREFIX_ . "product_attribute
                WHERE reference = '$sku_reference'";
            $ids_producto_y_prodattr = Db::getInstance()->getRow($sql_ids_producto_y_prodattr);
            if ($ids_producto_y_prodattr === false && $reference_old !== false) {
                $sql_ids_producto_y_prodattr = /** @lang MySQL DB query */
                    "SELECT id_product, id_product_attribute
                    FROM " . _DB_PREFIX_ . "product_attribute
                    WHERE reference = '$reference_old'";
                $ids_producto_y_prodattr = Db::getInstance()->getRow($sql_ids_producto_y_prodattr);
            }

            ## Si en este punto aún no se han obtenido producto padre e hijo:
            if ($ids_producto_y_prodattr === false) return null;

            ## Declaración de los identificadores de producto y atributos:
            $id_product = (int)$ids_producto_y_prodattr['id_product'];
            $id_product_attribute = (int)$ids_producto_y_prodattr['id_product_attribute'];
        } else {
            ## Declaración de los identificadores de producto y atributos:
            $id_product = (int)$productArray['IdProduct'];
            $id_product_attribute = (int)$productArray['IdProductAttr'];
            $available_stock = $productArray['QuantityOnStock'] * 1;
        }

        ## Si se ha recibido un array NO vacío con un conjunto de carritos temporales a tener en cuenta:
        if (!empty($tempCarts)) {
            ## Se recorre cada solicitud temporal:
            foreach ($tempCarts as $eachTmpCart) {
                ## Siempre que se hable del mismo 'id_product_attribute':
                if ((int)$eachTmpCart['id_product_attribute'] === $id_product_attribute) {
                    ## La cantidad recibida debe minorarse con las solicitadas:
                    $available_stock -= ($eachTmpCart['quantity'] * 1);
                }
            }
        }

        ## No se puede recibir una cantidad en stock inferior a 0. Así se tiene que:
        if ($available_stock < 0) $available_stock = 0;

        ## Si el array del primer parámetro contiene un precio de artículo:
        if (!empty($productArray['ItemPrice'])) $itemPrice = round($productArray['ItemPrice'], 6);

        ## En este punto se obtienen las características del presente artículo:
        $mySQLQuery = /** @lang MySQL DB query */
            "SELECT
                PEIF.item_reference,
                PEIF.item_features
            FROM " . _DB_PREFIX_ . "jlcedemo_items_features PEIF
            INNER JOIN " . _DB_PREFIX_ . "product_attribute PPA
                ON PPA.reference = PEIF.item_reference
                AND PPA.id_product = PEIF.id_product
            WHERE PPA.id_product = $id_product
            AND PPA.id_product_attribute = $id_product_attribute";
        $itemFeatsRow = Db::getInstance()->getRow($mySQLQuery);
        if (empty($itemFeatsRow['item_features'])) return false;
        $itemFeaturesArr = json_decode($itemFeatsRow['item_features'], true);

        ## Del arreglo de características se declaran:
        $itemType = $itemFeaturesArr['UTM'];
        $unitWeight = $itemFeaturesArr['UnitWeight'] * 1;
        $frmtConvRatio = 1;

        ## Para los artículos de tipo decimale ('ML', 'M2', 'M3'):
        if ($itemType !== 'NA') {
            ## El ratio de conversión de formato es el peso de 1 metro (o metro cuadrado) de producto, en gramos:
            $frmtConvRatio = $unitWeight * 1000;
            ## El stock disponible va a ser lo obtenido de multiplicar los metros (o m2) de producto disponibles,
            ## por su ratio de conversión de formato correspondiente; esto es, si tenemos, por ejemplo, 5 metros
            ## de producto en SAP y cada metro pesa 1500 gramos (1,5 Kgs), entonces tendremos un stock disponible
            ## de 7500 gramos. Así, se tiene que:
            $available_stock *= $frmtConvRatio;
        }

        ## Se actualiza el stock disponible del artículo en concreto:
        StockAvailable::setQuantity($id_product, $id_product_attribute, $available_stock);

        ## En el caso de que esté definido un precio y que sea mayor que 0, se
        ## procede también a actualizar el precio de la presente combinación:
        if (!empty($itemPrice)) {
            ## Según el tipo de artículo (por medidas o por unidades):
            if ($itemType !== 'NA') {
                /*
                 * De salida se hace necesario obtener tanto el precio base del producto (precio por gramo),
                 * como el precio base del producto por unidad de medida (precio por metro (o m2)).
                 * Además se necesita llegar a obtener el impacto de la combinación sobre el precio base por
                 * gramo y el impacto sobre el precio base por unidad de medida.
                 *
                 * Declarando las siguientes variables como:
                 * prBsPrd = precio base producto (precio por gramo del producto padre)
                 * prBsCmb = precio base combinación (precio por gramo de la combinación (artículo hijo))
                 * prBsPrdUdMd = precio base producto por unidad de medida (m o m2)
                 * itemPrice = precio base combinación por ud. de medida (m o m2)
                 * imPrCmb = impacto de precio combinación (por gramo)
                 * imPrCmbUdMd = impacto de precio combinación por unidad de medida (m o m2)
                 * frmtConvRatio = ratio de factor de conversión (cantidad en gramos de un metro (o m2) de producto)
                 * rtPrUdMd = ratio de precio de unidad de medida (m o m2) respecto a precio base
                 *
                 * Se tiene que:
                 * prBsPrdUdMd = prBsPrd / rtPrUdMd;
                 * itemPrice = prBsCmb * frmtConvRatio;
                 * prBsCmb = itemPrice / frmtConvRatio;
                 *
                 * imPrCmbUdMd = itemPrice - ((prBsCmb / prBsPrd) * prBsPrdUdMd);
                 * imPrCmb = prBsCmb - prBsPrd;
                 */

                ## Teniendo en cuenta lo anteriormente comentado, se busca obtener el 'prBsPrd' y el 'rtPrUdMd':
                $mySQLQuery = /** @lang MySQL DB query */
                    "SELECT price, unit_price_ratio
                    FROM " . _DB_PREFIX_ . "product
                    WHERE id_product = $id_product";
                $resRow = Db::getInstance()->getRow($mySQLQuery);
                if (empty($resRow['price'])) return false;
                $prBsPrd = $resRow['price'] * 1; // Precio base del producto (precio por gramo)
                $rtPrUdMd = $resRow['unit_price_ratio'] * 1;
                ## De lo anterior se calcula el 'prBsPrdUdMd':
                $prBsPrdUdMd = $prBsPrd / $rtPrUdMd; // Precio base del producto por ud. medida (por m o m2)

                ## Ahora es necesario obtener el precio base de la combinación por gramo, el 'prBsCmb':
                $prBsCmb = $itemPrice / $frmtConvRatio;

                ## Impacto en el precio de la combinación por gramo:
                $imPrCmb = $prBsCmb - $prBsPrd;
                $imPrCmb = JlcedemoTools::toFixed($imPrCmb, 6);

                ## Impacto del precio de la combinación por unidad de medida:
                if (empty($imPrCmb)) {
                    $imPrCmbUdMd = 0;
                } else {
                    $imPrCmbUdMd = $itemPrice - (($prBsCmb / $prBsPrd) * $prBsPrdUdMd);
                    $imPrCmbUdMd = JlcedemoTools::toFixed($imPrCmbUdMd, 6);
                }

                ## Por último se define el array de actualización de los precios:
                $updData = ["price" => $imPrCmb, "unit_price_impact" => $imPrCmbUdMd];
            } else {
                ## En este caso lo que hay que tener en cuenta es que se deben actualizar los impactos en
                ## precio sin más; por tanto, lo primero es obtener el precio de base del producto (padre):
                $mySQLQuery = /** @lang MySQL DB query */
                    "SELECT price
                    FROM " . _DB_PREFIX_ . "product
                    WHERE id_product = $id_product";
                $resPrice = Db::getInstance()->getValue($mySQLQuery);
                $prBsPrd = $resPrice * 1; // Precio base del producto (padre)
                $imPrCmb = $itemPrice - $prBsPrd; // Impacto en el precio del presente artículo (combinación)
                $imPrCmb = JlcedemoTools::toFixed($imPrCmb, 6);

                ## Por último se define el array de actualización del precio:
                $updData = ["price" => $imPrCmb];
            }

            ## Se proceden a actualizar las tablas 'ps_product_attribute' y la 'ps_product_attribute_shop':
            $priceUpd = Db::getInstance()->update(
                'product_attribute', $updData,
                "id_product_attribute = $id_product_attribute AND id_product = $id_product"
            );
            if ($priceUpd) {
                $priceShopUpd = Db::getInstance()->update(
                    'product_attribute_shop', $updData,
                    "id_product_attribute = $id_product_attribute AND id_product = $id_product"
                );

                ## Resultado NO esperado:
                if (!$priceShopUpd) {
                    PrestaShopLogger::addLog(
                        "El Id de combinación $id_product_attribute (producto con Id $id_product),"
                        . " NO ha podido actualizar del todo su precio",
                        3
                    );
                    return false;
                }

                ## Se guarda el resultado de la actualización del precio:
                (new Combination($id_product_attribute))->save();
            } else {
                ## Para un resultado NO esperado:
                PrestaShopLogger::addLog(
                    "El Id de combinación $id_product_attribute (producto con Id $id_product),"
                    . " NO ha podido actualizar su precio",
                    3
                );
                return false;
            }
        }

        return true;
    }

    /**
     * Función que actualiza el estado de un pedido al recibido por segundo parámetro.
     *
     * @param int|string $order_code (El ID de pedido o la referencia de pedido de PrestaShop)
     * @param int $id_order_state (El ID del estado al que se busca cambiar)
     * @param array $target_states (Sólo se va a actuar con los pedidos que estén en alguno de estos estados)
     * @return bool (Se devuelve 'true' si se ha cambiado el estado del pedido)
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public static function updateOrderState($order_code, int $id_order_state, array $target_states): bool
    {
        ## En caso de que el parámetro '$order' no sea numérico, se está ante una referencia de pedido.
        ## A partir de ahí se busca obtener el 'id_order' relacionado:
        if (!is_numeric($order_code) && is_string($order_code)) {
            $mysql_query = /** @lang MySQL DB query */
                "SELECT id_order
                FROM " . _DB_PREFIX_ . "orders
                WHERE reference = '$order_code'
                ORDER BY id_order DESC";
            $id_order = Db::getInstance()->getValue($mysql_query);
            if (!$id_order) return false; // Resultado no esperado

            ## Para un resultado esperado, en este caso, y habiendo recibido una referencia de pedido,
            ## lo que se hace es actualizar el estado directamente sin más comprobaciones:
            $order_code = (int)$id_order;
            (new Order($order_code))->setCurrentState($id_order_state);
            return true;
        }

        ## Se crea instancia de Order con el ID parametrizado:
        $thisOrder = new Order($order_code);

        ## Se obtiene su estado actual:
        $current_state = $thisOrder->getCurrentState();

        ## Si el estado actual del pedido se encuentra en uno de los estados parametrizados:
        if (in_array($current_state, $target_states)) {
            ## Se actualiza el estado del pedido de PS al pasado por segundo parámetro:
            $thisOrder->setCurrentState($id_order_state);
            return true;
        }

        return false;
    }

    /**
     * @param false $delNoActiveProds (Por defecto 'false'. Si se pasa 'true'
     * se eliminan todos aquellos productos que estén desactivados)
     * @return array (Se devuelve el array con el listado de referencias
     * activas en la tienda)
     * @throws PrestaShopDatabaseException
     */
    public static function getActiveProductsList(bool $delNoActiveProds = false): array
    {
        $activePSProductsArr = [];

        $getActiveProdsQuery = /** @lang MySQL DB query */
            "SELECT id_product, reference
            FROM " . _DB_PREFIX_ . "product
            WHERE active = 1
            AND reference NOT LIKE 'SRVIMP3D%'
            AND reference NOT LIKE 'DUMMY%'
            AND reference NOT LIKE 'S140101%'";

        $delNoActiveProdsQuery = /** @lang MySQL DB query */
            "DELETE FROM " . _DB_PREFIX_ . "product
            WHERE active = 0
            AND reference NOT LIKE 'SRVIMP3D%'
            AND reference NOT LIKE 'DUMMY%'
            AND reference NOT LIKE 'S140101%'";

        ## Eliminación de productos no activos si se busca hacerlo:
        if ($delNoActiveProds) Db::getInstance()->execute($delNoActiveProdsQuery);

        ## Se lanza la consulta para obtener los productos activos, sus referencias:
        $getActiveProdsArr = Db::getInstance()->executeS($getActiveProdsQuery);

        foreach ($getActiveProdsArr as $thisActiveProd) {
            ## Se conforma el array con el listado de referencias activas:
            $activePSProductsArr[$thisActiveProd['id_product']] = $thisActiveProd['reference'];
        }

        ## Se devuelve el listado buscado en formato array:
        return $activePSProductsArr;
    }

    /**
     * @return array (Se devuelve el array con el listado de referencias
     * hijas (las combinaciones de productos) con padres activos. En este
     * listado si se encuentran retales o recortes, también se van a mostrar
     * sus atributos largo y ancho)
     * @throws PrestaShopDatabaseException
     */
    public static function getActiveItemsList(): array
    {
        $getActiveItemsQuery = /** @lang MySQL DB query */
            "SELECT
                " . _DB_PREFIX_ . "product_attribute.id_product \"id_product\",
                " . _DB_PREFIX_ . "product_attribute.id_product_attribute \"id_product_attribute\",
                " . _DB_PREFIX_ . "product_attribute.reference \"reference\",
                CONCAT(
                    '{',
                    GROUP_CONCAT(
                        CONCAT('\"', " . _DB_PREFIX_ . "attribute_group_lang.name, '\":', " . _DB_PREFIX_ . "attribute_lang.name)
                        ORDER BY " . _DB_PREFIX_ . "attribute_group_lang.name
                    ),
                    '}'
                ) \"cutting_long_width\",
                CASE
                    WHEN (
                        SELECT COUNT(*)
                        FROM " . _DB_PREFIX_ . "feature_product
                        INNER JOIN " . _DB_PREFIX_ . "product PROD2
                            ON PROD2.id_product = " . _DB_PREFIX_ . "feature_product.id_product
                        WHERE PROD2.reference = SUBSTRING(" . _DB_PREFIX_ . "product.reference, 1, CHAR_LENGTH(" . _DB_PREFIX_ . "product.reference) - 3)
                        AND " . _DB_PREFIX_ . "feature_product.id_feature = 2
                        AND " . _DB_PREFIX_ . "feature_product.id_feature_value = 115
                    ) > 0 THEN 'M3'
                    WHEN (
                        SELECT COUNT(*)
                        FROM " . _DB_PREFIX_ . "feature_product
                        INNER JOIN " . _DB_PREFIX_ . "product PROD2
                            ON PROD2.id_product = " . _DB_PREFIX_ . "feature_product.id_product
                        WHERE PROD2.reference = SUBSTRING(" . _DB_PREFIX_ . "product.reference, 1, CHAR_LENGTH(" . _DB_PREFIX_ . "product.reference) - 3)
                        AND " . _DB_PREFIX_ . "feature_product.id_feature = 2
                        AND " . _DB_PREFIX_ . "feature_product.id_feature_value = 9
                    ) > 0 THEN 'M2'
                    ELSE 'ML'
                END \"udm\"
            FROM " . _DB_PREFIX_ . "product_attribute
            LEFT JOIN " . _DB_PREFIX_ . "product
                ON " . _DB_PREFIX_ . "product.id_product = " . _DB_PREFIX_ . "product_attribute.id_product
            LEFT JOIN " . _DB_PREFIX_ . "product_attribute_combination
                ON " . _DB_PREFIX_ . "product_attribute_combination.id_product_attribute = " . _DB_PREFIX_ . "product_attribute.id_product_attribute
            LEFT JOIN " . _DB_PREFIX_ . "attribute
                ON " . _DB_PREFIX_ . "attribute.id_attribute = " . _DB_PREFIX_ . "product_attribute_combination.id_attribute
            LEFT JOIN " . _DB_PREFIX_ . "attribute_group_lang
                ON " . _DB_PREFIX_ . "attribute_group_lang.id_attribute_group = " . _DB_PREFIX_ . "attribute.id_attribute_group
                AND " . _DB_PREFIX_ . "attribute_group_lang.id_lang = 1
            LEFT JOIN " . _DB_PREFIX_ . "attribute_lang
                ON " . _DB_PREFIX_ . "attribute_lang.id_attribute = " . _DB_PREFIX_ . "attribute.id_attribute
                AND " . _DB_PREFIX_ . "attribute_lang.id_lang = 1
            WHERE " . _DB_PREFIX_ . "product.active = 1
            AND (
                " . _DB_PREFIX_ . "product_attribute.reference LIKE '%RET'
                OR
                " . _DB_PREFIX_ . "product_attribute.reference LIKE '%REC'
            )
            AND (
                " . _DB_PREFIX_ . "attribute_group_lang.name = 'Largo/Alto'
                OR
                " . _DB_PREFIX_ . "attribute_group_lang.name = 'Ancho'
            )
            GROUP BY " . _DB_PREFIX_ . "product_attribute.id_product,
                " . _DB_PREFIX_ . "product_attribute.id_product_attribute,
                " . _DB_PREFIX_ . "product_attribute.reference
            UNION
            SELECT
                " . _DB_PREFIX_ . "product_attribute.id_product \"id_product\",
                " . _DB_PREFIX_ . "product_attribute.id_product_attribute \"id_product_attribute\",
                " . _DB_PREFIX_ . "product_attribute.reference \"reference\",
                NULL \"cutting_long_width\",
                CASE
                    WHEN (
                        SELECT COUNT(*)
                        FROM " . _DB_PREFIX_ . "feature_product
                        WHERE " . _DB_PREFIX_ . "feature_product.id_product = " . _DB_PREFIX_ . "product.id_product
                        AND " . _DB_PREFIX_ . "feature_product.id_feature = 2
                        AND " . _DB_PREFIX_ . "feature_product.id_feature_value = 115
                    ) > 0 THEN 'M3'
                    WHEN (
                        SELECT COUNT(*)
                        FROM " . _DB_PREFIX_ . "feature_product
                        WHERE " . _DB_PREFIX_ . "feature_product.id_product = " . _DB_PREFIX_ . "product.id_product
                        AND " . _DB_PREFIX_ . "feature_product.id_feature = 2
                        AND " . _DB_PREFIX_ . "feature_product.id_feature_value = 9
                    ) > 0 THEN 'M2'
                    WHEN (
                        SELECT COUNT(*)
                        FROM " . _DB_PREFIX_ . "feature_product
                        WHERE " . _DB_PREFIX_ . "feature_product.id_product = " . _DB_PREFIX_ . "product.id_product
                        AND " . _DB_PREFIX_ . "feature_product.id_feature = 2
                        AND " . _DB_PREFIX_ . "feature_product.id_feature_value = 8
                    ) > 0 THEN 'ML'
                    ELSE 'NA'
                END \"udm\"
            FROM " . _DB_PREFIX_ . "product_attribute
            LEFT JOIN " . _DB_PREFIX_ . "product
                ON " . _DB_PREFIX_ . "product.id_product = " . _DB_PREFIX_ . "product_attribute.id_product
            WHERE " . _DB_PREFIX_ . "product.active = 1
            AND " . _DB_PREFIX_ . "product_attribute.reference NOT LIKE '%RET'
            AND " . _DB_PREFIX_ . "product_attribute.reference NOT LIKE '%REC'
            AND " . _DB_PREFIX_ . "product.reference NOT LIKE 'SRVIMP3D%'
            AND " . _DB_PREFIX_ . "product.reference NOT LIKE 'DUMMY%'
            AND " . _DB_PREFIX_ . "product.reference NOT LIKE 'S140101%'";

        ## Se lanza la consulta para obtener los productos hijos activos, sus referencias:
        $getActiveItemsArr = Db::getInstance()->executeS($getActiveItemsQuery);

        ## Se formatea correctamente el valor contenido en los posibles atributos de largo y ancho (retales y recortes):
        if (is_array($getActiveItemsArr) && !empty($getActiveItemsArr)) {
            foreach ($getActiveItemsArr as $thisIndex => $itemArray) {
                if (isset($itemArray['cutting_long_width'])) {
                    $thisAttrsStr = $itemArray['cutting_long_width'];
                    $thisAttrArray = json_decode($thisAttrsStr, true);
                    $getActiveItemsArr[$thisIndex]['cutting_long_width'] = $thisAttrArray;
                }
            }

            return $getActiveItemsArr;
        }

        ## Si se llega hasta aquí, se devuelve un array vacío:
        return [];
    }

    /**
     * @param string|null $prodReference (Por defecto 'null'. Si se recibe
     * una referencia de producto (padre), se filtrará la consulta por ella)
     * @return array (Se devuelve el array con el listado de referencias
     * hijas (las combinaciones de productos) con padres tipo 'D' activos)
     * @throws PrestaShopDatabaseException
     */
    public static function getActiveDTypeItemsLst(?string $prodReference = null): array
    {
        $productFilter = "";
        if (!empty($prodReference)) {
            $productFilter .= "AND " . _DB_PREFIX_ . "product.reference = $prodReference";
        }

        $getActiveItemsQuery = /** @lang MySQL DB query */
            "SELECT
                " . _DB_PREFIX_ . "product_attribute.id_product \"id_product\",
                " . _DB_PREFIX_ . "product_attribute.id_product_attribute \"id_product_attribute\",
                " . _DB_PREFIX_ . "product_attribute.reference \"reference\",
                CASE
                    WHEN (
                        SELECT COUNT(*)
                        FROM " . _DB_PREFIX_ . "feature_product
                        WHERE " . _DB_PREFIX_ . "feature_product.id_product = " . _DB_PREFIX_ . "product.id_product
                        AND " . _DB_PREFIX_ . "feature_product.id_feature = 2
                        AND " . _DB_PREFIX_ . "feature_product.id_feature_value = 115
                    ) > 0 THEN 'M3'
                    WHEN (
                        SELECT COUNT(*)
                        FROM " . _DB_PREFIX_ . "feature_product
                        WHERE " . _DB_PREFIX_ . "feature_product.id_product = " . _DB_PREFIX_ . "product.id_product
                        AND " . _DB_PREFIX_ . "feature_product.id_feature = 2
                        AND " . _DB_PREFIX_ . "feature_product.id_feature_value = 9
                    ) > 0 THEN 'M2'
                    ELSE 'ML'
                END \"udm\"
            FROM " . _DB_PREFIX_ . "product_attribute
            LEFT JOIN " . _DB_PREFIX_ . "product
                ON " . _DB_PREFIX_ . "product.id_product = " . _DB_PREFIX_ . "product_attribute.id_product
            WHERE " . _DB_PREFIX_ . "product.active = 1
            $productFilter
            AND 0 < (
                SELECT COUNT(*)
                FROM " . _DB_PREFIX_ . "feature_product PFP
                WHERE PFP.id_product = " . _DB_PREFIX_ . "product.id_product
                AND PFP.id_feature = 2
                AND PFP.id_feature_value IN (8, 9, 115)
            )";

        ## Se lanza la consulta para obtener los productos hijos activos, sus referencias:
        $getActiveItemsArr = Db::getInstance()->executeS($getActiveItemsQuery);

        ## Siempre que se obtenga un array de datos:
        if (is_array($getActiveItemsArr) && !empty($getActiveItemsArr)) return $getActiveItemsArr;

        ## Si se llega hasta aquí, se devuelve un array vacío:
        return [];
    }

    /**
     * Esta función chequea si un producto determinado tiene alguna image subida.
     *
     * @param string $reference (El código de referencia del producto)
     * @return bool (Se devuelve 'true' si el producto existe y tiene alguna
     * imagen, o 'false' si existe y no tiene imágenes o si no existe siquiera)
     */
    public static function checkProdImage(string $reference): bool
    {
        $mySQLQuery = /** @lang MySQL DB query */
            "SELECT COUNT(*)
            FROM " . _DB_PREFIX_ . "product
            LEFT JOIN " . _DB_PREFIX_ . "image
                ON " . _DB_PREFIX_ . "image.id_product = " . _DB_PREFIX_ . "product.id_product
            WHERE " . _DB_PREFIX_ . "product.reference = '$reference'
            AND " . _DB_PREFIX_ . "image.id_image IS NOT NULL";
        $imgsCount = Db::getInstance()->getValue($mySQLQuery);
        if ($imgsCount && (int)$imgsCount > 0) return true;

        return false;
    }

    /**
     * Esta función crea o actualiza un producto de PrestaShop a partir de un array
     * (que representa al producto que se busca crear o actualizar) pasado por parámetro.
     *
     * @param array $product_array (Array que representa al producto que se busca crear (o actualizar))
     * @param bool $delete_product_combinations (Por defecto 'false'. Si se pasa un 'true', tras la
     * creación o actualización del presente producto, se van a eliminar todas sus combinaciones)
     * @param bool $saveProduct (Por defecto 'false'. Si se recibe un 'true' se lanza un guardado al
     * final del proceso)
     * @return array|string (Se devuelve un array con diversas propiedades del producto si ha podido
     * ser creado (o actualizado). En cualquier otro caso se devuelve un 'string' con los posibles
     * errores del proceso)
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public static function createOrUpdateProduct(
        array $product_array,
        bool  $delete_product_combinations = false,
        bool  $saveProduct = false
    )
    {
        ## Se definen unas variables básicas iniciales:
        $errores = '';
        $context = Context::getContext();
        $default_language_id = (int)Configuration::get('PS_LANG_DEFAULT');
        $id_lang = $default_language_id;
        $shop_ids = Shop::getCompleteListOfShopsID();
        $shop_is_feature_active = Shop::isFeatureActive();
        $thisPrdctRef = array_key_exists('reference', $product_array) && !empty(trim($product_array['reference'])) ? trim($product_array['reference']) : '---';
        $thisProdName = array_key_exists('name', $product_array) && !empty(trim($product_array['name'])) ? trim($product_array['name']) : '---';
        $thisProdInf = $thisPrdctRef !== '---' ? $thisPrdctRef : $thisProdName;

        ## Según el tipo de producto, se tiene que:
        if ($product_array['prod_type'] === "NA") {
            ## Productos de venta por unidades:
            $productUnitWeight = $product_array["unit_weight"] * 1;
        } else {
            ## Productos de venta por medidas (ML, M2, M3):
            $productUnitPrice = $product_array["unit_price"] * 1;
        }

        ## Se busca crear o actualizar el producto partiendo del código de referencia aportado:
        $reference_old = false; // Por defecto no hay referencia antigua
        $sku_no_validas = ['', 'null', '0', '-1', null, 0, -1]; // El SKU del artículo no puede valer esos valores
        ## Si efectivamente se pasa una referencia antigua:
        if (array_key_exists('reference_old', $product_array) && !in_array(trim($product_array['reference_old']), $sku_no_validas, true)) {
            $reference_old = trim($product_array['reference_old']);
        }

        if (array_key_exists('reference', $product_array)) {
            $datas = Db::getInstance()->getRow('SELECT p.id_product FROM '
                . _DB_PREFIX_ . 'product p ' . Shop::addSqlAssociation('product', 'p')
                . ' WHERE p.reference = "' . pSQL($thisPrdctRef) . '"', false);
            ## Si no se obtiene resultado por campo de referencia convencional, se busca por referencia antigua:
            if ($datas === false && $reference_old !== false) {
                $datas_sql = 'SELECT p.id_product FROM '
                    . _DB_PREFIX_ . 'product p ' . Shop::addSqlAssociation('product', 'p')
                    . ' WHERE p.reference = "' . pSQL($reference_old) . '"';
                $datas_sql_ref_old = 'SELECT p.id_product FROM '
                    . _DB_PREFIX_ . 'product p ' . Shop::addSqlAssociation('product', 'p')
                    . ' WHERE p.reference_old = "' . pSQL($reference_old) . '"';
                $datas = Db::getInstance()->getRow($datas_sql, false) ?: Db::getInstance()->getRow($datas_sql_ref_old, false);
            }
            if (isset($datas['id_product']) && $datas['id_product']) {
                $product = new Product((int)$datas['id_product']);
            } else {
                $product = new Product();
            }
        } else {
            $errores .= "¡¡ ERROR FATAL: NO es posible crear (o actualizar) el producto $thisProdInf sin indicar un código de referencia en este caso !! ··· ";
            return trim($errores);
        }

        $update_advanced_stock_management_value = false;
        $productExistsInDatabase = false;
        if (isset($product->id) && $product->id && Product::existsInDatabase((int)$product->id, 'product')) {
            $product->loadStockData();
            $update_advanced_stock_management_value = true;
            $productExistsInDatabase = true;
        }

        ## Según el tipo de producto recibido, lo primero es determinar el precio por cada gramo
        ## y el precio unitario por unidad de medida (m, m2). El peso unitario por unidad de
        ## medida, ayudará a determinar el ratio de conversión de formato (a gramos) del producto:
        $prod_type = $product_array["prod_type"] ?? "NA";
        unset($product_array["prod_type"]);
        $frmtConvRatio = 1;
        if ($prod_type === "NA") {
            ## Productos de venta por unidades:
            $product_array["price"] = $product_array["unit_price"] / $frmtConvRatio;
            unset($product_array["unit_price"]);
            $product_array["weight"] = $product_array["unit_weight"] * 1;
            unset($product_array["unit_weight"]);
        } else {
            ## Productos de venta por medidas (ML, M2, M3):
            $frmtConvRatio = $product_array["unit_weight"] * 1000;
            $product_array["price"] = $product_array["unit_price"] / $frmtConvRatio;
            unset($product_array["unit_weight"]);
            $product_array["weight"] = 0.001; // Siempre 1 gramo
            if ($prod_type === "ML") {
                $product_array["unity"] = "m";
            } else {
                $product_array["unity"] = strtolower($prod_type);
            }
        }

        ## A la clave 'features' del arreglo 'product_array' hay que añadirle siempre el tipo de producto:
        $prodFeats = $product_array["features"] ?? "";
        if (empty($prodFeats)) {
            $product_array["features"] = "Tipo de producto:$prod_type";
        } else {
            $product_array["features"] = "Tipo de producto:$prod_type,,$prodFeats";
        }

        ## Valores por defecto:
        $defaultFeats = [
            'active' => 0,
            'width' => 0.000000,
            'height' => 0.000000,
            'depth' => 0.000000,
            'weight' => 0.000000,
            'visibility' => 'both',
            'additional_shipping_cost' => 0.00,
            'price' => 0,
            'unit_price' => 0,
            'quantity' => 0,
            'minimal_quantity' => 1,
            'show_price' => 1,
            'id_tax_rules_group' => 1,
            'online_only' => 0,
            'condition' => 'new',
            'available_date' => date('Y-m-d'),
            'date_add' => date('Y-m-d H:i:s'),
            'date_upd' => date('Y-m-d H:i:s'),
            'customizable' => 1,
            'uploadable_files' => 0,
            'text_fields' => 1,
            'out_of_stock' => 2,
            'advanced_stock_management' => 0,
            'depends_on_stock' => 0
        ];
        foreach ($defaultFeats as $thisFeature => $thisFeatValue) {
            if (empty($product_array[$thisFeature])) {
                $product_array[$thisFeature] = $thisFeatValue;
            }
        }

        ## Para evitar problemas con funciones como la que sigue, y en el caso de que haya
        ## imágenes en formato string base64 (largas cadenas de texto codificado), se separa
        ## el array que contiene dichas imágenes. Así, se tiene que:
        $images_collection_exists = false;
        $images_collection_array = [];
        if (array_key_exists('images_collection', $product_array)) {
            $images_collection_exists = true;
            $images_collection_array = $product_array['images_collection'];
            unset($product_array['images_collection']);
        }

        ## Nombre, descripción, meta título, etc.:
        $product->reference = $thisPrdctRef;
        $product->name = trim(($product_array["name"] ?? "Test product"));
        if (!empty($product_array["meta_title"])) $product->meta_title = trim($product_array["meta_title"]);
        if (!empty($product_array["description"])) $product->description = trim($product_array["description"]);
        if (!empty($product_array["description_short"])) $product->description_short = trim($product_array["description_short"]);
        if (!empty($product_array["meta_description"])) $product->meta_description = trim($product_array["meta_description"]);
        $product->available_for_order = (bool)$product_array["available_for_order"];
        $product->product_type = ProductType::TYPE_COMBINATIONS; // Todos los productos padre necesitarán hijos (combinaciones)
        $product->customizable = 1; // Todos vendrán con personalización
        $product->text_fields = 1; // Al menos una personalización de tipo texto (la de 'jlce_Measures')
        $product->additional_delivery_times = 0;

        ## Tienda:
        if (!$shop_is_feature_active) {
            $product->shop = (int)Configuration::get('PS_SHOP_DEFAULT');
        } elseif (empty($product->shop)) {
            $product->shop = implode(',,', Shop::getContextListShopID());
        }
        if (!$shop_is_feature_active) {
            $product->id_shop_default = (int)Configuration::get('PS_SHOP_DEFAULT');
        } else {
            $product->id_shop_default = (int)Context::getContext()->shop->id;
        }
        $product->id_shop_list = array();
        foreach (explode(',,', $product->shop) as $shop) {
            if (!empty($shop) && !is_numeric($shop)) {
                $product->id_shop_list[] = Shop::getIdByName($shop);
            } elseif (!empty($shop)) {
                $product->id_shop_list[] = $shop;
            }
        }

        ## Grupo de impuestos:
        $product->id_tax_rules_group = $product_array["id_tax_rules_group"];
        if (!empty($product->id_tax_rules_group)) {
            if (Validate::isLoadedObject(new TaxRulesGroup($product->id_tax_rules_group))) {
                $address = $context->shop->getAddress();
                $tax_manager = TaxManagerFactory::getManager($address, $product->id_tax_rules_group);
                $product_tax_calculator = $tax_manager->getTaxCalculator();
                $product->tax_rate = $product_tax_calculator->getTotalRate();
            } else {
                $errores .= "Producto $thisProdInf ¡¡ ID de grupo de reglas de impuestos no válido. Primero hay que crear un grupo con este ID. !! ··· ";
            }
        }

        ## Fabricante:
        $product->manufacturer = $product_array["manufacturer"] ?? null;
        if (isset($product->manufacturer) && is_numeric($product->manufacturer) && Manufacturer::manufacturerExists((int)$product->manufacturer)) {
            $product->id_manufacturer = (int)$product->manufacturer;
        } elseif (isset($product->manufacturer) && is_string($product->manufacturer) && !empty($product->manufacturer)) {
            if ($manufacturer = Manufacturer::getIdByName($product->manufacturer)) {
                $product->id_manufacturer = (int)$manufacturer;
            } else {
                $manufacturer = new Manufacturer();
                $manufacturer->name = $product->manufacturer;
                $manufacturer->active = true;

                if (($field_error = $manufacturer->validateFields(false, true)) === true &&
                    ($lang_field_error = $manufacturer->validateFieldsLang(false, true)) === true &&
                    $manufacturer->add()
                ) {
                    $product->id_manufacturer = (int)$manufacturer->id;
                    $manufacturer->associateTo($product->id_shop_list);
                } else {
                    $mensaje_error = sprintf(
                        'Producto ' . $thisProdInf . ' ¡¡ NO se ha podido registrar el fabricante %1$s (ID: %2$s) !! ··· ',
                        Tools::htmlentitiesUTF8($manufacturer->name),
                        !empty($manufacturer->id) ? $manufacturer->id : 'null'
                    );
                    $errores .= ($field_error !== true ? $field_error : '')
                        . (isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '') . $mensaje_error;
                }
            }
        }

        ## Proveedor:
        $product->supplier = $product_array["supplier"] ?? null;
        if (isset($product->supplier) && is_numeric($product->supplier) && Supplier::supplierExists((int)$product->supplier)) {
            $product->id_supplier = (int)$product->supplier;
        } elseif (isset($product->supplier) && is_string($product->supplier) && !empty($product->supplier)) {
            if ($supplier = Supplier::getIdByName($product->supplier)) {
                $product->id_supplier = (int)$supplier;
            } else {
                $supplier = new Supplier();
                $supplier->name = $product->supplier;
                $supplier->active = true;

                if (($field_error = $supplier->validateFields(false, true)) === true &&
                    ($lang_field_error = $supplier->validateFieldsLang(false, true)) === true &&
                    $supplier->add()
                ) {
                    $product->id_supplier = (int)$supplier->id;
                    $supplier->associateTo($product->id_shop_list);
                } else {
                    $mensaje_error = sprintf(
                        'Producto ' . $thisProdInf . ' ¡¡ NO se ha podido registrar el proveedor %1$s (ID: %2$s) !! ··· ',
                        Tools::htmlentitiesUTF8($supplier->name),
                        !empty($supplier->id) ? Tools::htmlentitiesUTF8($supplier->id) : 'null'
                    );
                    $errores .= ($field_error !== true ? $field_error : '')
                        . (isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '') . $mensaje_error;
                }
            }
        }

        ## Precio, peso, unidad de medida. Sólo deben establecerse en el producto padre la primera vez, esto es
        ## así para evitar que los impactos en precio (y peso) ya existentes en sus combinaciones, que en esta sincro
        ## NO se incluyan, no hagan que éstas cambien los precios de venta sin quererlo al no verse modificadas:
        if ($productExistsInDatabase) {
            $mySQLQuery = /** @lang MySQL DB query */
                "SELECT price, weight, unity
                FROM " . _DB_PREFIX_ . "product
                WHERE id_product = " . $product->id;
            $resultRow = Db::getInstance()->getRow($mySQLQuery);
            if (!$resultRow) {
                $errores .= "¡¡ ERROR FATAL: NO es posible actualizar el producto $thisProdInf. NO se ha podido determinar su precio actual !! ··· ";
                return trim($errores);
            }

            $product_array["price"] = $resultRow["price"] * 1;
            if ($prod_type === 'NA') {
                ## Venta por unidades:
                $product_array["weight"] = $resultRow["weight"] * 1;
            } else {
                ## Venta por medidas:
                $product_array["unity"] = $resultRow["unity"];
                $product_array["unit_price"] = $resultRow["price"] * $frmtConvRatio;
            }
        }
        $product->price = round($product_array["price"], 6);
        $productPrice = $product->price;
        if (!empty($product_array["unit_price"])) {
            $product->unit_price = $product_array["unit_price"] * 1;
            $productUnitPrice = $product->unit_price;
        }
        $product->weight = $product_array["weight"];
        if ($prod_type === 'NA') $productUnitWeight = $product->weight;
        if (!empty($product_array["unity"])) $product->unity = $product_array["unity"];

        ## Ecotasa:
        if (!Configuration::get('PS_USE_ECOTAX')) $product->ecotax = 0;

        ## Producto activo o no:
        $product->active = (bool)$product_array["active"];

        ## Categorías (asociadas y defecto):
        $product->deleteCategories();
        $customer_groups_array = Group::getGroups($default_language_id);
        $sap_familia_l1_code = false; // Grupo en SAP
        $sap_familia_l2_code = false; // Familia en SAP
        $sap_familia_l3_code = false; // Subfamilia en SAP y último nivel en PS
        $mapear_familia_l1 = false;
        $mapear_familia_l2 = false;
        $mapear_familia_l3 = false;
        $category_assoc_is_num = false;
        ## Primeramente se revisan las claves relacionadas con las familias en SAP. Lo que se busca es
        ## comprobar que los códigos de familias en SAP estén ya mapeados a categorías de PrestaShop:
        if (array_key_exists('sap_familia_l3_code', $product_array)
            && array_key_exists('sap_familia_l2_code', $product_array)
            && array_key_exists('sap_familia_l1_code', $product_array)
            && is_numeric(trim($product_array['sap_familia_l3_code']))
            && is_numeric(trim($product_array['sap_familia_l2_code']))
            && is_numeric(trim($product_array['sap_familia_l1_code']))
        ) {
            ## La familia de SAP de referencia siempre será la de nivel 3 (subfamilia de SAP), por tanto:
            $sap_familia_l3_code = (int)$product_array['sap_familia_l3_code'];
            $sap_familia_l3_name = trim($product_array['sap_familia_l3_name']);

            ## Además se tiene que:
            $sap_familia_l2_code = (int)$product_array['sap_familia_l2_code'];
            $sap_familia_l2_name = trim($product_array['sap_familia_l2_name']);
            $sap_familia_l1_code = (int)$product_array['sap_familia_l1_code'];
            $sap_familia_l1_name = trim($product_array['sap_familia_l1_name']);

            ## Se busca la categoría de PS relacionada:
            $id_ps_category_default = self::getPSCategoryIDFromSAPFamilyID($sap_familia_l3_code);
            ## Si se obtiene un resultado esperado:
            if ($id_ps_category_default && is_numeric($id_ps_category_default)) {
                ## Entonces:
                $category_assoc_is_num = true;
                $product_array['category_assoc'] = $id_ps_category_default;
                $product_array['category_default'] = $id_ps_category_default;

                ## Asociación al árbol de categorías relacionadas, de haberlas:
                $prntCategoryId = self::getCategoryParentIDByCategoryID($id_ps_category_default);
                if ($prntCategoryId) {
                    $prntPrntCategoryId = self::getCategoryParentIDByCategoryID($prntCategoryId);
                    if ($prntPrntCategoryId) {
                        ## Se asocian las categorías padres al producto:
                        $product_array['category_assoc'] .= ",,$prntCategoryId,,$prntPrntCategoryId";
                    } else {
                        ## Se asocia la categoría padre al producto:
                        $product_array['category_assoc'] .= ",,$prntCategoryId";
                    }
                }
            } else {
                ## La NO existencia de la categoría en PrestaShop supone su creación,
                ## con el correspondiente mapeo a familia de SAP. Así, se tendría que:
                $mapear_familia_l3 = true;

                ## Se buscan las categorías padre de PS relacionadas:
                $id_ps_parent_l2_category = self::getPSCategoryIDFromSAPFamilyID($sap_familia_l2_code);
                $id_ps_parent_l1_category = self::getPSCategoryIDFromSAPFamilyID($sap_familia_l1_code);

                ## Cuando ya existe la categoría padre de nivel 2 en PS:
                if ($id_ps_parent_l2_category && is_numeric($id_ps_parent_l2_category)) {
                    ## Se obtiene su nombre:
                    $ps_parent_l2_category_name = self::getCategoryNameByID($id_ps_parent_l2_category);
                } else {
                    ## Cuando NO, se determina su nombre, habiendo que mapearla:
                    $ps_parent_l2_category_name = $sap_familia_l2_name;
                    $mapear_familia_l2 = true;
                }

                ## Cuando ya existe la categoría padre de nivel 1 en PS:
                if ($id_ps_parent_l1_category && is_numeric($id_ps_parent_l1_category)) {
                    ## Se obtiene su nombre:
                    $ps_parent_l1_category_name = self::getCategoryNameByID($id_ps_parent_l1_category);
                } else {
                    ## Cuando NO, se determina su nombre, habiendo que mapearla:
                    $ps_parent_l1_category_name = $sap_familia_l1_name;
                    $mapear_familia_l1 = true;
                }

                ## Y se conforman las claves 'category_assoc' y 'category_default':
                $product_array['category_default'] = "$ps_parent_l1_category_name/$ps_parent_l2_category_name/$sap_familia_l3_name";
                $product_array['category_assoc'] = "$ps_parent_l1_category_name/$ps_parent_l2_category_name/$sap_familia_l3_name";
                $product_array['category_assoc'] .= ",,$ps_parent_l1_category_name/$ps_parent_l2_category_name";
                $product_array['category_assoc'] .= ",,$ps_parent_l1_category_name";
            }
        }

        ## Seguidamente se comprueba la clave 'sap_alt_families' para ver si hay familias alternativas:
        if (!empty($product_array['sap_alt_families'])) {
            $sapAltFamilies = trim($product_array['sap_alt_families']);
            ## Se recorre el array resultante de la cadena que representa a todas esas familias alternativas:
            foreach (explode(',', $sapAltFamilies) as $sap_alt_family) {
                ## Siempre y cuando se vayan obteniendo IDs de familias válidos:
                if (is_numeric($sap_alt_family)) {
                    $sap_alt_family_code = (int)$sap_alt_family;
                    ## Se busca la categoría de PS relacionada:
                    $id_ps_alt_category = self::getPSCategoryIDFromSAPFamilyID($sap_alt_family_code);
                    ## Sólo se asocia el producto a una categoría alternativa, cuando ya existe en PS:
                    if ($id_ps_alt_category && is_numeric($id_ps_alt_category)) {
                        ## Entonces se tiene que:
                        if ($category_assoc_is_num) {
                            ## Se asocia esa categoría al producto:
                            $product_array['category_assoc'] .= ",,$id_ps_alt_category";
                        } else {
                            ## Cuando la asociación de categorías es por nombre:
                            $ps_alt_category_parent_id = self::getCategoryParentIDByCategoryID($id_ps_alt_category);
                            ## Sólo se hace la asociación si hay al menos una categoría padre de referencia válida:
                            if ($ps_alt_category_parent_id) {
                                $ps_alt_category_parent_name = self::getCategoryNameByID($ps_alt_category_parent_id);
                                $ps_alt_category_name = self::getCategoryNameByID($id_ps_alt_category);
                                if ($ps_alt_category_name && $ps_alt_category_parent_name) {
                                    $ps_alt_category = "$ps_alt_category_parent_name/$ps_alt_category_name";
                                    ## Se asocia esa categoría al producto:
                                    $product_array['category_assoc'] .= ",,$ps_alt_category";
                                }
                            }
                        }
                    }
                }
            }
        }

        ## Tras haber conformado el valor para las claves 'category_assoc' y 'category_default', se tiene que:
        if (array_key_exists('category_assoc', $product_array) && !empty(trim($product_array['category_assoc']))) {
            $product_categories_array = explode(',,', trim($product_array['category_assoc']));
            foreach ($product_categories_array as $product_cat) {
                if (is_numeric($product_cat)) {
                    if (Category::categoryExists((int)$product_cat)) {
                        $product->id_category[] = (int)$product_cat;
                    } else {
                        $errores .= 'Producto ' . $thisProdInf . ' ¡¡ NO se encuentra la categoría con ID: ' . (int)$product_cat . ' !! ··· ';
                    }
                } elseif (is_string($product_cat) && !empty(trim($product_cat))) {
                    $category = self::searchByStringPath($default_language_id, trim($product_cat));
                    if ($category['id_category']) {
                        $product->id_category[] = (int)$category['id_category'];
                    } else {
                        ## Crear la categoría:
                        $product_cats = explode('/', trim($product_cat));
                        $parent_categories = [];
                        foreach ($product_cats as $indice => $this_category) {
                            $thisCategory = trim($this_category);
                            $categoria_padre = $indice === 0 ? (int)Configuration::get('PS_HOME_CATEGORY') : (int)$parent_categories[($indice - 1)];
                            $thisCategoryArray = Category::searchByNameAndParentCategoryId($default_language_id, $thisCategory, $categoria_padre);
                            if ($thisCategoryArray['id_category']) {
                                ## Sólo se asocia el producto a la categoría de menor nivel jerárquico:
                                if ($indice === JlcedemoTools::get_array_last_key($product_cats)) $product->id_category[] = (int)$thisCategoryArray['id_category'];
                                $parent_categories[$indice] = (int)$thisCategoryArray['id_category'];
                            } else {
                                $category_to_create = new Category();
                                $category_to_create->name = self::createMultiLangField($thisCategory);
                                $category_to_create->active = 1;
                                $category_to_create->id_parent = $categoria_padre;
                                $category_link_rewrite = Tools::link_rewrite($category_to_create->name[$default_language_id]);
                                $category_to_create->link_rewrite = self::createMultiLangField($category_link_rewrite);
                                ## Añadidura y gestión de errores:
                                if (($field_error = $category_to_create->validateFields(false, true)) === true &&
                                    ($lang_field_error = $category_to_create->validateFieldsLang(false, true)) === true
                                    && $category_to_create->add()
                                ) {
                                    ## Sólo se asocia el producto a la categoría de menor nivel jerárquico:
                                    if ($indice === JlcedemoTools::get_array_last_key($product_cats)) $product->id_category[] = (int)$category_to_create->id;
                                    $parent_categories[$indice] = (int)$category_to_create->id;
                                    ## Asignación de la categoría a crear a todos los grupos de clientes de la tienda:
                                    foreach ($customer_groups_array as $customer_group_array) {
                                        $category_to_create->addGroupsIfNoExist((int)$customer_group_array['id_group']);
                                    }
                                } else {
                                    $mensaje_error = sprintf(
                                        'Producto ' . $thisProdInf . ' ¡¡ NO se ha podido registrar la categoría %1$s (ID: %2$s) !! ··· ',
                                        Tools::htmlentitiesUTF8($category_to_create->name[$default_language_id]),
                                        !empty($category_to_create->id) ? Tools::htmlentitiesUTF8($category_to_create->id) : 'null'
                                    );
                                    $errores .= ($field_error !== true ? $field_error : '')
                                        . (isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '') . $mensaje_error;
                                    $parent_categories[$indice] = (int)Configuration::get('PS_HOME_CATEGORY');
                                }
                            }
                        }
                    }
                } else {
                    $errores .= 'Producto ' . $thisProdInf . ' ¡¡ NO es posible gestionar la categoría ' . $product_cat . ' !! ··· ';
                }
            }
            $product->id_category = array_values(array_unique($product->id_category));
        }

        ## Categoría por defecto:
        if (array_key_exists('category_default', $product_array) && !empty(trim($product_array['category_default']))) {
            $product_category_default = trim($product_array['category_default']);

            if (is_numeric($product_category_default) && Category::categoryExists((int)$product_category_default)) {
                $product->id_category_default = (int)$product_category_default;
            } else {
                $category = self::searchByStringPath($default_language_id, trim($product_category_default));

                if ($category['id_category']) {
                    $product->id_category_default = (int)$category['id_category'];
                } else {
                    $mapear_familia_l1 = false;
                    $mapear_familia_l2 = false;
                    $mapear_familia_l3 = false;
                    $errores .= 'Producto ' . $thisProdInf . ' ¡¡ NO se ha podido registrar la categoría por defecto '
                        . trim($product_category_default) . ' !! ··· ';
                }
            }

            ## Si en este punto se hace necesario el mapeo a familias de SAP:
            if ($mapear_familia_l3 && $sap_familia_l3_code) {
                ## Primero se comprueba que NO exista ya hecho ese mapeo:
                $mapeo_hecho = self::getIDSAPCategory($product->id_category_default);

                if (!$mapeo_hecho) {
                    ## En este punto se procede a realizar el mapeo buscado:
                    self::setIDSAPCategory($product->id_category_default, $sap_familia_l3_code);
                }
            }

            if ($mapear_familia_l2 && $sap_familia_l2_code) {
                ## Se obtiene el padre (de nivel 2) de la categoría por defecto (que es de nivel 3):
                $id_parent_l2_default_category = self::getCategoryParentIDByCategoryID($product->id_category_default);

                ## Ahora se comprueba que NO exista ya hecho ese mapeo:
                $mapeo_hecho = self::getIDSAPCategory((int)$id_parent_l2_default_category);

                if (!$mapeo_hecho) {
                    ## En este punto se procede a realizar el mapeo buscado:
                    self::setIDSAPCategory((int)$id_parent_l2_default_category, $sap_familia_l2_code);
                }
            }

            if ($mapear_familia_l1 && $sap_familia_l1_code) {
                ## Se obtiene el padre (de nivel 1) de la categoría de nivel 2 relacionada en este caso:
                $id_category_parent_l2 = self::getPSCategoryIDFromSAPFamilyID($sap_familia_l2_code);
                $id_parent_l1_default_category = self::getCategoryParentIDByCategoryID($id_category_parent_l2);

                ## Ahora se comprueba que no exista ya hecho ese mapeo:
                $mapeo_hecho = self::getIDSAPCategory((int)$id_parent_l1_default_category);

                if (!$mapeo_hecho) {
                    ## En este punto se procede a realizar el mapeo buscado:
                    self::setIDSAPCategory((int)$id_parent_l1_default_category, $sap_familia_l1_code);
                }
            }
        } else {
            ## Se evita que la categoría por defecto sea eliminada si viene vacío ese dato en el array de producto parametrizado:
            if (isset($product->id_category[0])) {
                $product->id_category_default = (int)$product->id_category[0];
            } else {
                $defaultProductShop = new Shop($product->id_shop_default);
                $product->id_category_default = Category::getRootCategory(
                    null, Validate::isLoadedObject($defaultProductShop) ? $defaultProductShop : null
                )->id;
            }
        }

        ## URL reescrita (Rewrite link):
        $link_rewrite = Tools::link_rewrite($product->name, true);
        if (empty($link_rewrite)) $link_rewrite = "friendly-url-autogeneration-failed";
        $product->link_rewrite = $link_rewrite;

        ## Se convierte la ',' en '.' para valores flotantes:
        foreach (Product::$definition['fields'] as $key => $array) {
            if ($array['type'] == Product::TYPE_FLOAT) {
                $product->{$key} = str_replace(',', '.', $product->{$key});
            }
        }

        ## La indexación ya es 0 para un producto nuevo, pero no si es una actualización:
        if (($product->reference && $product->existsRefInDatabase($product->reference)) || $productExistsInDatabase) {
            $product->date_upd = date('Y-m-d H:i:s');
        }
        if (!$productExistsInDatabase) $product->indexed = 0;

        $thisRslt = false;
        $field_error = $product->validateFields(false, true);
        $lang_field_error = $product->validateFieldsLang(false, true);
        if ($field_error === true && $lang_field_error === true) {
            ## Chequear la cantidad:
            if ($product->quantity == null) $product->quantity = 0;
            ## Si es un producto ya existente en la base de datos, se intenta actualizar:
            if ($product->reference && $product->existsRefInDatabase($product->reference)) {
                $datas = Db::getInstance()->getRow('SELECT product_shop.date_add, p.id_product FROM '
                    . _DB_PREFIX_ . 'product p ' . Shop::addSqlAssociation('product', 'p')
                    . ' WHERE p.reference = "' . pSQL($product->reference) . '"', false);
                $product->id = (int)$datas['id_product'];
                $product->date_add = pSQL($datas['date_add']);
                $thisRslt = $product->update();
            } elseif ($productExistsInDatabase) {
                $datas = Db::getInstance()->getRow('SELECT product_shop.date_add FROM '
                    . _DB_PREFIX_ . 'product p ' . Shop::addSqlAssociation('product', 'p')
                    . ' WHERE p.id_product = ' . (int)$product->id, false);
                $product->date_add = pSQL($datas['date_add']);
                $thisRslt = $product->update();
            }
            if (!$thisRslt) {
                if (isset($product->date_add) && $product->date_add != '') {
                    $thisRslt = $product->add(false);
                } else {
                    $thisRslt = $product->add();
                }
            }
            if ($product->getType() == Product::PTYPE_VIRTUAL) {
                StockAvailable::setProductOutOfStock((int)$product->id, 1);
            } else {
                StockAvailable::setProductOutOfStock((int)$product->id, (int)$product->out_of_stock);
            }
        }

        $shops = [];
        $product_shop = explode(',,', $product->shop);
        foreach ($product_shop as $shop) {
            if (empty($shop)) {
                continue;
            }
            $shop = trim($shop);
            if (!empty($shop) && !is_numeric($shop)) {
                $shop = Shop::getIdByName($shop);
            }
            if (in_array($shop, $shop_ids)) {
                $shops[] = $shop;
            } else {
                $errores .= 'Producto ' . $thisProdInf . ' ¡¡ La tienda (shop) NO es válida !! ··· ';
            }
        }
        if (empty($shops)) {
            $shops = Shop::getContextListShopID();
        }
        ## Si ambos fallan:
        if (!$thisRslt) {
            $mensaje_error = sprintf(
                '¡¡ ERROR FATAL: NO es posible crear el producto %1$s (ID: %2$s) !! ··· ',
                !empty($product_array['name']) ? Tools::safeOutput($product_array['name']) : 'SIN NOMBRE',
                !empty($product_array['id']) ? Tools::safeOutput($product_array['id']) : 'SIN ID'
            );
            $errores .= ($field_error !== true ? $field_error : '')
                . (isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '') . $mensaje_error;
            return trim($errores);
        } else {
            ## Proveedor:
            if (isset($product->id) && $product->id && isset($product->id_supplier) && property_exists($product, 'supplier_reference')) {
                $id_product_supplier = ProductSupplier::getIdByProductAndSupplier((int)$product->id, 0, (int)$product->id_supplier);
                if ($id_product_supplier) {
                    $product_supplier = new ProductSupplier($id_product_supplier);
                } else {
                    $product_supplier = new ProductSupplier();
                }
                $product_supplier->id_product = (int)$product->id;
                $product_supplier->id_product_attribute = 0;
                $product_supplier->id_supplier = (int)$product->id_supplier;
                $product_supplier->product_supplier_price_te = $product->wholesale_price;
                $product_supplier->product_supplier_reference = $product->supplier_reference;
                $product_supplier->id_currency = Currency::getDefaultCurrency()->id;
                $product_supplier->save();
            }

            ## Precio específico (este proceso sólo admite la reducción básica):
            if (!$shop_is_feature_active) {
                $product_array['shop'] = 1;
            } elseif (empty($product_array['shop'])) {
                $product_array['shop'] = implode(',,', Shop::getContextListShopID());
            }
            $product_array['shop'] = explode(',,', $product_array['shop']);
            $id_shop_list = [];
            foreach ($product_array['shop'] as $shop) {
                if (!empty($shop) && !is_numeric($shop)) {
                    $id_shop_list[] = (int)Shop::getIdByName($shop);
                } elseif (!empty($shop)) {
                    $id_shop_list[] = $shop;
                }
            }
            ## Se procede ahora a gestionar el precio específico:
            if ((isset($product_array['reduction_price']) && $product_array['reduction_price'] > 0)
                || (isset($product_array['reduction_percent']) && $product_array['reduction_percent'] > 0)) {
                ## Se recorre la posible lista de tiendas:
                foreach ($id_shop_list as $id_shop) {
                    $specific_price = SpecificPrice::getSpecificPrice(
                        $product->id, $id_shop, 0, 0, 0, 1, 0
                    );
                    if (is_array($specific_price) && isset($specific_price['id_specific_price'])) {
                        $specific_price = new SpecificPrice((int)$specific_price['id_specific_price']);
                    } else {
                        $specific_price = new SpecificPrice();
                    }
                    $specific_price->id_product = (int)$product->id;
                    $specific_price->id_specific_price_rule = 0;
                    $specific_price->id_shop = $id_shop;
                    $specific_price->id_currency = 0;
                    $specific_price->id_country = 0;
                    $specific_price->id_group = 0;
                    $specific_price->price = -1;
                    $specific_price->id_customer = 0;
                    $specific_price->from_quantity = 1;
                    $specific_price->reduction = (isset($product_array['reduction_price']) && $product_array['reduction_price']) ? (float)str_replace(',', '.', $product_array['reduction_price']) : $product_array['reduction_percent'] / 100;
                    $specific_price->reduction_type = (isset($product_array['reduction_price']) && $product_array['reduction_price']) ? 'amount' : 'percentage';
                    $specific_price->from = (isset($product_array['reduction_from']) && Validate::isDate($product_array['reduction_from'])) ? $product_array['reduction_from'] : '0000-00-00 00:00:00';
                    $specific_price->to = (isset($product_array['reduction_to']) && Validate::isDate($product_array['reduction_to'])) ? $product_array['reduction_to'] : '0000-00-00 00:00:00';
                    if (!$specific_price->save()) {
                        $errores .= 'Producto ' . $thisProdInf . ' ¡¡ El descuento NO es válido !! ··· ';
                    }
                }
            }

            ## Etiquetas (tags):
            if (!empty($product_array['tags'])) {
                $tags_string = trim($product_array['tags']);
                ## Se limpia de caracteres no permitidos en PS, la cadena de etiquetas:
                $etiquetas = preg_replace('/[!<;>?=+#"°{}_$%.]/', '', $tags_string);
                if (isset($product->id) && $product->id && !empty($etiquetas)) {
                    ## Se borran todas las etiquetas del producto, para luego ir añadiendo las nuevas:
                    Tag::deleteTagsForProduct((int)$product->id);
                    $is_tag_added = Tag::addTags($default_language_id, (int)$product->id, $etiquetas, ',,');
                    if (!$is_tag_added) {
                        $errores .= 'Producto ' . $thisProdInf . ' ¡¡ El listado de \'tags\' NO es válido !! ··· ';
                    }
                }
            }

            if ($product_array['delete_existing_images']) {
                ## Se eliminan las imágenes del producto:
                $product->deleteImages();
            }

            ## Imágenes desde 'images_collection':
            if ($images_collection_exists && !empty($images_collection_array)) {
                $product_has_images = (bool)Image::getImages($default_language_id, (int)$product->id);
                ## Se obtienen los datos relacionados con cada imagen de la colección y se intenta generar las imágenes del producto:
                foreach ($images_collection_array as $indice => $image_data_array) {
                    ## Datos relacionados con la imagen actual:
                    $imageFileName = $image_data_array['file_name'];
                    $imageFileExt = $image_data_array['file_extension'];
                    $base64ImageString = $image_data_array['base64_image_str'];
                    ## Se conforma la ruta de destino de la imagen. Inicialmente se busca un destino temporal:
                    $output_filename = _PS_TMP_IMG_DIR_ . $imageFileName . '.' . $imageFileExt;
                    ## Se intenta generar el fichero en la ruta temporal:
                    $output_file = JlcedemoTools::base64ToImageFile($base64ImageString, $output_filename);
                    ## Se intenta generar la imagen del producto:
                    if (is_file($output_file)) {
                        $image = new Image();
                        $image->id_product = (int)$product->id;
                        $image->position = Image::getHighestPosition($product->id) + 1;
                        $image->cover = ($indice === 0 && !$product_has_images);
                        $alt = $product->image_alt[$indice];
                        if (strlen($alt) > 0) $image->legend = self::createMultiLangField($alt);
                        ## Se procede a asociar y guardar la imagen definitivamente:
                        if (($image->validateFields(false, true)) === true &&
                            ($image->validateFieldsLang(false, true)) === true && $image->add()) {
                            ## Asociar imagen a las tiendas (shops) seleccionadas:
                            $image->associateTo($shops);
                            ## Guardar la imagen en la ruta correspondiente a la entidad 'products':
                            if (self::saveImg($output_file, (int)$product->id, (int)$image->id)) {
                                ## Una vez guardada la imagen, se procede a la eliminación del fichero temporal si sigue ahí:
                                if (is_file($output_file)) unlink($output_file);
                            } else {
                                $image->delete();
                                if (is_file($output_file)) unlink($output_file); // Eliminación del temporal si sigue ahí
                                $errores .= 'Producto ' . $thisProdInf . ' ¡¡ ERROR copiando la imagen desde: ' . $output_file . ' !! ··· ';
                            }
                        } else {
                            $errores .= sprintf(
                                'Producto #%1$s: ¡¡ NO es posible guardar la imagen desde el path (%2$s) !! ··· ',
                                $thisProdInf, $output_file
                            );
                        }
                    } else {
                        $errores .= 'Producto ' . $thisProdInf . ': ¡¡ ERROR en la generación de imagen de producto !! ··· ';
                    }
                }
            }

            ## Imágenes desde 'images_urls':
            if (array_key_exists('images_urls', $product_array) && !empty($product_array['images_urls'])) {
                $array_urls = explode(',,', trim($product_array['images_urls']));
                $product_has_images = (bool)Image::getImages($default_language_id, (int)$product->id);
                foreach ($array_urls as $indice => $url) {
                    $url = str_replace(' ', '%20', trim($url));
                    if (!empty($url) && Validate::isUrl($url)) {
                        $image = new Image();
                        $image->id_product = (int)$product->id;
                        $image->position = Image::getHighestPosition($product->id) + 1;
                        $image->cover = $indice === 0 && !$product_has_images;
                        $alt = $product->image_alt[$indice];
                        if (strlen($alt) > 0) $image->legend = self::createMultiLangField($alt);
                        ## 'file_exists' no trabaja con protocolo HTTP:
                        if (($image->validateFields(false, true)) === true &&
                            ($image->validateFieldsLang(false, true)) === true && $image->add()) {
                            ## Asociar imagen a las tiendas (shops) seleccionadas:
                            $image->associateTo($shops);
                            if (!self::copyImg($product->id, $image->id, $url)) {
                                $image->delete();
                                $errores .= 'Producto ' . $thisProdInf . ' ¡¡ ERROR copiando la imagen desde: ' . $url . ' !! ··· ';
                            }
                        } else {
                            $errores .= sprintf(
                                'Producto #%1$s: ¡¡ NO es posible subir la imagen desde la URL (%2$s) !! ··· ',
                                $thisProdInf, $url
                            );
                        }
                    } else {
                        $errores .= 'Producto ' . $thisProdInf . ': ¡¡ ERROR en la generación de imagen de producto !! ··· ';
                    }
                }
            }

            if (isset($product->id_category) && is_array($product->id_category)) {
                $product->updateCategories(array_map('intval', $product->id_category));
            }

            $product->checkDefaultAttributes();
            if (!$product->cache_default_attribute) {
                Product::updateDefaultAttribute($product->id);
            }

            ## Características:
            self::delProdFeatUpToLimit((int)$product->id); // Se borran las features
            $features["features"] = $product_array["features"];
            if (!empty($features["features"])) {
                foreach (explode(',,', $features["features"]) as $single_feature) {
                    if (empty($single_feature)) continue;
                    $tab_feature = explode(':', $single_feature);
                    $feature_name = isset($tab_feature[0]) ? trim($tab_feature[0]) : '';
                    $feature_value = isset($tab_feature[1]) ? trim($tab_feature[1]) : '';
                    $position = isset($tab_feature[2]) ? (int)$tab_feature[2] - 1 : false;
                    $custom = isset($tab_feature[3]) ? (int)$tab_feature[3] : false;
                    if (!empty($feature_name) && !empty($feature_value)) {
                        $id_feature = Feature::addFeatureImport($feature_name, $position);
                        $id_product = null;
                        if ((isset($force_ids) && isset($match_ref)) && ($force_ids || $match_ref)) {
                            $id_product = (int)$product->id;
                        }
                        $id_feature_value = FeatureValue::addFeatureValueImport($id_feature, $feature_value, $id_product, $id_lang, $custom);
                        Product::addFeatureProductImport($product->id, $id_feature, $id_feature_value);
                    }
                }
            }
            ## Limpiar las posiciones de las características para evitar conflictos:
            Feature::cleanPositions();

            ## Administración avanzada de stock 'advanced_stock_management' (a_s_m):
            $product->advanced_stock_management = $product_array["advanced_stock_management"];
            if (!empty($product->advanced_stock_management)) {
                if ($product->advanced_stock_management != 1 && $product->advanced_stock_management != 0) {
                    $errores .= '¡¡ Valor incorrecto para a_s_m. No establecido para ' . $product->name[$default_language_id] . ' !! ··· ';
                } elseif (!Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && $product->advanced_stock_management == 1) {
                    $errores .= '¡¡ a_s_m no está habilitado. NO es posible activarlo para ' . $product->name[$default_language_id] . ' !! ··· ';
                } elseif ($update_advanced_stock_management_value) {
                    $product->setAdvancedStockManagement($product->advanced_stock_management);
                }
                ## Deshabilitar automáticamente 'depends_on_stock' si 'advanced_stock_management' está habilitada:
                if (StockAvailable::dependsOnStock($product->id) == 1 && $product->advanced_stock_management == 0) {
                    StockAvailable::setProductDependsOnStock($product->id, 0);
                }
            }

            ## Almacén:
            if (!empty($product_array["warehouse"])) $product->warehouse = $product_array["warehouse"];
            if (isset($product->warehouse) && $product->warehouse) {
                if (!Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                    $errores .= 'Producto ' . $thisProdInf . ' ¡¡ a_s_m no está habilitado. Almacén no establecido sobre el producto '
                        . $product->name[$default_language_id] . ' !! ··· ';
                } else {
                    if (Warehouse::exists($product->warehouse)) {
                        ## Obtener los almacenes ya asociados:
                        $associated_warehouses_collection = WarehouseProductLocation::getCollection($product->id);
                        ## Borrar cualquier entrada en el almacén para este producto:
                        foreach ($associated_warehouses_collection as $awc) {
                            $awc->delete();
                        }
                        $warehouse_location_entity = new WarehouseProductLocation();
                        $warehouse_location_entity->id_product = $product->id;
                        $warehouse_location_entity->id_product_attribute = 0;
                        $warehouse_location_entity->id_warehouse = $product->warehouse;
                        if (WarehouseProductLocation::getProductLocation($product->id, 0, $product->warehouse) !== false) {
                            $warehouse_location_entity->update();
                        } else {
                            $warehouse_location_entity->save();
                        }
                        StockAvailable::synchronize($product->id);
                    } else {
                        $errores .= '¡¡ Almacén inexistente. Imposible establecerlo sobre el producto '
                            . $product->name[$default_language_id] . ' !! ··· ';
                    }
                }
            }

            ## Depender de stock:
            $product->depends_on_stock = $product_array["depends_on_stock"];
            if (!empty($product->depends_on_stock)) {
                if ($product->depends_on_stock != 0 && $product->depends_on_stock != 1) {
                    $errores .= '¡¡ Valor incorrecto para \'depends_on_stock\' para el producto '
                        . $product->name[$default_language_id] . ' !! ··· ';
                } elseif ((!$product->advanced_stock_management || $product->advanced_stock_management == 0) && $product->depends_on_stock == 1) {
                    $errores .= '¡¡ a_s_m no habilitado; no se puede activar \'depends_on_stock\' para el producto '
                        . $product->name[$default_language_id] . ' !! ··· ';
                } else {
                    StockAvailable::setProductDependsOnStock($product->id, $product->depends_on_stock);
                }
                ## Se habilita establecer una cantidad y deshabilitar 'depends_on_stock':
                if (isset($product->quantity)) {
                    if ($product->depends_on_stock == 1) {
                        $stock_manager = StockManagerFactory::getManager();
                        $price = str_replace(',', '.', $product->wholesale_price);
                        if ($price == 0) {
                            $price = 0.000001;
                        }
                        $price = round(floatval($price), 6);
                        $warehouse = new Warehouse($product->warehouse);
                        if ($stock_manager->addProduct((int)$product->id, 0, $warehouse, (int)$product->quantity, 1, $price, true)) {
                            StockAvailable::synchronize((int)$product->id);
                        }
                    } else {
                        if ($shop_is_feature_active) {
                            foreach ($shops as $shop) {
                                StockAvailable::setQuantity((int)$product->id, 0, (int)$product->quantity, (int)$shop);
                            }
                        } else {
                            StockAvailable::setQuantity((int)$product->id, 0, (int)$product->quantity, (int)$context->shop->id);
                        }
                    }
                }
            } else {
                ## Si no está definido 'depends_on_stock':
                if ($shop_is_feature_active) {
                    foreach ($shops as $shop) {
                        StockAvailable::setQuantity($product->id, 0, (int)$product->quantity, (int)$shop);
                    }
                } else {
                    StockAvailable::setQuantity($product->id, 0, (int)$product->quantity, $context->shop->id);
                }
            }
        }

        ## Teniendo el Id de producto, en este punto se procede a añadir el campo de personalización
        ## 'jlce_Measures', pero sólo cuando se crea el producto la primera vez:
        $this_id_product = (int)$product->id;
        if (!$productExistsInDatabase) {
            $createJlceMsrsCustField = JlcedemoTools::createJlceMeasuresCustField($this_id_product);
            if (!$createJlceMsrsCustField) {
                PrestaShopLogger::addLog(
                    "ERROR: NO ha sido posible crear el campo de personalización 'jlce_Measures'"
                    . " en la creación del producto $thisPrdctRef !! No obstante, siempre se puede añadir"
                    . " manualmente al producto desde su ficha en el BackOffice de la tienda (pestaña"
                    . " 'Opciones', apartado 'Personalización').",
                    4
                );
            }
        }

        Module::processDeferedFuncCall();
        Module::processDeferedClearCache();
        Tag::updateTagCount();

        ## En este punto y si así se parametriza, se eliminan todas las combinaciones del presente producto:
        if ($delete_product_combinations) $product->deleteProductAttributes();

        ## Si se busca guardar el producto al final del proceso:
        if ($saveProduct) {
            $product->save();
            ## Se establecen los descuentos por grupos de clientes:
            $thisGroups = Group::getGroups(1);
            foreach ($thisGroups as $eachGroup) {
                $this_id_group = (int)$eachGroup['id_group'];
                $thisReduction = JlcedemoTools::getProdCatGrpReduction($this_id_product, $this_id_group);
                if ($thisReduction > 0) {
                    $mySQLQuery = /** @lang MySQL DB query */
                        "INSERT IGNORE
                        INTO " . _DB_PREFIX_ . "product_group_reduction_cache (id_product, id_group, reduction)
                        VALUES ($this_id_product, $this_id_group, $thisReduction)";
                    Db::getInstance()->execute($mySQLQuery);
                }
            }
        }

        return empty(trim($errores)) ? [
            "prd_price" => $productPrice,
            "prd_unit_price" => $productUnitPrice ?? null,
            "prd_unit_weight" => $productUnitWeight ?? null,
        ] : trim($errores);
    }

    /**
     * Esta función crea o actualiza una combinación de PrestaShop a partir de un array
     * (que representa a la combinación que se busca crear o actualizar) pasado por parámetro.
     *
     * @param array $combination_array (Array que representa a la combinación que se busca crear (o actualizar))
     * @param bool $onlyCreate (Por defecto 'false'. Marcando 'true' sólo se buscará crear combinaciones, sin
     * actualizar nada)
     * @param bool $updParentProduct (Por defecto 'false'. Si se recibe un 'true' se fuerza a lanar un guardado
     * del producto padre relacionado con la presente combinación)
     * @param array $tempCarts (Por defecto un array vacío. Se puede recibir un array de carritos temporales, los
     * cuales se tendrían en cuenta a la hora de registrar el stock de cada artículo)
     * @return bool|string (Se devuelve 'true' si ha podido crearse (o actualizarse) la combinación.
     * En cualquier otro caso se devuelve un 'string' con los posibles errores del proceso)
     * @throws PrestaShopDatabaseException|PrestaShopException
     */
    public static function createOrUpdateCombination(
        array $combination_array,
        bool  $onlyCreate = false,
        bool  $updParentProduct = false,
        array $tempCarts = []
    )
    {
        ## Se definen unas variables básicas iniciales:
        $err = '';
        $context = Context::getContext();
        $default_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $shop_is_feature_active = Shop::isFeatureActive();
        $thisItemRef = !empty(trim($combination_array['reference'])) ? trim($combination_array['reference']) : '';
        $itemCodeMssg = !empty($thisItemRef) ? 'Artículo ' . $thisItemRef . ': ' : '';

        $groups = [];
        foreach (AttributeGroup::getAttributesGroups($default_language) as $group) {
            $groups[$group['name']] = (int)$group['id_attribute_group'];
        }

        $attributes = [];
        foreach (Attribute::getAttributes($default_language) as $attribute) {
            $attributes[$attribute['attribute_group'] . '_' . $attribute['name']] = (int)$attribute['id_attribute'];
        }

        ## Para procesos de no sólo creación de combinaciones (artículos hijos), se considera
        ## que por defecto se está ante una actualización del presente artículo hijo (combinación):
        $updItem = true;
        if ($onlyCreate) $updItem = false;

        ## Características personalizadas por defecto de la combinación (artículo hijo):
        $item_features = [
            "UTM" => null, // El tipo de unidad de medida; a saber: "NA", "ML", "M2", "M3"
            "FormatType" => "", // El formato del artículo. Ejemplo "BR" (barra), "CJ" (caja), etc., etc.
            "QltAssu" => "Y", // Garantía de calidad. Por defecto se garantiza dicha calidad.
            "SynchItem" => "Y", // Por defecto se consideraría un artículo a futuro sincronizable
            "LongFormat" => null, // Formato almacenado. Largo. Ejemplo 3
            "WidthFormat" => null, // Formato almacenado. Ancho. Ejemplo 2.5
            "HeightFormat" => null, // Fomato almacenado. Alto. Ejemplo 1.2
            "UnitsFormat" => null, // Formato almacenado. Unidades. Ejemplo 50 (cajas de 50 tornillos...)
            "FormatDiscount" => 0, // Descuento por formato almacenado. Ejemplo 0.3 (sería un 30%)
            "CuttingPrice" => 0, // Precio del corte (suplemento de corte en euros). Ejemplo 1.191459
            "UnitWeight" => 0, // Peso unitario (en Kgrs.) por unidad de medida. Ejemplo 1.15
            "Availability" => 1, // Disponibilidad del artículo. El 1 para la máxima disponibilidad (sería un "A" en Randrade)
            "MinSaleUnits" => null, // Unidades mínimas de venta
            "MsrmntMultiples" => null, // Venta en múltiplos de... (en la ud. de medida que corresponda). Por ejemplo 0.2 (mtrs. 'ML') o 2 (uds. 'NA')
            "MinLong" => null, // Longitud mínima de venta en metros. Ejemplo 0.1
            "MaxLong" => null, // Longitud máxima de venta en metros. Ejemplo 2.25
            "MinWidth" => null, // Anchura mínima de venta en metros. Ejemplo 0.1
            "MaxWidth" => null, // Anchura máxima de venta en metros. Ejemplo 2.25
            "MinHeight" => null, // Altura mínima de venta en metros. Ejemplo 0.1
            "MaxHeight" => null, // Altura máxima de venta en metros. Ejemplo 1.15
        ];

        ## Valores por defecto de atributos:
        $defaultAttrs = [
            'reference' => '',
            'supplier_reference' => '',
            'ean13' => '',
            'upc' => '',
            'wholesale_price' => 0,
            'quantity' => 0,
            'minimal_quantity' => 1,
            'weight' => 0,
            'default_on' => false,
            'advanced_stock_management' => 0,
            'depends_on_stock' => 0,
            'available_date' => date('Y-m-d'),
            'delete_existing_images' => 0,
            'low_stock_threshold' => null,
            'low_stock_alert' => false,
        ];

        ## Se declara el arreglo de características del artículo hijo:
        $itemFeatsArr = $combination_array["item_features"];

        ## Si en el arreglo recibido por parámetro no se incluye alguno de los anteriores
        ## valores de 'defaultAttrs', se establecen como valores los de por defecto:
        foreach ($defaultAttrs as $attrKey => $attrValue) {
            if (empty($combination_array[$attrKey])) $combination_array[$attrKey] = $attrValue;
        }

        ## Si en el arreglo recibido por parámetro no se incluye alguno de los anteriores
        ## valores de 'item_features', se establecen como valores los de por defecto:
        foreach ($item_features as $featKey => $featValue) {
            if (empty($itemFeatsArr[$featKey])) $itemFeatsArr[$featKey] = $featValue;
        }

        ## Si hay información válida en la clave 'item_features' del array 'combination_array':
        $itemType = $itemFeatsArr["UTM"] ?? null;
        if (empty($itemType) || !in_array($itemType, ["NA", "ML", "M2", "M3"])) {
            ## NO se permite ya registrar la combinación; esto es, este artículo hijo:
            $err .= $itemCodeMssg . '¡¡ ERROR FATAL: NO se puede registrar esta combinación por '
                . 'NO tener características personalizadas válidas !! ··· ';
            return trim($err);
        }

        ## Tienda (o tiendas):
        if (!$shop_is_feature_active) {
            $combination_array['shop'] = 1;
        } elseif (empty($combination_array['shop'])) {
            $combination_array['shop'] = implode(',,', Shop::getContextListShopID());
        }
        $combination_array['shop'] = explode(',,', $combination_array['shop']);
        $id_shop_list = [];
        if (is_array($combination_array['shop']) && count($combination_array['shop'])) {
            foreach ($combination_array['shop'] as $shop) {
                if (!empty($shop) && !is_numeric($shop)) {
                    $id_shop_list[] = Shop::getIdByName($shop);
                } elseif (!empty($shop)) {
                    $id_shop_list[] = $shop;
                }
            }
        }

        ## Determinación del producto padre relacionado:
        $hay_id_product = false;
        $hay_product_ref = false;
        $hay_product_ref_old = false;
        $product = null;
        if (isset($combination_array['id_product'])
            && !empty(trim($combination_array['id_product']))
            && is_numeric($combination_array['id_product'])
            && $combination_array['id_product']
        ) $hay_id_product = true;
        if (isset($combination_array['product_reference'])
            && !empty(trim($combination_array['product_reference']))
            && $combination_array['product_reference']
        ) $hay_product_ref = true;
        if (isset($combination_array['product_reference_old'])
            && !empty(trim($combination_array['product_reference_old']))
            && $combination_array['product_reference_old']
        ) {
            $hay_product_ref = true;
            $hay_product_ref_old = true;
        }
        if ($hay_id_product) {
            $product = new Product((int)$combination_array['id_product'], false, $default_language);
        } elseif ($hay_product_ref) {
            $datas = Db::getInstance()->getRow('SELECT p.id_product FROM ' . _DB_PREFIX_ . 'product p '
                . Shop::addSqlAssociation('product', 'p')
                . ' WHERE p.reference = "' . pSQL($combination_array['product_reference']) . '"', false);
            ## Si no se obtiene resultado por campo de referencia convencional, se busca por referencia antigua:
            if ($datas === false && $hay_product_ref_old) {
                $datas_sql = 'SELECT p.id_product FROM ' . _DB_PREFIX_ . 'product p '
                    . Shop::addSqlAssociation('product', 'p')
                    . ' WHERE p.reference = "' . pSQL($combination_array['product_reference_old']) . '"';
                $datas = Db::getInstance()->getRow($datas_sql, false);
            }
            if (isset($datas['id_product']) && $datas['id_product']) {
                $product = new Product((int)$datas['id_product'], false, $default_language);
            }
        } else {
            $err .= '¡¡ ERROR FATAL: NO es posible crear (o actualizar) el artículo hijo '
                . $thisItemRef . ' sin indicar un producto (padre) existente relacionado !! ··· ';
            return trim($err);
        }

        /*
         * @todo Hay una casuística por la cual se puede determinar que un artículo sea sincronizable o no.
         * Hay una clave en el JSON de las características personalizadas de los artículos¹, llamada "SynchItem".
         * Implementar la manera de trabajar con este tipo de casuística; esto es, de que un artículo pueda
         * ser en un momento dado susceptible de sincronización o no, apoyándonos en este registro y lo que se
         * especifique en SAP en base a ello.
         *
         * ¹ Columna 'item_features' de la tabla 'ps_jlcedemo_items_features'.
         */
        /////////////////////
        // Aquí el código //
        ///////////////////

        ## Impactos en precio y precio por unidad de medida. De salida se declaran:
        $itemPrice = JlcedemoTools::toFixed($combination_array['unit_price'], 6);
        $frmtConvRatio = 1;
        $product_price = $combination_array['product_price'] ?? null;
        $product_unit_price = $combination_array['product_unit_price'] ?? null;
        $product_unit_weight = $combination_array['product_unit_weight'] ?? 0;
        ## Según el tipo de artículo:
        if ($itemType === "NA") {
            ## Venta por unidades. Imprescindible partir de un precio por unidad del producto padre:
            if (empty($product_price)) {
                $err .= "¡¡ ERROR FATAL: NO ha sido determinado el precio del producto padre del artículo hijo $thisItemRef !! ··· ";
                return trim($err);
            }

            ## Precio base del producto (padre):
            $prBsPrd = JlcedemoTools::toFixed($product_price, 6);

            ## Impacto en el precio del presente artículo hijo (combinación):
            $imPrCmb = $itemPrice - $prBsPrd;
            $imPrCmb = JlcedemoTools::toFixed($imPrCmb, 6);
            ## En este caso NO hay impacto en la unidad de medida, porque no hay una unidad de medida específica:
            $imPrCmbUdMd = 0;

            ## Cálculo del impacto en el peso respecto al producto padre:
            $impWghtCmb = ($itemFeatsArr["UnitWeight"] * 1) - ($product_unit_weight * 1);
            $impWghtCmb = JlcedemoTools::toFixed($impWghtCmb, 3);
        } else {
            ## Venta por medidas. Cálculo del ratio de conversión de formato:
            $frmtConvRatio = $itemFeatsArr["UnitWeight"] * 1000;

            ## Precio por gramo y precio por unidad de medida del producto padre:
            if (empty($product_price) || empty($product_unit_price)) {
                $err .= "¡¡ ERROR FATAL: NO ha sido determinado el precio del producto padre del artículo hijo $thisItemRef !! ··· ";
                return trim($err);
            }
            $prBsPrd = JlcedemoTools::toFixed($product_price, 6); // Precio base del producto padre (precio por gramo)
            $prBsPrdUdMd = JlcedemoTools::toFixed($product_unit_price, 12); // Precio base del producto padre por ud. medida (metro, etc.)

            ## Precio base de la combinación (este artículo hijo) por gramo:
            $prBsCmb = $itemPrice / $frmtConvRatio;

            ## Impacto del precio de la combinación (este artículo hijo) por gramo:
            $imPrCmb = $prBsCmb - $prBsPrd;
            $imPrCmb = JlcedemoTools::toFixed($imPrCmb, 6);
            ## Impacto del precio de la combinación (este artículo hijo) por unidad de medida:
            if (empty($imPrCmb)) {
                $imPrCmbUdMd = 0;
            } else {
                $imPrCmbUdMd = $itemPrice - (($prBsCmb / $prBsPrd) * $prBsPrdUdMd);
                $imPrCmbUdMd = JlcedemoTools::toFixed($imPrCmbUdMd, 6);
            }

            ## El impacto en el peso es nulo para la venta por medidas,
            ## porque el peso siempre se considera de 1 gramo:
            $impWghtCmb = 0;
        }

        ## Imágenes:
        $id_image = [];
        ## Borrar las imágenes existentes si la clave 'delete_existing_images' vale 1:
        if (array_key_exists('delete_existing_images', $combination_array)
            && $combination_array['delete_existing_images']
        ) {
            $product->deleteImages();
        }
        if (isset($combination_array['images_urls']) && !empty($combination_array['images_urls'])) {
            $combination_array['images_urls'] = explode(',,', $combination_array['images_urls']);
            if (is_array($combination_array['images_urls']) && count($combination_array['images_urls'])) {
                $product_has_images = (bool)Image::getImages($default_language, (int)$product->id);
                foreach ($combination_array['images_urls'] as $indice => $url) {
                    $url = str_replace(' ', '%20', trim($url));
                    if (!empty($url) && Validate::isUrl($url)) {
                        $image = new Image();
                        $image->id_product = (int)$product->id;
                        $image->position = Image::getHighestPosition($product->id) + 1;
                        $image->cover = $indice === 0 && !$product_has_images;
                        if (isset($combination_array['image_alt'])) {
                            $alt = JlcedemoTools::split($combination_array['image_alt']);
                            if (isset($alt[$indice]) && strlen($alt[$indice]) > 0) {
                                $alt = self::createMultiLangField($alt[$indice]);
                                $image->legend = $alt;
                            }
                        }
                        $field_error = $image->validateFields(false, true);
                        $lang_field_error = $image->validateFieldsLang(false, true);
                        if ($field_error === true && $lang_field_error === true && $image->add()) {
                            ## Asociar imagen a las tiendas (shops) seleccionadas:
                            $image->associateTo($id_shop_list);
                            if (!self::copyImg($product->id, $image->id, $url)) {
                                $image->delete();
                                $err .= $itemCodeMssg . '¡¡ ERROR copiando la imagen desde: '
                                    . Tools::htmlentitiesUTF8($url) . ' !! ··· ';
                            } else {
                                $id_image[] = (int)$image->id;
                            }
                        } else {
                            $err .= sprintf(
                                $itemCodeMssg . '¡¡ NO es posible subir la imagen desde la URL (%1$s) !! ··· ',
                                Tools::htmlentitiesUTF8($url)
                            );
                        }
                    } else {
                        $err .= $itemCodeMssg . '¡¡ ERROR en la generación de la imagen !! ··· ';
                    }
                }
            }
        } elseif (isset($combination_array['image_position']) && $combination_array['image_position']) {
            $combination_array['image_position'] = explode(',,', $combination_array['image_position']);
            if (is_array($combination_array['image_position']) && count($combination_array['image_position'])) {
                foreach ($combination_array['image_position'] as $position) {
                    ## Se escogen las imágenes del producto por posición:
                    $images = $product->getImages($default_language);
                    if ($images) {
                        foreach ($images as $row) {
                            if ($row['position'] == (int)$position) {
                                $id_image[] = (int)$row['id_image'];
                                break;
                            }
                        }
                    }
                    if (empty($id_image)) {
                        $err .= sprintf(
                            $itemCodeMssg . '¡¡ NO se ha encontrado imagen para la combinación'
                            . ' con id_product = %s y la posición de imagen = %s !! ··· ',
                            Tools::htmlentitiesUTF8($product->id),
                            (int)$position
                        );
                    }
                }
            }
        }

        ## Grupos de atributos:
        $id_attribute_group = 0;
        $groups_attributes = [];
        if (array_key_exists('group', $combination_array)
            && !empty(trim($combination_array['group']))
        ) {
            ## Para cada grupo de atributo:
            foreach (explode(',,', $combination_array['group']) as $key => $group) {
                if (empty($group)) continue;
                $tab_group = explode(':', $group);
                $group = trim($tab_group[0]);
                if (!isset($tab_group[1])) {
                    $type = 'select';
                } else {
                    $type = trim($tab_group[1]);
                }

                ## Establecer grupo:
                $groups_attributes[$key]['group'] = $group;

                ## Si hay indicada una posición:
                if (isset($tab_group[2])) {
                    $position = trim($tab_group[2]);
                } else {
                    $position = false;
                }

                if (!isset($groups[$group])) {
                    $obj = new AttributeGroup();
                    $obj->is_color_group = false;
                    $obj->group_type = pSQL($type);
                    $obj->name[$default_language] = $group;
                    $obj->public_name[$default_language] = $group;
                    $obj->position = (!$position) ? AttributeGroup::getHigherPosition() + 1 : $position;

                    if (($field_error = $obj->validateFields(false, true)) === true &&
                        ($lang_field_error = $obj->validateFieldsLang(false, true)) === true) {
                        $obj->add();
                        $obj->associateTo($id_shop_list);
                        $groups[$group] = $obj->id;
                    } else {
                        $err .= $itemCodeMssg . ($field_error !== true ? $field_error : '')
                            . (isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '');
                    }

                    ## Completar los grupos de atributos:
                    $id_attribute_group = $obj->id;
                    $groups_attributes[$key]['id'] = $id_attribute_group;
                } else {
                    ## Cuando ya existe:
                    $id_attribute_group = $groups[$group];
                    $groups_attributes[$key]['id'] = $id_attribute_group;
                }
            }
        } else {
            $err .= $itemCodeMssg . '¡¡ ERROR FATAL: NO se puede registrar esta combinación'
                . ' pues no tiene definido un tipo de atributo !! ··· ';
            return trim($err);
        }

        ## Iniciar el 'product_attribute':
        $id_product_attribute = 0;
        $id_product_attribute_update = false;
        $attributes_to_add = [];
        $qtyOnStock = 0;

        ## Para cada atributo:
        if (array_key_exists('attribute', $combination_array)
            && !empty(trim($combination_array['attribute']))
        ) {
            foreach (explode(',,', $combination_array['attribute']) as $key => $attribute) {
                if (empty($attribute)) continue;
                $tab_attribute = explode(':', $attribute);
                $attribute = trim($tab_attribute[0]);

                ## Si está indicada la posición:
                if (isset($tab_attribute[1])) {
                    $position = trim($tab_attribute[1]);
                } else {
                    $position = false;
                }

                if (isset($groups_attributes[$key])) {
                    $group = $groups_attributes[$key]['group'];
                    if (!isset($attributes[$group . '_' . $attribute]) && count($groups_attributes[$key]) == 2) {
                        $id_attribute_group = $groups_attributes[$key]['id'];
                        $obj = new Attribute();

                        ## Establecer el ID apropiado y correspondiente a su respectiva clave:
                        $obj->id_attribute_group = $groups_attributes[$key]['id'];
                        $obj->name[$default_language] = str_replace('\n', '', str_replace('\r', '', $attribute));
                        $obj->position = (!$position && isset($groups[$group])) ? Attribute::getHigherPosition($groups[$group]) + 1 : $position;

                        if (($field_error = $obj->validateFields(false, true)) === true &&
                            ($lang_field_error = $obj->validateFieldsLang(false, true)) === true) {
                            $obj->add();
                            $obj->associateTo($id_shop_list);
                            $attributes[$group . '_' . $attribute] = $obj->id;
                        } else {
                            $err .= $itemCodeMssg . ($field_error !== true ? $field_error : '') . (isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '');
                        }
                    }

                    $combination_array['minimal_quantity'] = !empty($itemFeatsArr["MinSaleUnits"]) ? (int)$itemFeatsArr["MinSaleUnits"] : 1;
                    $combination_array['low_stock_threshold'] =
                        empty($combination_array['low_stock_threshold']) && '0' != $combination_array['low_stock_threshold'] ? null : (int)$combination_array['low_stock_threshold'];
                    $combination_array['low_stock_alert'] = !empty($combination_array['low_stock_alert']);

                    $combination_array['wholesale_price'] = str_replace(',', '.', $combination_array['wholesale_price']);
                    $combination_array['price'] = str_replace(',', '.', $combination_array['price']);
                    $combination_array['ecotax'] = str_replace(',', '.', $combination_array['ecotax']);
                    $combination_array['weight'] = str_replace(',', '.', $combination_array['weight']);
                    $combination_array['available_date'] = Validate::isDate($combination_array['available_date']) ? $combination_array['available_date'] : null;

                    $itemFeatsArr['Availability'] = !empty(trim($itemFeatsArr['Availability'])) ? (int)$itemFeatsArr['Availability'] : 1;

                    ## Cantidad en stock:
                    if (!empty($combination_array['quantity'])) $qtyOnStock = $combination_array['quantity'] * $frmtConvRatio;

                    if (!Validate::isEan13($combination_array['ean13'])) {
                        $err .= sprintf(
                            $itemCodeMssg . 'EAN13 "%1s" tiene un valor incorrecto para el producto con ID %2d.',
                            $combination_array['ean13'],
                            $product->id
                        );
                        $combination_array['ean13'] = '';
                    }

                    if ($combination_array['default_on']) $product->deleteDefaultAttributes();

                    ## Si no sólo se busca la creación sin más de combinaciones y ya hay referencia para este artículo,
                    ## obtener el 'id_product_atribute' asociado para actualizar:
                    if (!$onlyCreate && !empty($thisItemRef)) {
                        $id_product_attribute = Combination::getIdByReference((int)$product->id, $thisItemRef);
                        ## Si no es encuentra un resultado para la referencia aportada,
                        ## se intenta buscar por referencia antigua (de haberla):
                        if (!$id_product_attribute && !empty(trim($combination_array['reference_old']))) {
                            ## Queries para intentar obtener el 'id_product_attribute' basado en la referencia antigua:
                            $sql_id_combination = 'SELECT id_product_attribute FROM '
                                . _DB_PREFIX_ . 'product_attribute WHERE reference LIKE "%'
                                . pSQL($combination_array['reference_old']) . '%" AND id_product = ' . (int)$product->id;
                            $id_product_attribute = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql_id_combination, false);
                        }

                        ## Actualiar la combinación; esto es, el artículo hijo:
                        if ($id_product_attribute) {
                            ## Obtener todas las combinaciones de su producto padre:
                            $attribute_combinations = $product->getAttributeCombinations($default_language);
                            ## Para cada combinación (artículo hijo) del producto (padre) relacionado:
                            foreach ($attribute_combinations as $attribute_combination) {
                                if ($id_product_attribute && in_array($id_product_attribute, $attribute_combination)) {
                                    $product->updateAttribute(
                                        $id_product_attribute,
                                        ($combination_array['wholesale_price'] * 1),
                                        $imPrCmb,
                                        $impWghtCmb,
                                        $imPrCmbUdMd,
                                        (Configuration::get('PS_USE_ECOTAX') ? ($combination_array['ecotax'] * 1) : 0),
                                        $id_image,
                                        $thisItemRef,
                                        strval($combination_array['ean13']),
                                        (bool)$combination_array['default_on'],
                                        0,
                                        strval($combination_array['upc']),
                                        $combination_array['minimal_quantity'],
                                        $combination_array['available_date'],
                                        null,
                                        $id_shop_list,
                                        '',
                                        $combination_array['low_stock_threshold'],
                                        $combination_array['low_stock_alert']
                                    );
                                    $id_product_attribute_update = true;
                                    if (!empty($combination_array['supplier_reference'])) {
                                        $product->addSupplierReference(
                                            $product->id_supplier, $id_product_attribute, $combination_array['supplier_reference']
                                        );
                                    }
                                }
                            }
                        }
                    }

                    ## Creación de la combinación (artículo hijo):
                    if (!$id_product_attribute) {
                        $updItem = false; // Aquí no hay actualización, sino creación
                        $id_product_attribute = $product->addCombinationEntity(
                            ($combination_array['wholesale_price'] * 1),
                            $imPrCmb,
                            $impWghtCmb,
                            $imPrCmbUdMd,
                            (Configuration::get('PS_USE_ECOTAX') ? ($combination_array['ecotax'] * 1) : 0),
                            $qtyOnStock,
                            $id_image,
                            $thisItemRef,
                            0,
                            strval($combination_array['ean13']),
                            (bool)$combination_array['default_on'],
                            0,
                            strval($combination_array['upc']),
                            $combination_array['minimal_quantity'],
                            $id_shop_list,
                            $combination_array['available_date'],
                            '',
                            $combination_array['low_stock_threshold'],
                            $combination_array['low_stock_alert']
                        );

                        if (!empty($combination_array['supplier_reference'])) {
                            $product->addSupplierReference(
                                $product->id_supplier, $id_product_attribute, $combination_array['supplier_reference']
                            );
                        }
                    }

                    ## Completar el array de atributos, para añadir los atributos a 'product_attribute' después:
                    if (isset($attributes[$group . '_' . $attribute])) {
                        $attributes_to_add[] = (int)$attributes[$group . '_' . $attribute];
                    }

                    ## Tras la inserción, se limpia la posición del atributo y grupo:
                    $obj = new Attribute();
                    $obj->cleanPositions((int)$id_attribute_group, false);
                    AttributeGroup::cleanPositions();
                }
            }
        } else {
            $err .= $itemCodeMssg
                . '¡¡ ERROR FATAL: NO se puede registrar esta combinación pues no tiene definido un valor de atributo !! ··· ';
            return trim($err);
        }

        ## Combinación por defecto del producto:
        $product->checkDefaultAttributes();
        if (!$product->cache_default_attribute) Product::updateDefaultAttribute($product->id);
        ## Siempre que exista la combinación:
        if ($id_product_attribute) {
            ## Ahora se añaden los atributos a la tabla 'attribute_combination':
            if ($id_product_attribute_update) {
                Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_
                    . 'product_attribute_combination WHERE id_product_attribute = ' . (int)$id_product_attribute);
            }
            foreach ($attributes_to_add as $attribute_to_add) {
                Db::getInstance()->execute('INSERT IGNORE INTO ' . _DB_PREFIX_
                    . 'product_attribute_combination (id_attribute, id_product_attribute) VALUES ('
                    . (int)$attribute_to_add . ',' . (int)$id_product_attribute . ')', false);
            }

            ## Establecer la gestión avanzada de stock o 'advanced stock managment' (a_s_m)):
            if (!empty($combination_array['advanced_stock_management'])) {
                if ($combination_array['advanced_stock_management'] != 1 && $combination_array['advanced_stock_management'] != 0) {
                    $err .= sprintf(
                        $itemCodeMssg
                        . '¡¡ La gestión avanzada de stock tiene un valor INCORRECTO. NO se establece pues para el producto con ID %d !! ··· ',
                        $product->id
                    );
                } elseif (!Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && $combination_array['advanced_stock_management'] == 1) {
                    $err .= sprintf(
                        $itemCodeMssg
                        . '¡¡ La gestión avanzada de stock NO está activada, NO se puede habilitar para el producto con ID %d !! ··· ',
                        $product->id
                    );
                } else {
                    $product->setAdvancedStockManagement($combination_array['advanced_stock_management']);
                }
                ## Automáticamente se deshabilita la opción de depender de stock ('depends_on_stock') si la a_s_m no está habilitada en este caso:
                if (StockAvailable::dependsOnStock($product->id) == 1 && $combination_array['advanced_stock_management'] == 0) {
                    StockAvailable::setProductDependsOnStock($product->id, 0, null, $id_product_attribute);
                }
            }

            ## Comprobar si existe el almacén:
            if (isset($combination_array['warehouse']) && $combination_array['warehouse']) {
                if (!Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                    $err .= sprintf(
                        $itemCodeMssg
                        . '¡¡ La gestión avanzada de stock NO está activada, NO se puede establecer almacén para el producto con ID %d !! ··· ',
                        Tools::htmlentitiesUTF8($product->id)
                    );
                } else {
                    if (Warehouse::exists($combination_array['warehouse'])) {
                        $warehouse_location_entity = new WarehouseProductLocation();
                        $warehouse_location_entity->id_product = $product->id;
                        $warehouse_location_entity->id_product_attribute = $id_product_attribute;
                        $warehouse_location_entity->id_warehouse = $combination_array['warehouse'];
                        if (WarehouseProductLocation::getProductLocation($product->id, $id_product_attribute, $combination_array['warehouse']) !== false) {
                            $warehouse_location_entity->update();
                        } else {
                            $warehouse_location_entity->save();
                        }
                        StockAvailable::synchronize($product->id);
                    } else {
                        $err .= sprintf(
                            $itemCodeMssg
                            . '¡¡ El almacén NO existe, NO se puede asociar al producto %1$s !! ··· ',
                            $product->name[$default_language]);
                    }
                }
            }

            ## Depender de stock (siempre que esté habilitada la a_s_m):
            if (isset($combination_array['depends_on_stock'])
                && (int)$combination_array['depends_on_stock'] === 1
            ) {
                if ($combination_array['depends_on_stock'] != 0 && $combination_array['depends_on_stock'] != 1) {
                    $err .= sprintf(
                        $itemCodeMssg
                        . '¡¡ Valor INCORRECTO de \'depender de stock\' para el producto %1$s !! ··· ',
                        Tools::htmlentitiesUTF8($product->name[$default_language])
                    );
                } elseif ((!$combination_array['advanced_stock_management'] || $combination_array['advanced_stock_management'] == 0) && $combination_array['depends_on_stock'] == 1) {
                    $err .= sprintf(
                        $itemCodeMssg
                        . '¡¡ La gestión avanzada de stock NO está activada, NO se puede activar \'depender de stock\' para %1$s !! ··· ',
                        Tools::htmlentitiesUTF8($product->name[$default_language])
                    );
                } else {
                    StockAvailable::setProductDependsOnStock($product->id, $combination_array['depends_on_stock'], null, $id_product_attribute);
                }

                ## Si se busca depender de stock y establecer cantidad, añadir cantidad al stock:
                $stock_manager = StockManagerFactory::getManager();
                $price = str_replace(',', '.', $combination_array['wholesale_price']);
                if ($price == 0) {
                    $price = 0.000001;
                }
                $price = round(floatval($price), 6);
                $warehouse = new Warehouse($combination_array['warehouse']);
                if ($stock_manager->addProduct($product->id, $id_product_attribute, $warehouse, $qtyOnStock, 1, $price, true)) {
                    StockAvailable::synchronize($product->id);
                }
            } else {
                ## Si no se establece 'depender de stock', usar el funcionamiento normal para la cantidad a añadir:
                $item_quantity = $qtyOnStock;
                ## Si se ha recibido un array no vacío con un conjunto de carritos temporales que tener en cuenta:
                if (!empty($tempCarts)) {
                    ## Se recorre cada solicitud temporal:
                    foreach ($tempCarts as $eachTmpCart) {
                        ## Siempre que se hable del mismo 'id_product_attribute':
                        if ((int)$eachTmpCart['id_product_attribute'] === (int)$id_product_attribute) {
                            ## La cantidad recibida debe minorarse con las solicitadas:
                            $item_quantity -= ($eachTmpCart['quantity'] * 1);
                        }
                    }
                }
                ## No se puede recibir una cantidad en stock inferior a 0. Así se tiene que:
                if ($item_quantity < 0) $item_quantity = 0;

                if ($shop_is_feature_active) {
                    foreach ($id_shop_list as $shop) {
                        StockAvailable::setQuantity($product->id, $id_product_attribute, $item_quantity, (int)$shop);
                    }
                } else {
                    StockAvailable::setQuantity($product->id, $id_product_attribute, $item_quantity, $context->shop->id);
                }
            }
        }

        ## En este punto se busca registrar la información de las características personalizadas del artículo hijo:
        $itemFeatsJSON = json_encode($itemFeatsArr, 64 | 256);
        ## Según se esté ante una actualización o una creación del artículo hijo:
        if ($updItem) {
            $action = "actualizar";
            $mySQLQuery = /** @lang MySQL DB query */
                "UPDATE " . _DB_PREFIX_ . "jlcedemo_items_features
                SET item_features = '$itemFeatsJSON'
                WHERE id_product = " . (int)$product->id . "
                AND item_reference = '$thisItemRef'";
        } else {
            $action = "registrar";
            ## Antes de nada se hace necesario limpiar la tabla 'ps_jlcedemo_items_features' de posibles restos
            ## de registros descartables con la misma referencia. De esta forma se hacen únicos los registros
            ## relacionados con cada referencia en particular:
            $deleteQuery = /** @lang MySQL DB query */
                "DELETE FROM " . _DB_PREFIX_ . "jlcedemo_items_features
                WHERE item_reference = '$thisItemRef'";
            Db::getInstance()->execute($deleteQuery);
            $mySQLQuery = /** @lang MySQL DB query */
                "INSERT INTO " . _DB_PREFIX_ . "jlcedemo_items_features (id_product, item_reference, item_features)
                VALUES (" . (int)$product->id . ", '$thisItemRef', '$itemFeatsJSON')";
        }
        $queryRslt = Db::getInstance()->execute($mySQLQuery);
        if (!$queryRslt) {
            $err .= $itemCodeMssg . "¡¡ ERROR FATAL: NO se ha podido $action esta combinación (artículo hijo), porque NO ha sido"
                . " posible $action sus características personalizadas en la tabla " . _DB_PREFIX_ . "jlcedemo_items_features !! ··· ";
            return trim($err);
        }

        ## Si la presente combinación es la última editada sobre su producto padre:
        if ($updParentProduct) {
            ## Se lanza el actualizado de dicho producto padre:
            $product->update();
            ## Se establecen los descuentos por grupos de clientes:
            $thisGroups = Group::getGroups(1);
            $this_id_product = (int)$product->id;
            foreach ($thisGroups as $eachGroup) {
                $this_id_group = (int)$eachGroup['id_group'];
                $thisReduction = JlcedemoTools::getProdCatGrpReduction($this_id_product, $this_id_group);
                if ($thisReduction > 0) {
                    $mySQLQuery = /** @lang MySQL DB query */
                        "INSERT IGNORE INTO " . _DB_PREFIX_ . "product_group_reduction_cache (id_product, id_group, reduction)
                        VALUES ($this_id_product, $this_id_group, $thisReduction)";
                    $queryRslt = Db::getInstance()->execute($mySQLQuery);
                    if (!$queryRslt) $err .= $itemCodeMssg . '¡¡ ERROR registrando el descuento de categoría !! ··· ';
                }
            }
            ## Se actualiza el índice de búsquedas para contemplar todos los cambios:
            Search::indexation(false, $this_id_product);
        }

        return empty(trim($err)) ? true : trim($err);
    }

    /**
     * @param int $id_familia_sap (ID de la familia de SAP B1)
     * @return bool|int (Se devuelve el ID de categoría de PrestaShop o falso si no existe)
     */
    public static function getPSCategoryIDFromSAPFamilyID(int $id_familia_sap)
    {
        $sql_query = /** @lang MySQL DB query */
            "SELECT id_category
            FROM " . _DB_PREFIX_ . "category
            WHERE id_sap_category = '$id_familia_sap'
            OR sap_alt_categories LIKE '$id_familia_sap'
            OR sap_alt_categories LIKE '$id_familia_sap,%'
            OR sap_alt_categories LIKE '%,$id_familia_sap,%'
            OR sap_alt_categories LIKE '%,$id_familia_sap'";
        $result = Db::getInstance()->getValue($sql_query);

        if (empty($result)) return false;

        return (int)$result;
    }

    /**
     * @param int $id_category (El ID de la categoría de PrestaShop)
     * @param int $id_lang (El ID del idioma. Por defecto 1)
     * @return bool|string (Se devuelve el nombre de la categoría o falso)
     */
    public static function getCategoryNameByID(int $id_category, int $id_lang = 1)
    {
        $sql_query = 'SELECT name'
            . ' FROM ' . _DB_PREFIX_ . 'category_lang'
            . ' WHERE id_lang = ' . $id_lang
            . ' AND id_category = ' . $id_category;
        $result = Db::getInstance()->getValue($sql_query);

        if (!$result || empty(trim($result))) return false;

        return $result;
    }

    /**
     * @param int $id_category (El ID de la categoría de PrestaShop)
     * @return false|int (Se devuelve el ID de la categoría padre o 'false')
     */
    public static function getCategoryParentIDByCategoryID(int $id_category)
    {
        $sql_query = /** @lang MySQL DB query */
            "SELECT id_parent
            FROM " . _DB_PREFIX_ . "category
            WHERE id_category = $id_category
            AND id_parent > 2";
        $result = Db::getInstance()->getValue($sql_query);
        if (empty($result)) return false;
        return (int)$result;
    }

    /**
     * @param int $id_lang (El ID del idioma)
     * @param string $path (El string que representa el path. Por ejemplo: 'cat_level_1/cat_level_2')
     * @return array|bool (El array que representa la categoría o falso)
     * @throws PrestaShopDatabaseException
     */
    public static function searchByStringPath(int $id_lang, string $path)
    {
        $categories_array = explode('/', trim($path));
        $category = $id_parent_category = false;

        if (is_array($categories_array) && count($categories_array)) {
            foreach ($categories_array as $category_name) {
                if ($id_parent_category) {
                    $category = Category::searchByNameAndParentCategoryId($id_lang, $category_name, $id_parent_category);
                } else {
                    $category = Category::searchByName($id_lang, $category_name, true, true);
                }

                if (isset($category['id_category']) && $category['id_category']) {
                    $id_parent_category = (int)$category['id_category'];
                } else {
                    return false;
                }
            }
        }

        return $category;
    }

    /**
     * Función estática que devuelve el 'id_sap_category' de una categoría de PS dada.
     *
     * @param int $id_category (El ID de la categoría de PrestaShop)
     * @return int|bool (Se devuelve el ID de categoría de SAP B1 o falso)
     */
    public static function getIDSAPCategory(int $id_category)
    {
        $result = Db::getInstance()->getValue('SELECT id_sap_category'
            . ' FROM ' . _DB_PREFIX_ . 'category WHERE id_category = ' . $id_category);

        if ($result) return (int)$result;

        return false;
    }

    /**
     * Método para establecer un ID de categoría (familia o subfamilia) de SAP B1 para un
     * ID de categoría de PrestaShop.
     *
     * @param int $id_category (El ID de categoría de PrestaShop)
     * @param int $id_sap_category (El ID de categoría (familia o subfamilia) de SAP B1)
     */
    public static function setIDSAPCategory(int $id_category, int $id_sap_category)
    {
        Db::getInstance()->update(
            'category',
            ['id_sap_category' => $id_sap_category],
            'id_category = ' . $id_category
        );
    }

    /**
     * Esta función copia una imagen localizada en una $url y la guarda
     * en un directorio determinado de acuerdo a su entidad correspondiente.
     * El parámetro 'id_image' es usado si se necesita una marca de agua.
     *
     * @param int $id_entity (ID del producto o categoría (establecido en la entidad))
     * @param int|null $id_image (Por defecto a null, es el ID de imagen para habilitar marca de agua)
     * @param string $url (Ruta o URL)
     * @param string $entity (Entidades productos, categorías, fabricantes o proveedores; esto es
     * se admiten sólo las siguientes cadenas: 'products', 'categories', 'manufacturers' o 'suppliers')
     * @param bool $regenerate
     * @return bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public static function copyImg(
        int    $id_entity,
        ?int   $id_image = null,
        string $url = '/',
        string $entity = 'products',
        bool   $regenerate = true
    ): bool
    {
        $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));

        switch ($entity) {
            default:
            case 'products':
                $image_obj = new Image($id_image);
                $path = $image_obj->getPathForCreation();
                break;
            case 'categories':
                $path = _PS_CAT_IMG_DIR_ . $id_entity;
                break;
            case 'manufacturers':
                $path = _PS_MANU_IMG_DIR_ . $id_entity;
                break;
            case 'suppliers':
                $path = _PS_SUPP_IMG_DIR_ . $id_entity;
                break;
            case 'stores':
                $path = _PS_STORE_IMG_DIR_ . $id_entity;
                break;
        }

        $url = urldecode(trim($url));
        $parced_url = parse_url($url);

        if (isset($parced_url['path'])) {
            $uri = ltrim($parced_url['path'], '/');
            $parts = explode('/', $uri);
            foreach ($parts as &$part) {
                $part = rawurlencode($part);
            }
            unset($part);
            $parced_url['path'] = '/' . implode('/', $parts);
        }

        if (isset($parced_url['query'])) {
            $query_parts = array();
            parse_str($parced_url['query'], $query_parts);
            $parced_url['query'] = http_build_query($query_parts);
        }

        if (!function_exists('http_build_url')) {
            require_once _PS_TOOL_DIR_ . 'http_build_url/http_build_url.php';
        }

        $url = http_build_url('', $parced_url);

        $orig_tmpfile = $tmpfile;

        if (Tools::copy($url, $tmpfile)) {
            ## Se evalúa la cantidad de memoria para hacer redimensionar la imagen: si es mucha, no se redimensiona:
            if (!ImageManager::checkImageMemoryLimit($tmpfile)) {
                @unlink($tmpfile);
                return false;
            }

            $tgt_width = $tgt_height = 0;
            $src_width = $src_height = 0;
            $error = 0;
            ImageManager::resize(
                $tmpfile, $path . '.jpg', null, null, 'jpg', false,
                $error, $tgt_width, $tgt_height, 5, $src_width, $src_height
            );
            $images_types = ImageType::getImagesTypes($entity, true);

            if ($regenerate) {
                $previous_path = null;
                $path_infos = array();
                $path_infos[] = array($tgt_width, $tgt_height, $path . '.jpg');
                foreach ($images_types as $image_type) {
                    $tmpfile = self::get_best_path($image_type['width'], $image_type['height'], $path_infos);

                    if (ImageManager::resize($tmpfile, $path . '-' . stripslashes($image_type['name']) . '.jpg', $image_type['width'],
                        $image_type['height'], 'jpg', false, $error, $tgt_width, $tgt_height, 5,
                        $src_width, $src_height)) {
                        ## La última imagen no debe ser añadida en la lista de candidatas si es más grande que la imagen original:
                        if ($tgt_width <= $src_width && $tgt_height <= $src_height) {
                            $path_infos[] = array($tgt_width, $tgt_height, $path . '-' . stripslashes($image_type['name']) . '.jpg');
                        }
                        if ($entity == 'products') {
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . $id_entity . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . $id_entity . '.jpg');
                            }
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . $id_entity . '_' . (int)Context::getContext()->shop->id . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . $id_entity . '_' . (int)Context::getContext()->shop->id . '.jpg');
                            }
                        }
                    }
                    if (in_array($image_type['id_image_type'], $watermark_types)) {
                        Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_entity));
                    }
                }
            }
        } else {
            @unlink($orig_tmpfile);
            return false;
        }
        unlink($orig_tmpfile);
        return true;
    }

    /**
     * Esta función guarda una imagen al directorio determinado de acuerdo a su entidad
     * correspondiente. El parámetro 'id_image' es usado si se necesita una marca de agua.
     *
     * @param string $tmpfile (Nombre del fichero temporal)
     * @param int $id_entity (ID del producto o categoría (establecido en la entidad))
     * @param int|null $id_image (Por defecto a null, es el ID de imagen para habilitar marca de agua)
     * @param string $entity (Entidades productos, categorías, fabricantes o proveedores; esto es
     * se admiten sólo las siguientes cadenas: 'products', 'categories', 'manufacturers' o 'suppliers')
     * @param bool $regenerate
     * @return bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public static function saveImg(
        string $tmpfile,
        int    $id_entity,
        int    $id_image = null,
        string $entity = 'products',
        bool   $regenerate = true
    ): bool
    {
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));

        switch ($entity) {
            default:
            case 'products':
                $image_obj = new Image($id_image);
                $path = $image_obj->getPathForCreation();
                break;
            case 'categories':
                $path = _PS_CAT_IMG_DIR_ . $id_entity;
                break;
            case 'manufacturers':
                $path = _PS_MANU_IMG_DIR_ . $id_entity;
                break;
            case 'suppliers':
                $path = _PS_SUPP_IMG_DIR_ . $id_entity;
                break;
            case 'stores':
                $path = _PS_STORE_IMG_DIR_ . $id_entity;
                break;
        }

        $orig_tmpfile = $tmpfile;

        ## Se evalúa la cantidad de memoria para hacer redimensionar la imagen: si es mucha, no se redimensiona:
        if (!ImageManager::checkImageMemoryLimit($tmpfile)) {
            @unlink($tmpfile);
            return false;
        }

        $tgt_width = $tgt_height = 0;
        $src_width = $src_height = 0;
        $error = 0;
        ImageManager::resize(
            $tmpfile, $path . '.jpg', null, null, 'jpg', false,
            $error, $tgt_width, $tgt_height, 5,
            $src_width, $src_height
        );
        $images_types = ImageType::getImagesTypes($entity, true);

        if ($regenerate) {
            $previous_path = null;
            $path_infos = array();
            $path_infos[] = array($tgt_width, $tgt_height, $path . '.jpg');
            foreach ($images_types as $image_type) {
                $tmpfile = self::get_best_path($image_type['width'], $image_type['height'], $path_infos);

                if (ImageManager::resize($tmpfile, $path . '-' . stripslashes($image_type['name']) . '.jpg', $image_type['width'],
                    $image_type['height'], 'jpg', false, $error, $tgt_width, $tgt_height, 5,
                    $src_width, $src_height)) {
                    ## La última imagen no debe ser añadida en la lista de candidatas si es más grande que la imagen original:
                    if ($tgt_width <= $src_width && $tgt_height <= $src_height) {
                        $path_infos[] = array($tgt_width, $tgt_height, $path . '-' . stripslashes($image_type['name']) . '.jpg');
                    }
                    if ($entity == 'products') {
                        if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . $id_entity . '.jpg')) {
                            unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . $id_entity . '.jpg');
                        }
                        if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . $id_entity . '_' . (int)Context::getContext()->shop->id . '.jpg')) {
                            unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . $id_entity . '_' . (int)Context::getContext()->shop->id . '.jpg');
                        }
                    }
                }
                if (in_array($image_type['id_image_type'], $watermark_types)) {
                    Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_entity));
                }
            }
        }

        unlink($orig_tmpfile);
        return true;
    }

    /**
     * @param $tgt_width
     * @param $tgt_height
     * @param $path_infos
     * @return mixed|string
     */
    private static function get_best_path($tgt_width, $tgt_height, $path_infos)
    {
        $path_infos = array_reverse($path_infos);
        $path = '';
        foreach ($path_infos as $path_info) {
            list($width, $height, $path) = $path_info;
            if ($width >= $tgt_width && $height >= $tgt_height) {
                return $path;
            }
        }
        return $path;
    }

    /**
     * Método que busca eliminar las características de un determinado producto pasado por parámetro.
     * Sólo se van a eliminar las características con un ID igual o inferior al pasado en segundo parámetro.
     *
     * @param int $id_product (El ID del producto)
     * @param int $limit_id (El último ID de característica afectado por la eliminación. Por defecto el de ID = 6)
     * @throws PrestaShopDatabaseException
     */
    public static function delProdFeatUpToLimit(int $id_product, int $limit_id = 6): void
    {
        ## Listado de características del producto identificado por parámetro:
        $features_sql = 'SELECT p.*, f.* FROM ' . _DB_PREFIX_ . 'feature_product as p LEFT JOIN '
            . _DB_PREFIX_ . 'feature_value as f ON f.id_feature_value = p.id_feature_value'
            . ' WHERE p.id_product = ' . $id_product . ' AND p.id_feature <= ' . $limit_id;
        $features = Db::getInstance()->executeS($features_sql);

        foreach ($features as $feature) {
            ## Eliminar características 'custom' del producto en cuestión
            if ($feature['custom']) {
                Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'feature_value WHERE id_feature_value = ' . (int)$feature['id_feature_value']);
                Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'feature_value_lang WHERE id_feature_value = ' . (int)$feature['id_feature_value']);
            }
        }

        ## Eliminar las características del producto parametrizado:
        Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'feature_product WHERE id_product = ' . $id_product . ' AND id_feature <= ' . $limit_id);

        SpecificPriceRule::applyAllRules(array($id_product));
    }


}

