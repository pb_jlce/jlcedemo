<?php

namespace jlcedemo\Classes;

use Db;
use Tools;
use Media;
use DbQuery;
use DateTime;
use Language;
use ObjectModel;
use Configuration;
use PrestaShopLogger;
use PrestaShopException;
use PrestaShopDatabaseException;

/**
 * Class JlcedemoTools
 * Herramientas diversas
 * @author Jose Lorenzo Cameselle Escribano <lorenzo.cameselle@gmail.com>
 */
class JlcedemoTools extends ObjectModel
{
    /**
     * @var array (array validators)
     */
    public static array $validators = [
        'active' => array('JlcedemoTools', 'getBoolean'),
        'tax_rate' => array('JlcedemoTools', 'getPrice'),
        /** Tax excluded */
        'price_tex' => array('JlcedemoTools', 'getPrice'),
        /** Tax included */
        'price_tin' => array('JlcedemoTools', 'getPrice'),
        'reduction_price' => array('JlcedemoTools', 'getPrice'),
        'reduction_percent' => array('JlcedemoTools', 'getPrice'),
        'wholesale_price' => array('JlcedemoTools', 'getPrice'),
        'ecotax' => array('JlcedemoTools', 'getPrice'),
        'name' => array('JlcedemoTools', 'createMultiLangField'),
        'description' => array('JlcedemoTools', 'createMultiLangField'),
        'description_short' => array('JlcedemoTools', 'createMultiLangField'),
        'meta_title' => array('JlcedemoTools', 'createMultiLangField'),
        'meta_keywords' => array('JlcedemoTools', 'createMultiLangField'),
        'meta_description' => array('JlcedemoTools', 'createMultiLangField'),
        'link_rewrite' => array('JlcedemoTools', 'createMultiLangField'),
        'available_now' => array('JlcedemoTools', 'createMultiLangField'),
        'available_later' => array('JlcedemoTools', 'createMultiLangField'),
        'category' => array('JlcedemoTools', 'split'),
        'online_only' => array('JlcedemoTools', 'getBoolean'),
    ];

    /**
     * @param $field
     * @return bool
     */
    protected static function getBoolean($field): bool
    {
        return (bool)$field;
    }

    /**
     * @param $field
     * @return float
     */
    protected static function getPrice($field): float
    {
        $field = ((float)str_replace(',', '.', $field));
        return ((float)str_replace('%', '', $field));
    }

    /**
     * @param $field
     * @return array
     */
    public static function split($field): array
    {
        if (empty($field)) return [];

        $separator = Tools::getValue('multiple_value_separator');
        if (is_null($separator) || trim($separator) == '') {
            $separator = ',,';
        }

        do {
            $uniqid_path = _PS_UPLOAD_DIR_ . uniqid();
        } while (file_exists($uniqid_path));

        file_put_contents($uniqid_path, $field);

        $tab = '';
        if (!empty($uniqid_path)) {
            $fd = fopen($uniqid_path, 'r');
            $tab = fgetcsv($fd, MAX_LINE_SIZE, $separator);
            fclose($fd);
            if (file_exists($uniqid_path)) {
                @unlink($uniqid_path);
            }
        }

        if (empty($tab) || (!is_array($tab))) return [];

        return $tab;
    }

    /**
     * @param $field
     * @return array
     */
    public static function createMultiLangField($field): array
    {
        $result = [];
        foreach (Language::getIDs(false) as $id_lang) {
            $result[$id_lang] = $field;
        }

        return $result;
    }

    /**
     * Esta función comprueba si el artículo (combinación) recibido por parámetro
     * es de tipo 'ML' (metro lineal) o no lo es.
     *
     * @param string $item_reference (La referencia de artículo hijo (combinación))
     * @return bool (Se devuelve 'true' en caso de ser un artículo de tipo
     * 'ML'; 'false' en cualquier otro caso)
     */
    public static function isMLItemType(string $item_reference): bool
    {
        $mySQLQuery = /** @lang MySQL DB query */
            "SELECT COUNT(*)
            FROM " . _DB_PREFIX_ . "jlcedemo_items_features
            WHERE item_features LIKE '%\"UTM\":\"ML\"%'
            AND item_reference = '$item_reference'";
        $isMLCuttingProdType = Db::getInstance()->getValue($mySQLQuery);

        return (bool)$isMLCuttingProdType;
    }

    /**
     * Esta función comprueba si el artículo (combinación) recibido por parámetro
     * es de tipo 'M2' (metro cuadrado) o no lo es.
     *
     * @param string $item_reference (La referencia de artículo hijo (combinación))
     * @return bool (Se devuelve 'true' en caso de ser un artículo de tipo
     * 'M2'; 'false' en cualquier otro caso)
     */
    public static function isM2ItemType(string $item_reference): bool
    {
        $mySQLQuery = /** @lang MySQL DB query */
            "SELECT COUNT(*)
            FROM " . _DB_PREFIX_ . "jlcedemo_items_features
            WHERE item_features LIKE '%\"UTM\":\"M2\"%'
            AND item_reference = '$item_reference'";
        $isM2CuttingProdType = Db::getInstance()->getValue($mySQLQuery);

        return (bool)$isM2CuttingProdType;
    }

    /**
     * Esta función comprueba si el artículo (combinación) recibido por parámetro
     * es de tipo 'NA' (venta por unidades enteras) o no lo es.
     *
     * @param string $item_reference (La referencia de artículo hijo (combinación))
     * @return bool (Se devuelve 'true' en caso de ser un artículo de tipo
     * 'NA'; 'false' en cualquier otro caso)
     */
    public static function isNAItemType(string $item_reference): bool
    {
        $mySQLQuery = /** @lang MySQL DB query */
            "SELECT COUNT(*)
            FROM " . _DB_PREFIX_ . "jlcedemo_items_features
            WHERE item_features LIKE '%\"UTM\":\"NA\"%'
            AND item_reference = '$item_reference'";
        $isNACuttingProdType = Db::getInstance()->getValue($mySQLQuery);

        return (bool)$isNACuttingProdType;
    }

    /**
     * @param array $enterArray (El array de entrada. Puede ser hasta bidimensional
     * no asociativo en su primera dimensión)
     * @param array $exclKeys (El array con las claves a excluir de la conversión.
     * Si se trata de un array bidimensional sólo se tratan esas claves en la
     * segunda dimensión; esto es, la dimensión de menor nivel)
     * @return array|mixed (Se devuelve el mismo array que el de entrada pero con
     * sus claves convertibles a número ya convertidas a ello. Se excluyen de
     * la conversión las claves pasadas en el array del segundo parámetro. Para
     * arrays bidimensionales, toda la conversión se realiza siempre en el nivel
     * dimensional más inferior)
     */
    public static function arrayValuesToNumeric(array $enterArray, array $exclKeys = []): array
    {
        if (!empty($enterArray) && is_array($exclKeys)) {
            if (isset($enterArray[0]) && is_array($enterArray[0])) {
                foreach ($enterArray as $thisKey => $thisArray) {
                    $enterArray[$thisKey] = self::arrayValuesToNumeric($thisArray, $exclKeys);
                }
            } else {
                foreach ($enterArray as $eachKey => $eachData) {
                    if (!empty($exclKeys) && in_array($eachKey, $exclKeys)) {
                        continue;
                    }
                    if (is_numeric($eachData)) {
                        $enterArray[$eachKey] = $eachData * 1;
                    }
                }
            }
        }

        return $enterArray;
    }

    /**
     * Función que devuelve el código ISO del país de una
     * dirección dado su ID de dirección
     *
     * @param int $id_address (El ID de la dirección)
     * @return string (El código ISO del país de la dirección)
     */
    public static function getAddress_iso_code(int $id_address): string
    {
        $iso_code = '';

        $sql_address = 'SELECT ctr.iso_code'
            . ' FROM ' . _DB_PREFIX_ . 'address dir'
            . ' INNER JOIN ' . _DB_PREFIX_ . 'country ctr ON dir.id_country = ctr.id_country '
            . ' WHERE dir.id_address = ' . $id_address;

        $result = Db::getInstance()->getValue($sql_address);
        if ($result) $iso_code = $result;

        return $iso_code;
    }

    /**
     * @param array $arr
     * @param array $set
     * @return array
     */
    public static function recursive_change_key(array $arr, array $set): array
    {
        $newArr = [];
        foreach ($arr as $k => $v) {
            $key = array_key_exists($k, $set) ? $set[$k] : $k;
            $newArr[$key] = is_array($v) ? self::recursive_change_key($v, $set) : $v;
        }
        return empty($newArr) ? $arr : $newArr;
    }

    /**
     * @param $arr
     * @param $oldkey
     * @param $newkey
     * @return array|false
     */
    public static function array_replace_key($arr, $oldkey, $newkey)
    {
        if (array_key_exists($oldkey, $arr)) {
            $keys = array_keys($arr);
            $keys[array_search($oldkey, $keys)] = $newkey;
            return array_combine($keys, $arr);
        }
        return $arr;
    }

    /**
     * @param string $haystack (La cadena en la que buscar la subcadena)
     * @param string $needle (La subcadena por la que se busca que comience
     * la cadena del primer argumento)
     * @return bool (Devuelve 'true' si la cadena pasada como primer argumento
     * comienza por la subcadena pasada como segundo argumento. 'False' en
     * cualquier otro caso.)
     */
    public static function startsWith(string $haystack, string $needle): bool
    {
        return (substr($haystack, 0, strlen($needle)) === $needle);
    }

    /**
     * @param string $haystack (La cadena en la que buscar la subcadena)
     * @param string $needle (La subcadena por la que se busca que finalice
     * la cadena del primer argumento)
     * @return bool (Devuelve 'true' si la cadena pasada como primer argumento
     * finaliza por la subcadena pasada como segundo argumento. 'False' en
     * cualquier otro caso.)
     */
    public static function endsWith(string $haystack, string $needle): bool
    {
        if (strlen($needle) == 0) {
            return true;
        }

        return (substr($haystack, -strlen($needle)) === $needle);
    }

    /**
     * Función que devuelve los códigos ISO de todos los países
     * @return array (Devuelve los códigos ISO de todos los países)
     */
    public static function getCountries_iso_codes(): array
    {
        return array
        (
            'AF' => 'Afghanistan', 'AX' => 'Aland Islands', 'AL' => 'Albania', 'DZ' => 'Algeria', 'AS' => 'American Samoa',
            'AD' => 'Andorra', 'AO' => 'Angola', 'AI' => 'Anguilla', 'AQ' => 'Antarctica', 'AG' => 'Antigua And Barbuda',
            'AR' => 'Argentina', 'AM' => 'Armenia', 'AW' => 'Aruba', 'AU' => 'Australia', 'AT' => 'Austria', 'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas', 'BH' => 'Bahrain', 'BD' => 'Bangladesh', 'BB' => 'Barbados', 'BY' => 'Belarus', 'BE' => 'Belgium',
            'BZ' => 'Belize', 'BJ' => 'Benin', 'BM' => 'Bermuda', 'BT' => 'Bhutan', 'BO' => 'Bolivia', 'BA' => 'Bosnia And Herzegovina',
            'BW' => 'Botswana', 'BV' => 'Bouvet Island', 'BR' => 'Brazil', 'IO' => 'British Indian Ocean Territory', 'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria', 'BF' => 'Burkina Faso', 'BI' => 'Burundi', 'KH' => 'Cambodia', 'CM' => 'Cameroon', 'CA' => 'Canada',
            'CV' => 'Cape Verde', 'KY' => 'Cayman Islands', 'CF' => 'Central African Republic', 'TD' => 'Chad', 'CL' => 'Chile', 'CN' => 'China',
            'CX' => 'Christmas Island', 'CC' => 'Cocos (Keeling) Islands', 'CO' => 'Colombia', 'KM' => 'Comoros', 'CG' => 'Congo',
            'CD' => 'Congo, Democratic Republic', 'CK' => 'Cook Islands', 'CR' => 'Costa Rica', 'CI' => 'Cote D\'Ivoire', 'HR' => 'Croatia',
            'CU' => 'Cuba', 'CY' => 'Cyprus', 'CZ' => 'Czech Republic', 'DK' => 'Denmark', 'DJ' => 'Djibouti', 'DM' => 'Dominica', 'DO' => 'Dominican Republic',
            'EC' => 'Ecuador', 'EG' => 'Egypt', 'SV' => 'El Salvador', 'GQ' => 'Equatorial Guinea', 'ER' => 'Eritrea', 'EE' => 'Estonia', 'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands (Malvinas)', 'FO' => 'Faroe Islands', 'FJ' => 'Fiji', 'FI' => 'Finland', 'FR' => 'France', 'GF' => 'French Guiana',
            'PF' => 'French Polynesia', 'TF' => 'French Southern Territories', 'GA' => 'Gabon', 'GM' => 'Gambia', 'GE' => 'Georgia', 'DE' => 'Germany',
            'GH' => 'Ghana', 'GI' => 'Gibraltar', 'GR' => 'Greece', 'GL' => 'Greenland', 'GD' => 'Grenada', 'GP' => 'Guadeloupe', 'GU' => 'Guam',
            'GT' => 'Guatemala', 'GG' => 'Guernsey', 'GN' => 'Guinea', 'GW' => 'Guinea-Bissau', 'GY' => 'Guyana', 'HT' => 'Haiti',
            'HM' => 'Heard Island & Mcdonald Islands', 'VA' => 'Holy See (Vatican City State)', 'HN' => 'Honduras', 'HK' => 'Hong Kong', 'HU' => 'Hungary',
            'IS' => 'Iceland', 'IN' => 'India', 'ID' => 'Indonesia', 'IR' => 'Iran, Islamic Republic Of', 'IQ' => 'Iraq', 'IE' => 'Ireland', 'IM' => 'Isle Of Man',
            'IL' => 'Israel', 'IT' => 'Italy', 'JM' => 'Jamaica', 'JP' => 'Japan', 'JE' => 'Jersey', 'JO' => 'Jordan', 'KZ' => 'Kazakhstan', 'KE' => 'Kenya',
            'KI' => 'Kiribati', 'KR' => 'Korea', 'KW' => 'Kuwait', 'KG' => 'Kyrgyzstan', 'LA' => 'Lao People\'s Democratic Republic', 'LV' => 'Latvia',
            'LB' => 'Lebanon', 'LS' => 'Lesotho', 'LR' => 'Liberia', 'LY' => 'Libyan Arab Jamahiriya', 'LI' => 'Liechtenstein', 'LT' => 'Lithuania',
            'LU' => 'Luxembourg', 'MO' => 'Macao', 'MK' => 'Macedonia', 'MG' => 'Madagascar', 'MW' => 'Malawi', 'MY' => 'Malaysia', 'MV' => 'Maldives',
            'ML' => 'Mali', 'MT' => 'Malta', 'MH' => 'Marshall Islands', 'MQ' => 'Martinique', 'MR' => 'Mauritania', 'MU' => 'Mauritius', 'YT' => 'Mayotte',
            'MX' => 'Mexico', 'FM' => 'Micronesia, Federated States Of', 'MD' => 'Moldova', 'MC' => 'Monaco', 'MN' => 'Mongolia', 'ME' => 'Montenegro',
            'MS' => 'Montserrat', 'MA' => 'Morocco', 'MZ' => 'Mozambique', 'MM' => 'Myanmar', 'NA' => 'Namibia', 'NR' => 'Nauru', 'NP' => 'Nepal',
            'NL' => 'Netherlands', 'AN' => 'Netherlands Antilles', 'NC' => 'New Caledonia', 'NZ' => 'New Zealand', 'NI' => 'Nicaragua', 'NE' => 'Niger',
            'NG' => 'Nigeria', 'NU' => 'Niue', 'NF' => 'Norfolk Island', 'MP' => 'Northern Mariana Islands', 'NO' => 'Norway', 'OM' => 'Oman',
            'PK' => 'Pakistan', 'PW' => 'Palau', 'PS' => 'Palestinian Territory, Occupied', 'PA' => 'Panama', 'PG' => 'Papua New Guinea', 'PY' => 'Paraguay',
            'PE' => 'Peru', 'PH' => 'Philippines', 'PN' => 'Pitcairn', 'PL' => 'Poland', 'PT' => 'Portugal', 'PR' => 'Puerto Rico', 'QA' => 'Qatar',
            'RE' => 'Reunion', 'RO' => 'Romania', 'RU' => 'Russian Federation', 'RW' => 'Rwanda', 'BL' => 'Saint Barthelemy', 'SH' => 'Saint Helena',
            'KN' => 'Saint Kitts And Nevis', 'LC' => 'Saint Lucia', 'MF' => 'Saint Martin', 'PM' => 'Saint Pierre And Miquelon',
            'VC' => 'Saint Vincent And Grenadines', 'WS' => 'Samoa', 'SM' => 'San Marino', 'ST' => 'Sao Tome And Principe', 'SA' => 'Saudi Arabia',
            'SN' => 'Senegal', 'RS' => 'Serbia', 'SC' => 'Seychelles', 'SL' => 'Sierra Leone', 'SG' => 'Singapore', 'SK' => 'Slovakia', 'SI' => 'Slovenia',
            'SB' => 'Solomon Islands', 'SO' => 'Somalia', 'ZA' => 'South Africa', 'GS' => 'South Georgia And Sandwich Isl.', 'ES' => 'Spain', 'LK' => 'Sri Lanka',
            'SD' => 'Sudan', 'SR' => 'Suriname', 'SJ' => 'Svalbard And Jan Mayen', 'SZ' => 'Swaziland', 'SE' => 'Sweden', 'CH' => 'Switzerland',
            'SY' => 'Syrian Arab Republic', 'TW' => 'Taiwan', 'TJ' => 'Tajikistan', 'TZ' => 'Tanzania', 'TH' => 'Thailand', 'TL' => 'Timor-Leste', 'TG' => 'Togo',
            'TK' => 'Tokelau', 'TO' => 'Tonga', 'TT' => 'Trinidad And Tobago', 'TN' => 'Tunisia', 'TR' => 'Turkey', 'TM' => 'Turkmenistan',
            'TC' => 'Turks And Caicos Islands', 'TV' => 'Tuvalu', 'UG' => 'Uganda', 'UA' => 'Ukraine', 'AE' => 'United Arab Emirates', 'GB' => 'United Kingdom',
            'US' => 'United States', 'UM' => 'United States Outlying Islands', 'UY' => 'Uruguay', 'UZ' => 'Uzbekistan', 'VU' => 'Vanuatu', 'VE' => 'Venezuela',
            'VN' => 'Viet Nam', 'VG' => 'Virgin Islands, British', 'VI' => 'Virgin Islands, U.S.', 'WF' => 'Wallis And Futuna', 'EH' => 'Western Sahara',
            'YE' => 'Yemen', 'ZM' => 'Zambia', 'ZW' => 'Zimbabwe'
        );
    }

    /**
     * Función para limpiar de espacios, puntos, guiones, etc., una cadena solicitada (pasada por parámetro)
     * @param string $cadena (La cadena a limpiar)
     * @return string (La cadena limpia de caracteres como espacios, guiones, etc.)
     */
    public static function trimCadena(string $cadena): string
    {
        ## Se procede a limpiar la cadena de caracteres como espacios, guiones, puntos, etc.:
        return preg_replace('/[\t\n\r\f\e\v\s\.\-\_]/', '', $cadena);
    }

    /**
     * Función que devuelve el ID de la dirección por defecto de un cliente (customer);
     * esto es, en este caso la primera dirección que coincida con el primer alias de dirección
     * que haya creado (aunque se hubiere modificado), pero que esté actualmente y en cualquier
     * caso activa y no eliminada.
     *
     * @param int $id_customer (ID del cliente (customer).)
     * @return int (Se devuelve el ID de la dirección o 0 si no existe.)
     */
    public static function getInitialCustomerAddressId(int $id_customer): int
    {
        ## Primero se obtiene el primer alias de dirección que se hubiera
        ## registrado en primera instancia, aunque esta dirección esté ya eliminada,
        ## debido a una modificación por ejemplo:
        $sql_first_address_alias = 'SELECT alias'
            . ' FROM ' . _DB_PREFIX_ . 'address'
            . ' WHERE id_customer = ' . $id_customer
            . ' AND active = 1';
        $first_address_alias = Db::getInstance()->getValue($sql_first_address_alias);
        if (!$first_address_alias) return 0;

        ## Ahora se obtiene el ID de la dirección activa y no eliminada que se
        ## corresponde a ese primer alias inicial registrado:
        $sql_id_address_initial_bassed = 'SELECT id_address'
            . ' FROM ' . _DB_PREFIX_ . 'address'
            . ' WHERE id_customer = ' . $id_customer
            . ' AND alias = \'' . $first_address_alias . '\''
            . ' AND active = 1 AND deleted = 0';
        $id_address_initial_bassed = Db::getInstance()->getValue($sql_id_address_initial_bassed);

        ## Si la consulta anterior arroja un resultado negativo:
        if (!$id_address_initial_bassed) {
            ## Se busca sin más la primera dirección que se hubiere registrado,
            ## aunque ya no haya ningún alias igual y ésta esté eliminada:
            $sql_customer_first_id_address = 'SELECT id_address'
                . ' FROM ' . _DB_PREFIX_ . 'address'
                . ' WHERE id_customer = ' . $id_customer
                . ' AND active = 1';

            $customer_first_id_address = Db::getInstance()->getValue($sql_customer_first_id_address);

            if (!$customer_first_id_address) return 0;
            return (int)$customer_first_id_address;
        }

        return (int)$id_address_initial_bassed;
    }

    /**
     * Función que devuelve un array bidimensional con los datos de los cupones que puedan estar imputados a un
     * determinado carrito de un pedido. Devuelve un array vacío en caso de no haber cupones relacionados.
     *
     * @param int $id_order (El ID de pedido de PrestaShop)
     * @param int $id_cart (El ID del carrito asociado al pedido de PrestaShop)
     * @return array (Se devuelve bien un array vacío si no hay cupones, bien un array bidimensional con los datos de los cupones que haya)
     * @throws PrestaShopDatabaseException
     */
    public static function getOrderVouchers(int $id_order, int $id_cart): array
    {
        $order_array_cupones_dto = [];

        ## Se obtienen los cupones que puedan estar relacionados con el ID de carrito dado:
        $cart_cart_rule_rows = Db::getInstance()->executeS('SELECT * FROM ' . _DB_PREFIX_ . 'cart_cart_rule WHERE id_cart = ' . $id_cart);
        if (empty($cart_cart_rule_rows)) return [];

        ## Se recorre el array anterior:
        foreach ($cart_cart_rule_rows as $cart_cart_rule_row) {
            ## Se obtienen los datos de los cupones relacionados con cada línea:
            $cart_rule_data_row = Db::getInstance()->getRow('SELECT code, description, reduction_percent, reduction_amount, reduction_tax, '
                . 'reduction_currency, reduction_product, date_from, date_to FROM ' . _DB_PREFIX_
                . 'cart_rule WHERE id_cart_rule = ' . (int)$cart_cart_rule_row['id_cart_rule']);

            $order_cart_rule_row = Db::getInstance()->getRow('SELECT name, value, value_tax_excl, free_shipping FROM ' . _DB_PREFIX_
                . 'order_cart_rule WHERE id_cart_rule = ' . (int)$cart_cart_rule_row['id_cart_rule']
                . ' AND id_order = ' . $id_order);

            if (!empty($order_cart_rule_row)) {
                $cart_rule_value = $order_cart_rule_row['value'];
                $cart_rule_value_tax_excl = $order_cart_rule_row['value_tax_excl'];
                if ($cart_rule_value_tax_excl > 0) {
                    $cart_rule_tax_rate = (($cart_rule_value / $cart_rule_value_tax_excl) - 1) * 100;
                } else {
                    $cart_rule_tax_rate = 0;
                }

                $cart_cart_rule_row['value'] = $cart_rule_value;
                $cart_cart_rule_row['value_tax_excl'] = $cart_rule_value_tax_excl;
                $cart_cart_rule_row['tax_rate'] = round($cart_rule_tax_rate);
                $cart_cart_rule_row['free_shipping'] = $order_cart_rule_row['free_shipping'];
                $cart_cart_rule_row['name'] = $order_cart_rule_row['name'];
            }

            ## Si hay datos relacionados con ese ID:
            if (!empty($cart_rule_data_row)) {
                $cart_cart_rule_row['code'] = $cart_rule_data_row['code'];
                $cart_cart_rule_row['description'] = $cart_rule_data_row['description'];
                $cart_cart_rule_row['reduction_percent'] = $cart_rule_data_row['reduction_percent'];
                $cart_cart_rule_row['reduction_amount'] = $cart_rule_data_row['reduction_amount'];
                $cart_cart_rule_row['reduction_tax'] = $cart_rule_data_row['reduction_tax'];
                $cart_cart_rule_row['reduction_currency'] = $cart_rule_data_row['reduction_currency'];
                $cart_cart_rule_row['reduction_product'] = $cart_rule_data_row['reduction_product'];
                $cart_cart_rule_row['date_from'] = $cart_rule_data_row['date_from'];
                $cart_cart_rule_row['date_to'] = $cart_rule_data_row['date_to'];
            }

            ## Se llena el array contenedor 'order_array_cupones_dto' con los datos de los cupones:
            $order_array_cupones_dto[] = $cart_cart_rule_row;
        }

        return self::arrayValuesToNumeric($order_array_cupones_dto, [
            "free_shipping", "name", "code", "description"
        ]);
    }

    /**
     * Esta función devuelve la última clave de un array pasado por parámetro
     *
     * @param array $this_array (El array al que se le quiere buscar su última clave)
     * @return int|string|null (Se devuelve bien la última clave del array, bien 'null')
     */
    public static function get_array_last_key(array $this_array)
    {
        if (empty($this_array)) return null;
        return array_keys($this_array)[(count($this_array) - 1)];
    }

    /**
     * Esta función transforma una imagen en forma de string base64
     * a un fichero imagen que se guarda en una ruta parametrizada.
     *
     * @param string $base64_string (La imagen como string base64)
     * @param string $output_file (La ruta buscada para la generación del fichero imagen)
     * @return string (La ruta de generación del fichero imagen)
     */
    public static function base64ToImageFile(string $base64_string, string $output_file): string
    {
        $file = fopen($output_file, 'wb');

        fwrite($file, base64_decode($base64_string));
        fclose($file);

        return $output_file;
    }

    /**
     * Método para forzar el vaciado de caché de PrestaShop.
     *
     * @param bool $clearSmartyCache (Forzar borrado de caché smarty)
     * @param bool $clearMediaCache (Forzar borrado de caché media)
     * @param bool $clearXMLCache (Forzar borrado de caché XML)
     * @param bool $generateIndex (Regenerar el índice de búsquedas)
     */
    public static function clearPSCache(
        bool $clearSmartyCache = true,
        bool $clearMediaCache = true,
        bool $clearXMLCache = true,
        bool $generateIndex = true
    )
    {
        if ($clearSmartyCache) Tools::clearSmartyCache();
        if ($clearMediaCache) Media::clearCache();
        if ($clearXMLCache) Tools::clearXMLCache();
        if ($generateIndex) Tools::generateIndex();
    }

    /**
     * @param int $id_product (El identificador del producto de PrestaShop)
     * @param int $id_group (El identificador de grupo de cliente de PrestaShop)
     * @return float (Se devuelve la reducción aplicada al producto recibido
     * por primer parámetro, para el grupo recibido por segundo parámetro)
     */
    public static function getProdCatGrpReduction(int $id_product, int $id_group)
    {
        $mySQLQuery = /** @lang MySQL DB query */
            "SELECT pgr.reduction
            FROM " . _DB_PREFIX_ . "group_reduction pgr
            LEFT JOIN " . _DB_PREFIX_ . "product pp
                ON pp.id_category_default = pgr.id_category
            WHERE pgr.id_group = $id_group
            AND pp.id_product = $id_product";

        $result = Db::getInstance()->getValue($mySQLQuery);

        ## Para un resultado esperado:
        if ($result && is_numeric($result)) return ($result * 1);

        return 0;
    }

    /**
     * @param string $url_request (La URL completa de la petición HTTP)
     * @param string $method (Métodos de petición HTTP: GET, POST, PUT, PATCH, DELETE, etc.)
     * @param string|null $json_body_request ('null' o el cuerpo de la petición en formato JSON)
     * @param string|null $bearer_token ('null' o el token de autorización para poder realizar
     * la petición si es exigido)
     * @return bool|mixed|string
     */
    public static function cURL_HTTP_Request(
        string $url_request,
        string $method,
        string $json_body_request = null,
        string $bearer_token = null
    )
    {
        $headers = [
            'Accept: application/json',
            'Content-Type: application/json'
        ];

        if (isset($bearer_token)) $headers[] = 'Authorization: Bearer ' . $bearer_token;

        $curl = curl_init();
        if (isset($json_body_request)) {
            curl_setopt_array($curl, [
                CURLOPT_URL => $url_request,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_POSTFIELDS => $json_body_request,
                CURLOPT_HTTPHEADER => $headers
            ]);
        } else {
            curl_setopt_array($curl, [
                CURLOPT_URL => $url_request,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_HTTPHEADER => $headers
            ]);
        }

        $response = curl_exec($curl);
        $responseInfo = curl_getinfo($curl);

        ## Respuesta no esperada:
        if ($responseInfo["http_code"] < 200 || $responseInfo["http_code"] >= 300) {
            $responseInfo["error_response"] = is_string($response) ? json_decode($response, true) : $response;
            $response = $responseInfo;
        }

        curl_close($curl);
        return $response;
    }

    /**
     * Se comprueba que el NIF recibido por parámetro sea mínimamente válido.
     * No se hace una validación de DNIs.
     *
     * @param string $nif (El número de identificación fiscal (NIF) a validar)
     * @return bool|string (Se devuelve el NIF ligeramente formateado si el
     * recibido pasa esta validación; 'false' en cualquier otro caso)
     */
    public static function validateNIF(string $nif)
    {
        ## Se le da un poco de formato al NIF recibido por parámetro:
        $nif = preg_replace('/[^0-9a-zA-Z]/i', '', $nif);
        $nif = strtoupper($nif);

        ## Debe haber un NIF que procesar:
        if (empty($nif)) return false;

        ## Si el supuesto NIF ya tiene un tamaño inferior a 8 caracteres, se descarta directamente:
        if (strlen($nif) < 8) return false;

        ## Subcadenas no válidas:
        $noValidStrs = [
            '11111', '22222', '33333', '44444', '55555',
            '66666', '77777', '88888', '99999', '00000'
        ];
        foreach ($noValidStrs as $eachSubStr) {
            if (strpos($nif, $eachSubStr) !== false) return false;
        }

        ## No se permiten más de 3 caracteres no numéricos en la cadena $nif:
        $noNumericChars = 0;
        $nifCharsArr = str_split($nif);
        foreach ($nifCharsArr as $thisChar) {
            if (!is_numeric($thisChar)) $noNumericChars++;
        }
        if ($noNumericChars > 3) return false;

        return $nif;
    }

    /**
     * @param array $inventory (El array que determina el número de unidades de cada medida
     * actualmente en inventario, por ejemplo: [["UnitsQty" => 7, "Measure" => 3000]])
     * @param array $ordered (El array que informa del número de unidades solicitadas de
     * cada medida concreta, por ejemplo: [["UnitsQty" => 9, "Measure" => 2000]])
     * @param array $scraps (Por defecto un array vacío. Se puede pasar un array de recortes
     * existentes, y se unificará con el array de inventario)
     * @param bool $scrapsToInventory (Por defecto 'false'. Si se recibe un 'true', en el
     * retorno de la función se devolverán el array de inventario y restos unificado)
     * @return array (Se devulve el array que determina lo que queda en inventario, el array
     * de lo que falte por servir y el array de los posibles restos generados en el proceso.
     * Si se ha optado por unificar inventario con restos, el tercer array del retorno estará
     * siempre vacío, y el primero será la unión de inventario y posibles restos del proceso)
     */
    public static function cutting1D(
        array $inventory,
        array $ordered,
        array $scraps = [],
        bool  $scrapsToInventory = false
    ): array
    {
        ## Se comienza ordenando y reestructurando los arrays recibios por el valor de las medidas:
        if (!empty($inventory)) {
            $inventory = self::arrayGroupBy($inventory, ["Measure"], "UnitsQty");
            $inventory = self::assoc_arr_sort($inventory, "Measure");
        }
        if (!empty($ordered)) {
            $ordered = self::arrayGroupBy($ordered, ["Measure"], "UnitsQty");
            $ordered = self::assoc_arr_sort($ordered, "Measure", true);
        }
        if (!empty($scraps)) {
            $scraps = self::arrayGroupBy($scraps, ["Measure"], "UnitsQty");
            $scraps = self::assoc_arr_sort($scraps, "Measure");
        }

        if ($scrapsToInventory && !empty($scraps)) {
            $inventory = array_merge($inventory, $scraps);
            $inventory = self::arrayGroupBy($inventory, ["Measure"], "UnitsQty");
            $inventory = self::assoc_arr_sort($inventory, "Measure");
            $scraps = [];
        }

        ## Si no hay nada en inventario, o no se ha pedido nada:
        if (empty($inventory) || empty($ordered)) {
            return [$inventory, $ordered, $scraps];
        }

        ## Se comienza el proceso de consumo de piezas de inventario para satisfacer lo solicitado:
        restartProccess:
        $scrapsArray = [];
        $scrpMsrToInv = [];
        foreach ($inventory as $invIndex => &$eachInvArr) {
            ## De cada medida del inventario se definen:
            $invUnitsQty = (int)$eachInvArr["UnitsQty"];
            $invMeasure = $eachInvArr["Measure"] * 1;

            foreach ($ordered as $orderIndex => &$order) {
                $ordUnitsQty = (int)$order["UnitsQty"];
                $ordMeasure = $order["Measure"] * 1;

                ## Se comienza el proceso que consume las unidades inventariadas con las solicitadas,
                ## generando tras de sí posibles restos:
                while ($invMeasure >= $ordMeasure && $invUnitsQty > 0 && $ordUnitsQty > 0) {
                    $thisScrapMsr = $invMeasure - $ordMeasure;
                    $invUnitsQty--;
                    $ordUnitsQty--;

                    ## Si el resto obtenido da para satisfacer más unidades de las solicitadas:
                    while ($thisScrapMsr > 0 && $thisScrapMsr >= $ordMeasure && $ordUnitsQty > 0) {
                        $thisScrapMsr -= $ordMeasure;
                        $ordUnitsQty--;
                    }

                    ## Si aún hay algún resto en esta vuelta del bucle:
                    if (!empty($thisScrapMsr)) {
                        ## En este punto se intenta determinar si este resto puede satisfacer
                        ## alguna de las solicitudes. Si fuera así, se añadirá al inventario,
                        ## para intentar satisfacer dichas solicitudes:
                        $addScrpToInv = false;
                        foreach ($ordered as $eachOrdr) {
                            if ($thisScrapMsr >= ($eachOrdr["Measure"] * 1)) {
                                $addScrpToInv = true;
                                break;
                            }
                        }
                        ## Según se pueda añadir el presente resto a inventario o no:
                        if ($addScrpToInv) {
                            $scrpMsrToInv[] = [
                                "UnitsQty" => 1,
                                "Measure" => $thisScrapMsr
                            ];
                        } else {
                            $scrpMsrToInv = [];
                            ## Se va conformando un array de restos:
                            $scrapsArray[] = [
                                "UnitsQty" => 1,
                                "Measure" => $thisScrapMsr
                            ];
                        }
                    }
                }

                ## Siempre que falte algo de lo solicitado por satisfacer:
                if ($ordUnitsQty > 0) {
                    $ordered[$orderIndex] = [
                        "UnitsQty" => $ordUnitsQty,
                        "Measure" => $ordMeasure
                    ];
                } else {
                    ## Si ya se ha satisfecho la solicitud de la medida concreta:
                    unset($ordered[$orderIndex]);
                }

                ## Si previamente se ha encontrado un resto que pueda satisfacer solicitudes:
                if (!empty($scrpMsrToInv)) break;
            }

            ## Se recalcula lo que queda en inventario:
            if ($invUnitsQty > 0) {
                $inventory[$invIndex] = [
                    "UnitsQty" => $invUnitsQty,
                    "Measure" => $invMeasure
                ];
            } else {
                ## Si se han consumido todas las unidades de la presente medida:
                unset($inventory[$invIndex]);
            }

            ## Si previamente se ha encontrado un resto que pueda satisfacer solicitudes:
            if (!empty($scrpMsrToInv)) {
                ## Se fusiona con el inventario:
                $inventory = array_merge($inventory, $scrpMsrToInv);
                $inventory = self::assoc_arr_sort($inventory, "Measure");
                ## Y se reinicia el proceso porque es necesario empezar desde las medidas
                ## más pequeñas en inventario:
                goto restartProccess;
            }
        }

        ## En este punto se busca agrupar los posibles restos del proceso anterior:
        if (!empty($scrapsArray)) {
            foreach ($scrapsArray as $thisScrap) {
                $thisUtsQty = 0;
                $thisMsr = $thisScrap["Measure"] * 1;
                foreach ($scrapsArray as $index => $eachScrp) {
                    $eachUtsQty = (int)$eachScrp["UnitsQty"];
                    $eachMsr = $eachScrp["Measure"] * 1;
                    if ($eachMsr === $thisMsr) {
                        $thisUtsQty += $eachUtsQty;
                        unset($scrapsArray[$index]);
                    }
                }
                if ($thisUtsQty > 0) {
                    $scraps[] = [
                        "UnitsQty" => $thisUtsQty,
                        "Measure" => $thisMsr
                    ];
                }
            }
        }

        ## Si llegado a esta altura, aún hay unidades pendientes de satisfacer, se
        ## intentan satisfacer incluso con los restos obtenidos, de haberlos:
        if (!empty($scraps)) {
            ## Siempre que haya restos, se reestructuran por medidas:
            $scraps = self::arrayGroupBy($scraps, ["Measure"], "UnitsQty");
            $scraps = self::assoc_arr_sort($scraps, "Measure");
            ## Sólo se continúa si hay unidades pendientes de satisfacer:
            if (!empty($ordered)) {
                ## Orden descendente de solicitudes de medidas:
                $ordered = self::assoc_arr_sort($ordered, "Measure", true);
                ## Por cada resto:
                foreach ($scraps as $scrpIndex => $eachScrpArr) {
                    ## De cada medida de los restos se definen:
                    $scrpUnitsQty = (int)$eachScrpArr["UnitsQty"];
                    $scrpMeasure = $eachScrpArr["Measure"] * 1;

                    foreach ($ordered as $orderIndex => $orderArr) {
                        $ordUnitsQty = (int)$orderArr["UnitsQty"];
                        $ordMeasure = $orderArr["Measure"] * 1;

                        ## Se comienza el proceso que consume las unidades de los restos con las solicitadas,
                        ## generando tras de sí posibles nuevos restos:
                        while ($scrpMeasure >= $ordMeasure && $scrpUnitsQty > 0 && $ordUnitsQty > 0) {
                            $newScrapMsr = $scrpMeasure - $ordMeasure;
                            $scrpUnitsQty--;
                            $ordUnitsQty--;

                            ## Si el nuevo resto obtenido da para satisfacer más unidades de las solicitadas:
                            while ($newScrapMsr > 0 && $newScrapMsr >= $ordMeasure && $ordUnitsQty > 0) {
                                $newScrapMsr -= $ordMeasure;
                                $ordUnitsQty--;
                            }

                            ## Si aún hay algún resto en esta vuelta del bucle:
                            if (!empty($newScrapMsr)) {
                                ## Se añaden al existente array de restos:
                                $scraps[] = [
                                    "UnitsQty" => 1,
                                    "Measure" => $newScrapMsr
                                ];
                            }
                        }

                        ## Siempre que falte algo de lo solicitado por satisfacer:
                        if ($ordUnitsQty > 0) {
                            $ordered[$orderIndex] = [
                                "UnitsQty" => $ordUnitsQty,
                                "Measure" => $ordMeasure
                            ];
                        } else {
                            ## Si ya se ha satisfecho la solicitud de la medida concreta:
                            unset($ordered[$orderIndex]);
                        }
                    }

                    ## Se recalcula lo que pueda quedar pendiente de restos de los cuales
                    ## se hayan descontado unidades en el proceso de consumo:
                    if ($scrpUnitsQty > 0) {
                        $scraps[$scrpIndex] = [
                            "UnitsQty" => $scrpUnitsQty,
                            "Measure" => $scrpMeasure
                        ];
                    } else {
                        ## Si se han consumido todas las unidades de la presente medida:
                        unset($scraps[$scrpIndex]);
                    }
                }
            }
            ## Para ir terminando se vuelve a reestructurar el array de restos:
            $scraps = self::arrayGroupBy($scraps, ["Measure"], "UnitsQty");
            $scraps = self::assoc_arr_sort($scraps, "Measure");
        }

        ## Y se reestructuran los arrays de inventario y solicitudes de medidas:
        $inventory = self::arrayGroupBy($inventory, ["Measure"], "UnitsQty");
        $ordered = self::arrayGroupBy($ordered, ["Measure"], "UnitsQty");
        $inventory = self::assoc_arr_sort($inventory, "Measure");
        $ordered = self::assoc_arr_sort($ordered, "Measure", true);

        if ($scrapsToInventory && !empty($scraps)) {
            $inventory = array_merge($inventory, $scraps);
            $inventory = self::arrayGroupBy($inventory, ["Measure"], "UnitsQty");
            $inventory = self::assoc_arr_sort($inventory, "Measure");
            $scraps = [];
        }

        return [$inventory, $ordered, $scraps];
    }

    /**
     * @param array $assocArrToSort (El array asociativo a ordenar)
     * @param string $key (La clave por la que se quiere ordenar el array recibido)
     * @param bool $descMethod (Por defecto 'false', orden ascendente. Si se recibe
     * un 'true' el orden será descendente)
     * @return array (Se devuelve el array recibido pero ordenado por los valores de
     * la clave pasada en segundo parámetro. En caso de fallo, se devuelve el array
     * sin modificaciones)
     */
    public static function assoc_arr_sort(array $assocArrToSort, string $key, bool $descMethod = false): array
    {
        if (empty($assocArrToSort)) return [];
        $columnArr = array_column($assocArrToSort, $key);
        array_multisort($columnArr, ($descMethod ? SORT_DESC : SORT_ASC), $assocArrToSort);
        return $assocArrToSort;
    }

    /**
     * Esta función recibe un array bidimensional para agruparlo por una serie de claves de sus
     * arrays interiores y opcionalmente cuenta esas repeticiones sobre una clave determinada
     * y parametrizada de los mismos.
     *
     * @param array $originalArray (El array original sobre el que buscar hacer la agrupación)
     * @param array $groupByKeysArray (El array que representa el listado de claves sobre las que agrupar)
     * @param string $countKey (Opcionalmente la clave sobre la que contar las repeticiones. Por defecto
     * es una cadena vacía)
     * @param bool $setCounter (Por defecto 'false'. Si se pasa un 'true' se va a llevar un contador de
     * repeticiones sobre una clave llamada "RepsCount")
     * @return array (Se devuelve el array final ya agrupado o un array vacío si hay algún tipo de problema)
     */
    public static function arrayGroupBy(
        array  $originalArray,
        array  $groupByKeysArray,
        string $countKey = '',
        bool   $setCounter = false
    ): array
    {
        if (empty($countKey) || !is_string($countKey)) $countKey = '';
        $groupedArray = [];

        ## Se recorre el array original:
        foreach ($originalArray as $childArray) {

            $repeat = false;
            $resultValues = [];

            ## Se conforma el array 'groupedArray':
            for ($index = 0; $index < count($groupedArray); $index++) {
                $makeGroup = true;

                ## Se recorren las claves por las que se busca filtrar:
                if ($setCounter) $groupedArray[$index]['RepsCount'] = 1;
                for ($idx = 0; $idx < count($groupByKeysArray); $idx++) {
                    $thisKey = $groupByKeysArray[$idx];
                    ## En la última clave se termina por esclarecer si se trata de una repetición.
                    ## De ser así, se sale de la actual iteración:
                    if ($idx === (count($groupByKeysArray) - 1)) {
                        if ($makeGroup && $groupedArray[$index][$thisKey] === $childArray[$thisKey]) {
                            if (!empty($countKey)) $groupedArray[$index][$countKey] += $childArray[$countKey];
                            if ($setCounter) $groupedArray[$index]['RepsCount'] += 1;
                            $repeat = true;
                            break;
                        }
                    } else {
                        ## En las demás claves se va determinando si se está ante una repetición:
                        if ($makeGroup && $groupedArray[$index][$thisKey] === $childArray[$thisKey]) {
                            $makeGroup = true;
                        } else {
                            $makeGroup = false;
                        }
                    }
                }
            }

            ## Siempre que no se esté ante una repetición de valores:
            if ($repeat === false) {
                ## Se conforma el array de valores agrupados y contados:
                foreach ($groupByKeysArray as $eachValue) {
                    $resultValues[$eachValue] = $childArray[$eachValue];
                }
                if (!empty($countKey)) $resultValues[$countKey] = $childArray[$countKey];
                if ($setCounter && !array_key_exists('RepsCount', $resultValues)) $resultValues['RepsCount'] = 1;

                ## Se establece el array comentado como objetivo del retorno de la función:
                $groupedArray[] = $resultValues;
            }
        }

        return $groupedArray;
    }

    /**
     * @return string (Se devuelve la cadena que representa el tiempo actual en formato tiempo TZ;
     * por ejemplo: "2021-11-24T18:33:58.677133Z")
     */
    public static function nowTZ(): string
    {
        return DateTime::createFromFormat('U.u', number_format(
                microtime(true), 6, '.', '')
        )->format("Y-m-d\TH:i:s.u\Z");
    }

    /**
     * Esta función busca evaluar si una cadena (un string) está definida
     * y si lo está, evaluar si está vacía. Si está definida y no vacía,
     * se devolverá limpia de espacios a los lados. Si se trata de un número
     * se retorna como cadena igualmente.
     *
     * @param string|int|float $cadena (La supuesta cadena o número a evaluar)
     * @return string (Se devuelve una cadena limpia de espacios a los dos lados, una
     * cadena que representa a un número, o una cadena vacía. En cualquier caso un string)
     */
    public static function evRetStr($cadena): string
    {
        if (isset($cadena) && is_string($cadena) && !empty(trim($cadena))) {
            return trim($cadena);
        }

        if (isset($cadena) && is_numeric($cadena)) return $cadena;

        return '';
    }

    /**
     * Esta función busca evaluar si una variable tipo número está definida
     * y si lo está, evaluar si efectivamente es un número. De ser así
     * se devuelve; y si no se cumple, se devuelve un 0 o una cadena vacía,
     * según lo que se busque y defina desde el segundo parámetro.
     *
     * @param $num (El supuesto número a evaluar)
     * @param bool $else_return_zero (Si el primer parámetro no es numérico, devolver
     * un 0 o una cadena vacía. Por defecto sí se devuelve ese número 0 ('true'))
     * @return int|float|string (Se devuelve ese mismo número o, si ese primer
     * parámetro no es numérico, un 0 o una cadena vacía, según lo parametrizado
     * desde el segundo parámetro)
     */
    public static function evRetNum($num, bool $else_return_zero = true)
    {
        if (isset($num) && is_numeric($num)) return ($num * 1);
        return $else_return_zero ? 0 : '';
    }

    /**
     * Esta función recibe una medida concreta y evalúa si se trata o no de un número
     * entero. Si es así se devuelve ese número, de lo contrario se devuelve bien
     * una cadena vacía, bien un null si se recibe un true en el segundo parámetro.
     *
     * @param $measure (La medida a evaluar)
     * @param bool $returNull (Por defecto false. Si se pasa un true, cuando la
     * medida recibida no es numérica, se devuelve un null directamente)
     * @return int|float|string|null (Se devuelve un número que representa la medida,
     * o una cadena vacía (o null si así se parametriza) si la medida no es válida)
     */
    public static function evMeasure($measure, bool $returNull = false)
    {
        if (empty($measure) || $measure <= 0 || $measure == 'null' || !is_numeric($measure)) {
            return ($returNull ? null : '');
        }

        return ($measure * 1);
    }

    /**
     * Función para obtener la representación en formato decimal de un número recibido en coma flotante.
     *
     * @param float $strNumber (El número en coma flotante del que obtener su representación decimal)
     * @param int $precision (La precisión en forma de cantidad máxima de cifras decimales)
     * @param bool $stringNum (Por defecto se devolverá un float|int, pero se puede forzar a devolver
     * una cadena numérica si se recibe un 'true' en este parámetro)
     * @return float|int (Se devuelve el número en formato decimal)
     */
    public static function toFixed(float $strNumber, int $precision = 0, bool $stringNum = false)
    {
        $strNumber = number_format($strNumber, $precision, '.', '');
        $number = $strNumber * 1;
        return $stringNum ? $strNumber : $number;
    }

    /**
     * @param $string
     * @param $start
     * @param $end
     * @return false|string
     */
    public static function getStringBetween($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    /**
     * @return mixed
     */
    public static function array_orderby()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = [];
                foreach ($data as $key => $row)
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }

    /**
     * @param int|null $id_product_attribute (El identificador de la combinación de producto;
     * esto es, el identificador del artículo a buscar)
     * @param string $reference (Opcional. La referencia de la combinación del producto;
     * esto es, el código de artículo a buscar)
     * @return array (Se devuelve bien un array con el contenido del campo 'measures_stocks'
     * de la tabla 'ps_jlcedemo_items_features', bien un array vacío)
     */
    public static function getItemsLotsInventory(
        ?int   $id_product_attribute,
        string $reference = ""
    ): array
    {
        if (empty($id_product_attribute) && !empty($reference)) {
            $mySQLQuery = /** @lang MySQL DB query */
                "SELECT measures_stocks
                FROM " . _DB_PREFIX_ . "jlcedemo_items_features
                WHERE item_reference = '$reference'";
        } elseif (!empty($id_product_attribute)) {
            $mySQLQuery = /** @lang MySQL DB query */
                "SELECT PEIF.measures_stocks
                FROM " . _DB_PREFIX_ . "jlcedemo_items_features PEIF
                INNER JOIN " . _DB_PREFIX_ . "product_attribute PPA
                    ON PPA.reference = PEIF.item_reference
                    AND PPA.id_product = PEIF.id_product
                WHERE PPA.id_product_attribute = $id_product_attribute";
        } else {
            return [];
        }

        $result = Db::getInstance()->getValue($mySQLQuery);

        if (is_string($result) && is_array(json_decode($result, true))) {
            $result = json_decode($result, true);
            return self::assoc_arr_sort($result, "long");
        }

        return [];
    }

    /**
     * @param string|null $item_reference (La referencia del artículo hijo (combinación) de un
     * producto padre)
     * @param array|null $measures_stocks (El array que representa el stock por medidas de lotes del
     * artículo hijo recibido)
     * @return bool (Se devuelve 'true' si se actualiza correctamente el campo 'measures_stocks'
     * del artículo hijo recibido; 'false' en cualquier otro caso)
     */
    public static function setItemLotsInventory(?string $item_reference, ?array $measures_stocks): bool
    {
        ## Debe recibirse una referencia de artículo NO válida:
        if (empty($item_reference)) return false;

        if (empty($measures_stocks)) {
            $mySQLQuery = /** @lang MySQL DB query */
                "UPDATE " . _DB_PREFIX_ . "jlcedemo_items_features
                SET measures_stocks = NULL
                WHERE item_reference = '$item_reference'";
        } else {
            $measuresStocksJSON = json_encode($measures_stocks);
            $mySQLQuery = /** @lang MySQL DB query */
                "UPDATE " . _DB_PREFIX_ . "jlcedemo_items_features
                SET measures_stocks = '$measuresStocksJSON'
                WHERE item_reference = '$item_reference'";
        }

        return Db::getInstance()->execute($mySQLQuery);
    }

    /**
     * Para una referencia de artículo hijo dada, obtener su ID de
     * producto padre relacionado.
     *
     * @param string $reference (La referencia del artículo hijo)
     * @return int (Se devuelve el ID del producto padre relacionado con la referencia
     * de artículo hijo recibida por parámetro)
     */
    public static function getIdProductByChildItemRef(string $reference): int
    {
        if (empty($reference)) return 0;

        $query = new DbQuery();
        $query->select('pa.id_product');
        $query->from('product_attribute', 'pa');
        $query->where('pa.reference LIKE \'%' . pSQL($reference) . '%\'');

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);
        if ($result) return $result;
        return 0;
    }

    /**
     * Para un Id de artículo hijo (combinación) determinado, obtener su código de referencia.
     *
     * @param int $id_product_attribute (El Id de artículo hijo (combinación) del producto)
     * @return string (Se devuelve el código de referencia del artículo hijo (combinación))
     */
    public static function getReferenceByItemId(int $id_product_attribute): string
    {
        if (empty($id_product_attribute)) return "";

        $query = new DbQuery();
        $query->select("pa.reference");
        $query->from("product_attribute", "pa");
        $query->where("pa.id_product_attribute = $id_product_attribute");

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);
        if ($result) return $result;

        return "";
    }

    /**
     * @param string|null $reference (La referencia que identifica al artículo hijo (combinación))
     * @param int|null $id_product_attribute (El identificador del artículo hijo (combinación))
     * @return string (Se devuelve el tipo de artículo; a saber, 'ML' (metro lineal), 'M2'
     * (metro cuadrado) o 'NA' (venta por unidades))
     */
    public static function getItemType(?string $reference, ?int $id_product_attribute = null): string
    {
        if (empty($reference) && empty($id_product_attribute)) return "undefined";

        if (!empty($reference)) {
            $mysql_query = /** @lang MySQL DB query */
                'SELECT
                    CASE
                        WHEN item_features LIKE \'%"UTM":"ML"%\' THEN \'ML\'
                        WHEN item_features LIKE \'%"UTM":"M2"%\' THEN \'M2\'
                        ELSE \'NA\'
                    END "UTM"
                FROM ' . _DB_PREFIX_ . 'jlcedemo_items_features
                WHERE item_reference = \'' . $reference . '\'';
        } else {
            $mysql_query = /** @lang MySQL DB query */
                'SELECT
                    CASE
                        WHEN PEIF.item_features LIKE \'%"UTM":"ML"%\' THEN \'ML\'
                        WHEN PEIF.item_features LIKE \'%"UTM":"M2"%\' THEN \'M2\'
                        ELSE \'NA\'
                    END "UTM"
                FROM ' . _DB_PREFIX_ . 'jlcedemo_items_features PEIF
                WHERE PEIF.item_reference = (
                    SELECT PPA.reference
                    FROM ' . _DB_PREFIX_ . 'product_attribute PPA
                    WHERE PPA.id_product_attribute = ' . $id_product_attribute . '
                )';
        }

        $itemType = Db::getInstance()->getValue($mysql_query);
        return empty($itemType) ? "undefined" : $itemType;
    }

    /**
     * @param $array
     * @param $funcname
     * @param mixed $user_data
     * @return bool
     */
    public static function arrayWalk(&$array, $funcname, &$user_data = false): bool
    {
        if (!is_callable($funcname)) return false;

        foreach ($array as $k => $row) {
            if (!call_user_func_array($funcname, array($row, $k, &$user_data))) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $infos
     * @param $key
     * @param $entity
     * @return bool
     * @throws PrestaShopException
     */
    public static function fillInfo($infos, $key, &$entity): bool
    {
        $infos = trim($infos);
        if (isset(self::$validators[$key][1]) && self::$validators[$key][1] == 'createMultiLangField' && Tools::getValue('iso_lang')) {
            $id_lang = Language::getIdByIso(Tools::getValue('iso_lang'));
            $tmp = call_user_func(self::$validators[$key], $infos);
            foreach ($tmp as $id_lang_tmp => $value) {
                if (empty($entity->{$key}[$id_lang_tmp]) || $id_lang_tmp == $id_lang) {
                    $entity->{$key}[$id_lang_tmp] = $value;
                }
            }
        } elseif (!empty($infos) || $infos == '0') { // ($infos == '0') => Para deshabilitar un producto usar '0' en 'active', porque empty('0') devuelve 'true'
            $entity->{$key} = isset(self::$validators[$key]) ? call_user_func(self::$validators[$key], $infos) : $infos;
        }

        return true;
    }

    /**
     * Función que crea el campo de personalización 'jlce_Measures' para un producto cuyo
     * identificador 'id_product' se recibe por parámetro.
     *
     * @param int $id_product (El identificador del producto de PrestaShop)
     * @return bool (Se devuelve 'true' en caso de éxito; 'false' en cualquier otro caso)
     * @todo En el futuro, si corresponde, mejorar la función para que sea multidioma y multitienda.
     */
    public static function createJlceMeasuresCustField(int $id_product): bool
    {
        ## De salida el campo sólo se va a crear para el idioma y tienda por defecto:
        $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $id_shop = (int)Configuration::get('PS_SHOP_DEFAULT');

        ## Se busca insertar el campo de personalización en la tabla de la BD correspondiente:
        $mySQLQuery = /** @lang MySQL DB query */
            "INSERT IGNORE
            INTO " . _DB_PREFIX_ . "customization_field (id_product, type, required, is_module, is_deleted)
            VALUES ($id_product, 1, 0, 0, 0)";
        $insertReslt = Db::getInstance()->execute($mySQLQuery);

        ## Resultado esperado:
        if ($insertReslt) {
            ## Se consulta el ID de campo de personalización generado en el paso anterior:
            $mySQLQuery = /** @lang MySQL DB query */
                "SELECT id_customization_field
                FROM " . _DB_PREFIX_ . "customization_field
                WHERE id_product = $id_product";
            $id_customization_field = (int)Db::getInstance()->getValue($mySQLQuery);

            ## Resultado esperado:
            if (!empty($id_customization_field)) {
                ## Se inserta el nombre del campo 'jlce_Measures', en la tabla que le corresponde:
                $mySQLQuery = /** @lang MySQL DB query */
                    "INSERT IGNORE
                    INTO " . _DB_PREFIX_ . "customization_field_lang (id_customization_field, id_lang, id_shop, name)
                    VALUES ($id_customization_field, $id_lang, $id_shop, 'jlce_Measures')";
                $insertResult = Db::getInstance()->execute($mySQLQuery);

                ## Resultado NO esperado:
                if (!$insertResult) {
                    ## Se elimina el campo de personalización anteriormente creado:
                    $mySQLQuery = /** @lang MySQL DB query */
                        "DELETE FROM " . _DB_PREFIX_ . "customization_field WHERE id_product = $id_product";
                    Db::getInstance()->execute($mySQLQuery);
                    return false;
                }
            } else {
                ## Resultado NO esperado => Se elimina el campo de personalización anteriormente creado:
                $mySQLQuery = /** @lang MySQL DB query */
                    "DELETE FROM " . _DB_PREFIX_ . "customization_field WHERE id_product = $id_product";
                Db::getInstance()->execute($mySQLQuery);
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * Función que obtiene el ratio de factor de conversión de un artículo a partir
     * de los datos de la tabla 'ps_jlcedemo_items_features'.
     *
     * @param int $id_product_attribute (El identificador del artículo (hijo), la
     * combinación de producto)
     * @return int (Se devuelve el entero que representa el ratio de factor de
     * conversión del artículo)
     */
    public static function getItemFrmtCnvrsnRatio(int $id_product_attribute): int
    {
        $mySQLQuery = /** @lang MySQL DB query */
            "SELECT peif.item_features featrs
            FROM " . _DB_PREFIX_ . "jlcedemo_items_features peif
            WHERE peif.item_reference = (
                SELECT ppa.reference
                FROM " . _DB_PREFIX_ . "product_attribute ppa
                WHERE ppa.id_product_attribute = $id_product_attribute
            )";
        $item_features = Db::getInstance()->getValue($mySQLQuery);
        if (empty($item_features)) return 1;

        ## Se obtiene un array de la cadena obtenida 'item_features':
        $item_features = json_decode($item_features, true);
        $itemUTM = $item_features["UTM"];

        ## Ratio de factor de conversión; esto es, gramos que pesa cada metro, o
        ## metro cuadrado, de producto (para 'ML' y 'M2'). Para productos 'NA' es 1:
        if (in_array($itemUTM, ['ML', 'M2'])) return (int)($item_features["UnitWeight"] * 1000);

        ## Artículos de tipo 'NA':
        return 1;
    }


}

