<?php

use jlcedemo\Classes\JlcedemoTools;
use Doctrine\Persistence\ObjectManager;

if (!defined('_PS_VERSION_')) exit;

/**
 * Módulo PrestaShop
 *
 * Class jlcedemo
 * @author  Jose Lorenzo Cameselle Escribano <lorenzo.cameselle@gmail.com>
 */
class jlcedemo extends Module
{
    public static ?AppKernel $kernel = null;
    public static ?ObjectManager $doctrine = null;

    /**
     * jlcedemo constructor.
     */
    function __construct()
    {
        $this->name = 'jlcedemo';
        $this->tab = 'jlcedemo';
        $this->author = 'Jose Lorenzo Cameselle Escribano (lorenzo.cameselle@gmail.com)';
        $this->version = '1.8.1';
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('jlcedemo', [], 'Modules.jlcedemo.Admin');
        $this->description = $this->trans(
            'jlcedemo, por Jose Lorenzo Cameselle Escribano (lorenzo.cameselle@gmail.com)',
            [], 'Modules.jlcedemo.Admin'
        );
        $this->ps_versions_compliancy = ['min' => '1.7.7.x', 'max' => _PS_VERSION_];
        $this->need_instance = 0;
        $this->confirmUninstall = $this->trans(
            '¿Deseas desinstalar el módulo?', [], 'Modules.jlcedemo.Admin'
        );
    }

    /**
     * @return bool
     */
    public function install(): bool
    {
        $mySQLQuery = "CREATE TABLE IF NOT EXISTS
            " . _DB_PREFIX_ . "jlcedemo_items_features(
                id_product INT(10) UNSIGNED NOT NULL,
                item_reference VARCHAR(64) NOT NULL,
                item_features TEXT DEFAULT NULL,
                measures_stocks MEDIUMTEXT DEFAULT NULL,
                PRIMARY KEY (id_product, item_reference)
            ) ENGINE = " . _MYSQL_ENGINE_ . " DEFAULT CHARSET = UTF8";
        if (!Db::getInstance()->execute($mySQLQuery)) return false;

        $withErr = false;
        ## Se añaden 4 decimales más a la columna 'unit_price_ratio' de la tabla 'product'.
        ## Por defecto es un DECIMAL(20,6):
        $mySQLQuery = "ALTER TABLE " . _DB_PREFIX_
            . "product CHANGE unit_price_ratio unit_price_ratio DECIMAL(20,10) NOT NULL DEFAULT 0";
        if (!Db::getInstance()->execute($mySQLQuery)) $withErr = true;

        ## Se añaden 4 decimales más a la columna 'unit_price_ratio' de la tabla 'product_shop'.
        ## Por defecto es un DECIMAL(20,6):
        $mySQLQuery = "ALTER TABLE " . _DB_PREFIX_
            . "product_shop CHANGE unit_price_ratio unit_price_ratio DECIMAL(20,10) NOT NULL DEFAULT 0";
        if (!Db::getInstance()->execute($mySQLQuery)) $withErr = true;

        ## Se añaden 4 decimales más a la columna 'price' de la tabla 'customized_data'.
        ## Por defecto es un DECIMAL(20,6):
        $mySQLQuery = "ALTER TABLE " . _DB_PREFIX_
            . "customized_data CHANGE price price DECIMAL(20,10) NOT NULL DEFAULT 0";
        if (!Db::getInstance()->execute($mySQLQuery)) $withErr = true;

        ## Se modifica la columna 'value' de la tabla 'customized_data' para poder almacenar hasta 765 caracteres.
        ## Por defecto es un VARCHAR(255):
        $mySQLQuery = "ALTER TABLE " . _DB_PREFIX_
            . "customized_data CHANGE value value VARCHAR(765) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL";
        if (!Db::getInstance()->execute($mySQLQuery)) $withErr = true;

        ## Se añade el siguiente hook (gancho) personalizado:
        $mySQLQuery = "INSERT INTO " . _DB_PREFIX_
            . "hook(name, title, description, active, position) VALUES ('displayJlceCustomHook', 'displayJlceCustomHook', 'displayJlceCustomHook', 1, 1)";
        Db::getInstance()->execute($mySQLQuery);

        ## Se establece un alias para el gancho anterior:
        $mySQLQuery = "INSERT INTO " . _DB_PREFIX_ . "hook_alias(alias, name) VALUES ('jlceHook', 'displayJlceCustomHook')";
        Db::getInstance()->execute($mySQLQuery);

        if (!$withErr && parent::install()
            && $this->registerHook('displayHeader')
            && $this->registerHook('displayAdminProductsExtra')
            && $this->registerHook('displayProductAdditionalInfo')
            && $this->registerHook('actionAdminControllerSetMedia')
            && $this->registerHook('actionAdminProductsControllerSaveBefore')
            && $this->registerHook('actionValidateOrder')
            && $this->registerHook('addWebserviceResources')
            && $this->registerHook('actionProductDelete')
            && $this->uninstallOverrides()
            && $this->installOverrides()
        ) {
            return true;
        }

        ## Si se llega hasta aquí, hay que eliminar lo creado y alterado por el módulo:
        if (!$this->cleanInstallation()) {
            $errMssg = "Desinstalación del módulo 'jlcedemo':";
            $errMssg .= " NO se han revertido todos los cambios hechos en BD por el módulo.";
            $errMssg .= " Revisar logs !!";
            PrestaShopLogger::addLog($errMssg, 4);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function uninstall(): bool
    {
        $this->_clearCache('*');

        if (parent::uninstall()
            && $this->unregisterHook('displayHeader')
            && $this->unregisterHook('displayAdminProductsExtra')
            && $this->unregisterHook('displayProductAdditionalInfo')
            && $this->unregisterHook('actionAdminControllerSetMedia')
            && $this->unregisterHook('actionAdminProductsControllerSaveBefore')
            && $this->unregisterHook('actionValidateOrder')
            && $this->unregisterHook('addWebserviceResources')
            && $this->unregisterHook('actionProductDelete')
            && $this->uninstallOverrides()
        ) {
            if (!$this->cleanInstallation()) {
                $errMssg = "Desinstalación del módulo 'jlcedemo':";
                $errMssg .= " NO se han revertido todos los cambios hechos en BD por el módulo.";
                $errMssg .= " Revisar logs !!";
                PrestaShopLogger::addLog($errMssg, 4);
            }
            return true;
        }

        return false;
    }

    /**
     * Método para eliminar la tabla y gancho creados por el módulo y para
     * revertir cambios hechos en ciertas tablas del sistema de base de datos.
     *
     * @return bool (Se devuelve verdadero si se ha limpiado correctamente el
     * despliegue del módulo en BD; falso en cualquier otro caso)
     */
    public function cleanInstallation(): bool
    {
        $this->_clearCache('*');
        $success = true;

        ## Se elimina la tabla creada por el módulo:
        $mySQLQuery = "DROP TABLE IF EXISTS " . _DB_PREFIX_ . "jlcedemo_items_features";
        if (!Db::getInstance()->execute($mySQLQuery)) {
            PrestaShopLogger::addLog(
                "La tabla '" . _DB_PREFIX_ . "jlcedemo_items_features' NO ha podido ser eliminada !!", 3
            );
            $success = false;
        }

        ## Se revierten las alteraciones hechas sobre ciertas tablas:
        $mySQLQuery = "ALTER TABLE " . _DB_PREFIX_
            . "product CHANGE unit_price_ratio unit_price_ratio DECIMAL(20,6) NOT NULL DEFAULT 0";
        if (!Db::getInstance()->execute($mySQLQuery)) {
            PrestaShopLogger::addLog(
                "NO se han podido revertir los cambios sobre la tabla '" . _DB_PREFIX_ . "product'", 3
            );
            $success = false;
        }
        $mySQLQuery = "ALTER TABLE " . _DB_PREFIX_
            . "product_shop CHANGE unit_price_ratio unit_price_ratio DECIMAL(20,6) NOT NULL DEFAULT 0";
        if (!Db::getInstance()->execute($mySQLQuery)) {
            PrestaShopLogger::addLog(
                "NO se han podido revertir los cambios sobre la tabla '" . _DB_PREFIX_ . "product_shop'", 3
            );
            $success = false;
        }
        $mySQLQuery = "ALTER TABLE " . _DB_PREFIX_
            . "customized_data CHANGE price price DECIMAL(20,6) NOT NULL DEFAULT 0";
        if (!Db::getInstance()->execute($mySQLQuery)) {
            PrestaShopLogger::addLog(
                "NO se han podido revertir los cambios sobre la tabla '" . _DB_PREFIX_ . "customized_data'", 3
            );
            $success = false;
        }
        $mySQLQuery = "ALTER TABLE " . _DB_PREFIX_
            . "customized_data CHANGE value value VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL";
        if (!Db::getInstance()->execute($mySQLQuery)) {
            PrestaShopLogger::addLog(
                "NO se han podido revertir los cambios sobre la tabla '" . _DB_PREFIX_ . "customized_data'", 3
            );
            $success = false;
        }

        if ($success) {
            ## Se elimina el gancho (y su alias) creado por el módulo:
            Db::getInstance()->delete("hook_alias", "name = 'displayJlceCustomHook'");
            Db::getInstance()->delete("hook", "name = 'displayJlceCustomHook'");
        }

        return $success;
    }

    /**
     * @return AppKernel|null
     */
    public static function getKernel(): ?AppKernel
    {
        ## Si el singleton no existe:
        if (!self::$kernel) {
            ## Intentar cargarlo globalmente (para las páginas del backoffice):
            global $kernel;
            if ($kernel) {
                self::$kernel = $kernel;
            } else {
                ## O crearlo manualmente:
                require_once _PS_ROOT_DIR_ . '/app/AppKernel.php';
                $env = 'prod'; // _PS_MODE_DEV_ ? 'dev' : 'prod';
                $debug = false; // _PS_MODE_DEV_ ? true : false;
                self::$kernel = new AppKernel($env, $debug);
                self::$kernel->boot();
            }
        }
        return self::$kernel;
    }

    /**
     * @return ObjectManager|null
     */
    public static function getDoctrine(): ?ObjectManager
    {
        if (!self::$doctrine) {
            self::$doctrine = self::getKernel()->getContainer()->get('doctrine')->getManager();
        }
        return self::$doctrine;
    }

    /**
     * @param $service
     * @return object|null
     */
    public static function getService($service)
    {
        return self::getKernel()->getContainer()->get($service);
    }

    /**
     * @return string
     */
    public function postProcess(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->postProcess()
            . $this->renderEnrnPresentation();
    }

    /**
     * @return string
     */
    public function renderEnrnPresentation(): string
    {
        $html = '<div class="jlce-center-center">';
        $html .= '<div class="jlce-h1">jlcedemo</div>';
        $html .= '<div class="jlce-h2">por <strong>JLCE</strong> - Jose Lorenzo Cameselle Escribano (<i>lorenzo.cameselle@gmail.com</i>)</div>';
        $html .= '</div>';

        return $html;
    }

    public function hookDisplayHeader(): void
    {
        $this->context->controller->registerStylesheet(
            'modules-jlcedemo',
            'modules/' . $this->name . '/views/css/jlcedemo.css',
            [
                'media' => 'all',
                'priority' => 150
            ]
        );
        $this->context->controller->registerJavascript(
            'modules-jlcedemo',
            'modules/' . $this->name . '/views/js/jlcedemo.js',
            [
                'position' => 'bottom',
                'priority' => 150
            ]
        );
    }

    public function hookActionAdminControllerSetMedia()
    {
        $this->context->controller->addCSS($this->_path . 'views/admin/css/jlcedemo.css');
        $this->context->controller->addJS($this->_path . 'views/admin/js/jlcedemo.js');
    }

    /**
     * @param $params
     * @return false|string
     */
    public function hookDisplayAdminProductsExtra($params)
    {
        $productItemsFeatures = self::getProductItemsFeatures($params['id_product']);
        $productItemsFeaturesJSON = json_encode($productItemsFeatures, 64 | 256);
        ## Para escapar las "" y poder cargar el JSON en el valor de un INPUT:
        $productItemsFeaturesHTMLData = htmlspecialchars($productItemsFeaturesJSON);

        $this->smarty->assign([
            'product_items_features' => $productItemsFeatures,
            'product_items_features_html_data' => $productItemsFeaturesHTMLData,
            'lang_id' => $this->context->language->id ?? 1,
            'lang_code' => $this->context->language->getLanguageCode(),
            'jlce_id_product' => (int)$params['id_product']
        ]);

        return $this->display(__FILE__, 'views/templates/hook/displayAdmProdsExtra.tpl');
    }

    public function hookActionAdminProductsControllerSaveBefore()
    {
        $languages = Language::getLanguages(false);
        $id_product = (int)$_REQUEST['jlce_id_product'];
        $validated = false;
        foreach ($languages as $lang) {
            ## Se toma el valor del primer idioma que se encuentra (estas características
            ## no se van a truducir, siendo siempre el mismo texto en todos los casos):
            $productItemsFeaturesJSON = $_REQUEST['form_product_items_features'][$lang['id_lang']];
            ## Validación del objeto JSON obtenido, convirtiéndolo ya de paso en arreglo:
            $productItemsFeatures = json_decode($productItemsFeaturesJSON, true);
            if (is_array($productItemsFeatures)) $validated = true;
            break;
        }

        ## Sólo si hay características válidas:
        if ($validated && !empty($productItemsFeatures)) {
            ## Sólo se actualizan datos si son distintos a los ya previamente registrados:
            $productItemsFeatures_old = self::getProductItemsFeatures($id_product);

            if ($productItemsFeatures !== $productItemsFeatures_old) {
                ## Se recorre cada row del array anterior:
                foreach ($productItemsFeatures as $reference => $productItemFeatures) {
                    ## Aquí se necesita trabajar con cadenas; esto es, objetos JSON:
                    $productItemFeaturesJSON = json_encode($productItemFeatures, 64 | 256);
                    ## Se actualiza cada listado de características por cada
                    ## referencia de artículo (combinación) del producto base:
                    $sqlDBQuery = "
                    UPDATE " . _DB_PREFIX_ . "jlcedemo_items_features
                    SET item_features = '" . $productItemFeaturesJSON . "'
                    WHERE id_product = $id_product
                    AND item_reference = '" . $reference . "'";
                    Db::getInstance()->execute($sqlDBQuery);
                }
            }
        }
    }

    /**
     * @return false|string
     */
    public function hookDisplayProductAdditionalInfo()
    {
        $templateVars = $this->context->smarty->getTemplateVars();
        if (!empty($templateVars['product']['id'])) {
            $this->smarty->assign([
                'product_items_features' => self::getProductItemsFeatures($templateVars['product']['id'])
            ]);
        }
        return $this->display(__FILE__, 'views/templates/hook/displayProductAdditionalInfo.tpl');
    }

    /**
     * @param int $id_product (El identificador de producto de PrestaShop)
     * @return array (Se devuelve un arreglo con todas las características de los
     * artículos hijos (combinaciones) del producto recibido por parámetro)
     */
    public static function getProductItemsFeatures(int $id_product): array
    {
        $prodItemsFeats = [];

        $sqlQuery = "
            SELECT item_reference, item_features
            FROM " . _DB_PREFIX_ . "jlcedemo_items_features
            WHERE id_product = $id_product";
        $productItemsFeatures = Db::getInstance()->executeS($sqlQuery);
        if (!is_array($productItemsFeatures)) return [];

        foreach ($productItemsFeatures as $productItemFeatures) {
            $thisItemRef = $productItemFeatures["item_reference"];
            $thisItemFeatsJSON = $productItemFeatures["item_features"];
            $prodItemsFeats[$thisItemRef] = json_decode($thisItemFeatsJSON, true);
        }

        return $prodItemsFeats;
    }

    /**
     * Método que lanza ciertas acciones tras validarse un pedido tras su confirmación.
     * @throws PrestaShopDatabaseException
     */
    public function hookActionValidateOrder(): void
    {
        ## Actualizar el stock por medidas de lotes para los artículos del carrito en contexto:
        $this->setCartItemsMsrsLots($this->context->cart->id);
    }

    /**
     * Método para actualizar el stock por medidas de lotes de los
     * artículos tipo 'ML' y/o 'M2' de un carrito recibido por parámetro.
     * @param int $id_cart (El identificador 'id_cart' de un carrito de PS)
     * @throws PrestaShopDatabaseException
     */
    public function setCartItemsMsrsLots(int $id_cart): void
    {
        ## Measures stocks de lotes (sólo para artículos 'ML' o 'M2'):
        $mysql_query = /** @lang MySQL DB query */
            "SELECT
                PCP.id_cart,
                PCP.id_product,
                PEIF.item_reference,
                PCD.value customztn_measrs
            FROM " . _DB_PREFIX_ . "cart_product PCP
            INNER JOIN " . _DB_PREFIX_ . "product_attribute PPA
                ON PPA.id_product_attribute = PCP.id_product_attribute
            LEFT JOIN " . _DB_PREFIX_ . "customized_data PCD
                ON PCD.id_customization = PCP.id_customization
            LEFT JOIN " . _DB_PREFIX_ . "jlcedemo_items_features PEIF
                ON PEIF.item_reference = PPA.reference
            WHERE PCP.id_cart = $id_cart
            AND PEIF.item_reference IS NOT NULL
            AND PEIF.item_features IS NOT NULL
            AND PEIF.item_features NOT LIKE '%\"UTM\":\"NA\"%'";
        $cartLinesArr = Db::getInstance()->executeS($mysql_query);

        ## Siempre que de la anterior expresión se obtenga un array válido:
        if (!empty($cartLinesArr[0]['customztn_measrs'])) {
            ## Aquí se busca formatear de la manera más adecuada la información obtenida:
            $cartLnsArr = [];

            ## Se recorre cada tupla del array:
            foreach ($cartLinesArr as $eachRow) {
                ## Array de personalización de medidas:
                $eachMeasrsCustArr = json_decode($eachRow["customztn_measrs"], true);
                ## Se va conformando el array formateado:
                $cartLnsArr[] = [
                    "id_cart" => $id_cart,
                    "id_product" => (int)$eachRow["id_product"],
                    "reference" => $eachRow["item_reference"],
                    "base_units" => (int)$eachMeasrsCustArr["baseUnits"],
                    "long" => $eachMeasrsCustArr["long"] * 1,
                    "width" => $eachMeasrsCustArr["width"] * 1,
                    "quantity" => round(($eachMeasrsCustArr["msrQtyWntd"] * 1), 9)
                ];
            }

            ## Siempre que el array 'cartLnsArr' no esté vacío:
            if (!empty($cartLnsArr)) {
                ## Se termina de conformar ya el formato buscado:
                $cartLinesArr = JlcedemoTools::array_orderby(
                    $cartLnsArr, "reference", SORT_DESC, "long", SORT_DESC, "width", SORT_DESC
                );
            }
        }

        ## Para un resultado esperado:
        if (!empty($cartLinesArr[0]['base_units'])) {
            ## Se recorre el array de resultados para registrar el stock de medidas de lotes
            ## de cada artículo 'ML' o 'M2' del presente carrito:
            foreach ($cartLinesArr as $eachItem) {
                $thisItemReference = $eachItem["reference"];
                $thisLong = $eachItem["long"] * 1;
                $thisWidth = $eachItem["width"] * 1;
                $msrLong = $thisLong * 1000;
                $msrWidth = $thisWidth * 1000;
                $msrUnits = (int)$eachItem["base_units"];
                ## Cálculo de cantidad:
                $thisWidth = empty($thisWidth) ? 1 : $thisWidth;
                $msrQty = $msrUnits * $thisLong * $thisWidth;
                ## Se obtiene el stock de medidas de lotes actualizado en base sólo al presente carrito:
                $current_msrs_stock_item = Cart::itemMsrsLotsLessReqsMsrs(
                    $thisItemReference,
                    [
                        "base_units" => $msrUnits,
                        "long" => $msrLong,
                        "width" => $msrWidth,
                        "quantity" => $msrQty
                    ],
                    true
                );
                ## En este punto se espera que 'current_msrs_stock_item' sea siempre un array. De no serlo
                ## habría que informar del problema:
                if (is_array($current_msrs_stock_item)) {
                    ## Se actualiza el stock de medidas de lotes del presente artículo:
                    $updResult = JlcedemoTools::setItemLotsInventory($thisItemReference, $current_msrs_stock_item);
                    ## Para un resultado fallido:
                    if (!$updResult) {
                        $errorMssg = "Carrito $id_cart: NO ha sido posible actualizar el stock de medidas de lotes del artículo $thisItemReference";
                        PrestaShopLogger::addLog($errorMssg, 3);
                        MailCore::Send(1, "http_req_failed", "ERROR: actualización de stock de medidas de lotes", [
                            '{error_content}' => $errorMssg
                        ], "lorenzo.cameselle@gmail");
                    }
                } else {
                    $errorMssg = "Carrito $id_cart: NO ha sido posible determinar el stock de medidas de lotes del artículo $thisItemReference";
                    PrestaShopLogger::addLog($errorMssg, 3);
                    MailCore::Send(1, "http_req_failed", "ERROR: determinación de stock de medidas de lotes", [
                        '{error_content}' => $errorMssg
                    ], "lorenzo.cameselle@gmail");
                }
            }
        }
    }

    /**
     * Añadir entidad al Webservice
     *
     * @param array $params (Parámetros bajo los que se lanza la acción)
     * @return array (El arreglo con los nuevos recursos)
     */
    public function hookAddWebserviceResources($params)
    {
        include_once(_PS_MODULE_DIR_ . 'jlcedemo/classes/WebserviceSpecificManagementJlcedemo.php');
        return [
            'jlcedemo' => [
                'description' => 'Recursos personalizados jlcedemo',
                'specific_management' => true,
            ],
        ];
    }

    /**
     * Tras eliminarse un producto, se eliminará de la tabla 'ps_jlcedemo_items_features'.
     *
     * @param mixed $params (Parámetros bajo los que se lanza la acción)
     */
    public function hookActionProductDelete($params): void
    {
        $deleteQuery = /** @lang MySQL DB query */
            "DELETE FROM " . _DB_PREFIX_ . "jlcedemo_items_features
            WHERE id_product = " . $params['id_product'];
        Db::getInstance()->execute($deleteQuery);
    }


}

